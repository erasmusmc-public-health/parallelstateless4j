/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.Collection;

/**
 * Example implementation of {@link IContextPropertyListener}.
 *
 * @author rinke
 */
public class ExampleContextPropertyListener implements IContextPropertyListener {

    // private static final Logger logger = LoggerFactory.getLogger(DefaultContextPropertyListener.class);

    @Override
    public <S, T> void onChange(IStateMachineContext<S, T> context, String name, Object oldValue, Object newValue) {
        System.out.println("Context property " + name + " changed from "
                + nullSafeToString(oldValue) + " to " + nullSafeToString(newValue));
        // if (logger.isDebugEnabled()) {
        // logger.debug("Context property {} changed from {} to {}",
        // name, nullSafeToString(oldValue), nullSafeToString(newValue));
        // }
    }

    @Override
    public <S, T, E> void onCollectionChange(IStateMachineContext<S, T> context, String name, Collection<E> additions,
            Collection<E> removals, Collection<E> newValue) {
        int nAdditions = (additions == null) ? 0 : additions.size();
        int nRemovals = (removals == null) ? 0 : removals.size();
        System.out.println("Context collection property " + name + " changed....: " +
                nAdditions + " additions and " + nRemovals + " removals, now having " +
                newValue.size() + " values.");
        System.out.println("      Added: " + collectionToString(additions));
        System.out.println("    Removed: " + collectionToString(removals));
    }

    private <E> String collectionToString(Collection<E> coll) {
        if (coll == null) {
            return "null";
        }
        if (coll.size() == 0) {
            return "empty";
        }
        StringBuilder sb = new StringBuilder();
        for (E e : coll) {
            sb.append(e.toString()).append(", ");
        }
        return sb.toString();
    }

    private String nullSafeToString(Object value) {
        return (value == null) ? "null" : value.toString();
    }

}
