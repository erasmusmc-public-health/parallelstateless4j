/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;
import nl.erasmusmc.mgz.parallelstateless4j.testUtils.BaseSMTest;
import nl.erasmusmc.mgz.parallelstateless4j.testUtils.TestUtils;

/**
 * tests for the statemachine. Parallel states are not tested here. The test class tries to test
 * everything, but focus lies on issues which were added since forking from the original stateless4j.
 *
 * @author rinke
 *
 */
public class StateMachineTest extends BaseSMTest {

    private static final Logger LOGGER = (Logger) LoggerFactory.getLogger(StateMachineTest.class);

    /**
     * tests assigning a state as substate of two different parents.
     */
    @Test(expected = IllegalStateException.class)
    public void test00aTwiceSubstate() {
        config.configure("parent");
        config.configure("other");
        config.configure("child1").substateOf("parent");
        config.configure("child1").substateOf("other");
    }

    /**
     * tests assigning a state as substate of an unknown parent
     */
    @Test(expected = IllegalStateException.class)
    public void test00bSubstateUnknownParent() {
        config.configure("parent");
        config.configure("child1").substateOf("other");
    }

    @Test(expected = IllegalStateException.class)
    public void test00cUnknownTrigger() {
        config.configure("state");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        sm.fire("go");
    }

    /**
     * tests correct substates and initial state of a state machine.
     * Structure:
     *
     * <pre>
     * - parent
     * - - c1
     * - - - c1.1
     * - - - - c1.1.1
     * - other
     * </pre>
     *
     * Just making the object should put a new StateMachine in state child1.1.1 (and in all parent states).
     */
    @Test
    public void test01aSubsAndInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1.1.1", sm);
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests the creation of a state machine 100 levels deep.
     */
    @Test
    public void test01bSubsAndInitial() {
        config.configure("parent");
        int i = 1;
        String parent = "parent";
        while (i < 100) {
            final String newLevel = "level" + i;
            config.configure(newLevel).substateOf(parent);
            parent = newLevel;
            i++;
        }
        config.configure("other");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        final StateMachineStatus<String> status = sm.getStateMachineStatus();
        for (int j = 1; j < 100; j++) {
            final String state = "level" + j;
            assertTrue(status.isInState(state));
        }
    }

    /**
     * tests explicit initial state of a state machine.
     */
    @Test
    public void test02aExplicitInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        // config.configure("other");
        config.setInitialState("other");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other", sm);
    }

    /**
     * tests explicit initial state of a state machine, setting an initial state which is already the default initial
     */
    @Test
    public void test02bExplicitInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other");
        config.setInitialState("parent");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1.1.1", sm);
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests explicit initial state of a state machine,
     * setting an initial state of a substate on the state machine (which is already initial)
     */
    @Test
    public void test02cExplicitInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other");
        config.setInitialState("child1");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1.1.1", sm);
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests explicit initial state of a state machine,
     * setting an initial state of a substate on the state machine
     */
    @Test
    public void test02dExplicitInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other");
        config.configure("other.child").substateOf("other");
        config.setInitialState("other.child");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other.child", sm);
        TestUtils.testInStates(sm, "other", "other.child");
    }

    /**
     * tests correct substates and initial state of a state in a state machine.
     * At least one of the initial states is NOT the first state in the parent, so it
     * has been explicitly set as initial.
     */
    @Test
    public void test03ExplicitInitial() {
        config.configure("parent");
        config.configure("child2").substateOf("parent");
        config.configure("child1").initialStateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        // config.configure("other");
        config.setInitialState("other").permit("toParent", "parent", TestUtils.getTransitionAction("toParent"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other", sm);
        sm.fire("toParent", new Object[] {});
        // we are explicitly pointing the destination to parent, but still it should have entered the initial child states.
        TestUtils.testInLeafState("child1.1.1", sm);
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests correct states after transition from leaf to leaf.
     */
    @Test
    public void test11LeafToLeaf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other");
        config.configure("other.child").substateOf("other");
        config.setInitialState("other");
        config.configure("other.child").permit("toChild1.1.1", "child1.1.1", TestUtils.getTransitionAction("toChild1.1.1"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other.child", sm);
        sm.fire("toChild1.1.1", new Object[] {});
        // we are explicitly pointing the destination to parent, but still it should have entered the child states.
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests correct states after transition from non-leaf to leaf.
     */
    @Test
    public void test12NonLeafToLeaf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other");
        config.configure("other.child").substateOf("other");
        final Action<String, String> action = (c, t, p, a) -> {
            LOGGER.info("firing trigger toChild1.1.1");
            assertEquals("other", p);
        };
        config.setInitialState("other").permit("toChild1.1.1", "child1.1.1", action);
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other.child", sm);
        sm.fire("toChild1.1.1", new Object[] {});
        // we are explicitly pointing the destination to parent, but still it should have entered the child states.
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests transition leaf to non leaf
     */
    @Test
    public void test13LeafToNonLeaf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        // config.configure("other");
        config.setInitialState("other").permit("toParent", "parent", TestUtils.getTransitionAction("toParent"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other", sm);
        sm.fire("toParent", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests transition non leaf to non leaf
     */
    @Test
    public void test14NonLeafToNonLeaf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other");
        config.configure("other.child").substateOf("other");
        config.setInitialState("other").permit("toParent", "parent", TestUtils.getTransitionAction("toParent"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other.child", sm);
        sm.fire("toParent", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests transition sub1 to sub2 under parent
     */
    @Test
    public void test15aSubToSubUnderParent() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("child1.1").permit("toChild2", "child2", TestUtils.getTransitionAction("toChild2"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1.1.1", sm);
        sm.fire("toChild2", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child2");
    }

    /**
     * as 15a, with deeper subbing
     */
    @Test
    public void test15bSubToSubUnderParent() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("child1").permit("toChild2", "child2", TestUtils.getTransitionAction("toChild2"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1", sm);
        sm.fire("toChild2", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child2");
    }

    /**
     * transition from non initial leaf state to own parent
     */
    @Test
    public void test16NonInitialToParent() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("child1").permit("toChild2", "child2", TestUtils.getTransitionAction("toChild2"));
        config.configure("child2").permit("toParent", "parent", TestUtils.getTransitionAction("toParent"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1", sm);
        sm.fire("toChild2", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child2");
        sm.fire("toParent", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child1");
    }

    /**
     * transition from non leaf state in which it is, to leaf state which is child of parent, but
     * in which it is not.
     */
    @Test
    public void test17ParentToLeaf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("parent").permit("toChild2", "child2", TestUtils.getTransitionAction("toChild2"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1", sm);
        sm.fire("toChild2", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child2");
    }

    /**
     * transition from non leaf state in which it is, to leaf state which is child of parent,
     * in which it already is.
     */
    @Test
    public void test18aParentToLeaf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("parent").permit("toChild1", "child1", TestUtils.getTransitionAction("toChild1"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1", sm);
        sm.fire("toChild1", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child1");
    }

    /**
     * transition from leaf state in which it is, to the parent of that leaf state (in
     * which it is of course too).
     */
    @Test(expected = IllegalStateException.class)
    public void test18bLeafToParent() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("child1").permit("toParent", "parent", TestUtils.getTransitionAction("toParent"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1", sm);
        sm.fire("toParent", new Object[] {});
        TestUtils.testInStates(sm, "parent", "child1");
    }

    /**
     * transition from leaf state to self
     */
    @Test(expected = IllegalStateException.class)
    public void test19aLeafToSelf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("child1").permit("toChild1", "child1", TestUtils.getTransitionAction("toChild1"));
    }

    /**
     * transition from non-leaf state to self
     */
    @Test(expected = IllegalStateException.class)
    public void test19bNonLeafToSelf() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("parent").permit("toParent", "parent", TestUtils.getTransitionAction("toParent"));
    }

    /**
     * transition from parent to its only possible child
     */
    @Test(expected = IllegalStateException.class)
    public void test19cParentToOnlyChild() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("parent").permit("toChild1.1", "child1.1", TestUtils.getTransitionAction("toChild1.1"));
    }

    /**
     * transition from initial leaf state to own parent
     */
    @Test(expected = IllegalStateException.class)
    public void test19dInitialToParent() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("child1").permit("toParent", "parent", TestUtils.getTransitionAction("toParent"));
    }

    /**
     * trigger matching: unknown trigger, default handling
     */
    @Test(expected = IllegalStateException.class)
    public void test20aUnknownTrigger() {
        config.configure("state");
        config.configure("state2");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("toChild1", new Object[] {});
    }

    /**
     * trigger matching: unknown trigger, custom handling
     */
    @Test
    public void test20bUnknownTrigger() {
        config.configure("state");
        config.configure("state2");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        sm.onUnhandledTrigger((c, t, p, a) -> LOGGER.info("Unhandled Trigger"));
        TestUtils.testInLeafState("state", sm);
        sm.fire("toChild1", new Object[] {});
    }

    /**
     * trigger matching: *
     */
    @Test(expected = IllegalStateException.class)
    public void test21TriggerMatching() {
        config.configure("state");
        config.configure("state2");
        config.configure("state").permit("to2", "state2");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("*", new Object[] {});
        TestUtils.testInLeafState("state2", sm);
    }

    /**
     * tests multiple exit transitions for the same trigger on the same state.
     * The state machine should just pick the first registered. In previous versions,
     * this would raise an exception.
     */
    @Test
    public void test31ConflictingTransitions() {
        config.configure("state");
        config.configure("target");
        config.configure("target2");
        config.configure("target3");
        config.configure("state").permit("go", "target").permit("go", "target2").permit("go", "target3");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("target", sm);
    }

    /**
     * tests multiple exit transitions for the same trigger on the same state.
     * This time the first registered has a failing guard.
     */
    @Test
    public void test32ConflictingTransitions() {
        config.configure("state");
        config.configure("target");
        config.configure("target2");
        config.configure("target3");
        config.configure("state")
                .permitIf("go", "target", (c, a) -> Boolean.FALSE)
                .permit("go", "target2").permit("go", "target3");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("target2", sm);
    }

    /**
     * tests multiple exit transitions for the same trigger on the same state.
     * This time the first registered has ignored fire result
     */
    @Test
    public void test33ConflictingTransitions() {
        config.configure("state");
        config.configure("target");
        config.configure("target2");
        config.configure("target3");
        config.configure("state")
                .permitDynamic("go", (c, a) -> "state", TestUtils.getTransitionAction("toSelf"))
                .permit("go", "target2").permit("go", "target3");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("state", sm);
    }

    /**
     * tests multiple exit transitions for the same trigger on parent/child states.
     * The deepest declared trigger has priority
     */
    @Test
    public void test34ConflictingTransitions() {
        config.configure("parent");
        config.configure("state").substateOf("parent");
        config.configure("target");
        config.configure("target2");
        config.configure("state").permit("go", "target");
        config.configure("parent").permit("go", "target2");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("target", sm);
    }

    /**
     * tests multiple exit transitions for the same trigger on parent/child states.
     * The deepest declared trigger normally has priority, but now has failing guard.
     */
    @Test
    public void test35ConflictingTransitions() {
        config.configure("parent");
        config.configure("state").substateOf("parent");
        config.configure("target");
        config.configure("target2");
        config.configure("state").permitIf("go", "target", (c, a) -> Boolean.FALSE);
        config.configure("parent").permit("go", "target2");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("target2", sm);
    }

    /**
     * tests multiple exit transitions for the same trigger on parent/child states.
     * The deepest declared trigger has priority, even though it has fireResult Ignored.
     */
    @Test
    public void test36ConflictingTransitions() {
        config.configure("parent");
        config.configure("state").substateOf("parent");
        config.configure("target");
        config.configure("target2");
        config.configure("state").permitDynamic("go", (c, a) -> "state", TestUtils.getTransitionAction("toSelf"));
        config.configure("parent").permit("go", "target2");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("state", sm);
    }

    /**
     * a previous, with random choosing
     */
    @Test
    public void test39ConflictingTransitionsWithRandom() {
        config.configure("state");
        config.configure("target1");
        config.configure("target2");
        config.configure("target3");
        config.configure("state")
                .permitIf("go", "target1", TestUtils.randomGuard(0.1))
                .permitIf("go", "target2", TestUtils.randomGuard(0.2))
                .permitIf("go", "target3", TestUtils.randomGuard(0.3));
        config.onFailedConditionTrigger(null);
        final int numberOfMachines = 1_000;
        final List<String> stateScores = new ArrayList<>(numberOfMachines);
        for (int i = 0; i < numberOfMachines; i++) {
            final StateMachine<String, String> machine = new StateMachine<>(config, context);
            machine.fire("go");
            stateScores.add(machine.getStateMachineStatus().getLeafStates().iterator().next());
        }

        final double d1 = Collections.frequency(stateScores, "target1") * 1.0 / numberOfMachines;
        final double d2 = Collections.frequency(stateScores, "target2") * 1.0 / numberOfMachines;
        final double d3 = Collections.frequency(stateScores, "target3") * 1.0 / numberOfMachines;
        final double d4 = Collections.frequency(stateScores, "state") * 1.0 / numberOfMachines;
        assertEquals(0.1, d1, 0.02);
        assertEquals(0.18, d2, 0.05);
        assertEquals(0.216, d3, 0.05);
        assertEquals(0.504, d4, 0.05);
    }

    /**
     * tests a failing guard for a single trigger config, with no alternative for the fail.
     * Default behaviour
     */
    @Test(expected = IllegalStateException.class)
    public void test41FailingGuard() {
        config.configure("state");
        config.configure("target");
        config.configure("state").permitIf("go", "target", (c, a) -> Boolean.FALSE);
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("state", sm);
    }

    /**
     * tests a failing guard for a single trigger config, with no alternative for the fail,
     * custom behaviour.
     */
    @Test
    public void test42FailingGuard() {
        config.configure("state");
        config.configure("target");
        config.configure("state").permitIf("go", "target", (c, a) -> Boolean.FALSE);
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.onFailedConditionTrigger((c, t, p, a) -> LOGGER.info("Failed Guard"));
        sm.fire("go");
        TestUtils.testInLeafState("state", sm);
    }

    /**
     * tests a not failing guard for a single trigger config, with no alternative for the fail,
     */
    @Test
    public void test43NotFailingGuard() {
        config.configure("state");
        config.configure("target");
        config.configure("state").permitIf("go", "target", (c, a) -> Boolean.TRUE);
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.onFailedConditionTrigger((c, t, p, a) -> LOGGER.info("Failed Guard"));
        sm.fire("go");
        TestUtils.testInLeafState("target", sm);
    }

    /**
     * tests if exits are correct. In lepra, all substates of a parent state would
     * exit, even if not active. In this test, this would mean that child2 is reported to
     * be exiting, while the sm never was in child2. This behaviour is not correct.
     */
    @Test
    public void test50CorrectExits() {
        final ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child2").substateOf("parent");
        config.configure("other");
        config.configure("parent").permit("toOther", "other", TestUtils.getTransitionAction("toOther"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);

        final Logger logger = (Logger) TestUtils.LOGGER;
        logger.addAppender(listAppender);

        try {
            sm.fire("toOther");
            // now the listAppender should NOT contain the "State child2: exiting" message
            for (final ILoggingEvent event : listAppender.list) {
                assertFalse(event.getMessage().contains("State child2: exiting"));
            }
        } finally {
            LOGGER.detachAppender(listAppender);
        }
    }

    /**
     * tests correct substates and initial state of a state in a state machine
     * in case of dynamic destinations. In such a case, config cannot determine
     * the redirecting to the appropriate leaf state (when pointing to a parent), so
     * that must be done runtime by the StateMachine class itself.
     * This tests from leaf to non-leaf
     */
    @Test
    public void test65RunTimeInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        final Func<String, String, String> func = (c, a) -> "parent";
        config.setInitialState("other").permitDynamic("toParent", func,
                TestUtils.getTransitionAction("toParent"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other", sm);
        sm.fire("toParent", new Object[] {});
        // we are explicitly pointing the destination to parent, but still it should
        // have entered the initial child states.
        TestUtils.testInLeafState("child1.1.1", sm);
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests what happens if a dynamic trigger points to self of a leaf state.
     */
    @Test
    public void test66DynamicSelf() {
        config.configure("parent");
        config.configure("other");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        final Func<String, String, String> func = (c, a) -> "child1.1.1";
        config.configure("child1.1.1").substateOf("child1.1").permitDynamic("toSelf", func,
                TestUtils.getTransitionAction("toSelf"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("child1.1.1", sm);
        sm.fire("toSelf", new Object[] {});
        TestUtils.testInLeafState("child1.1.1", sm);
        TestUtils.testInStates(sm, "parent", "child1", "child1.1", "child1.1.1");
    }

    /**
     * tests the permitReentry method for a leaf state.
     * Check the onentry/onexit messages visually
     */
    @Test
    public void test70PermitReentry() {
        config.configure("parent");
        config.configure("state").substateOf("parent").permitReentry("go", TestUtils.getTransitionAction("Reentry"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("state", sm);
    }

    /**
     * tests the permitReentry method for a non-leaf state, initial leaf.
     * Check the onentry/onexit messages visually
     */
    @Test
    public void test71PermitReentry() {
        config.configure("parent");
        config.configure("subParent").substateOf("parent");
        config.configure("state").substateOf("subParent");
        config.configure("subParent").permitReentry("go", TestUtils.getTransitionAction("Reentry"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("state", sm);
        sm.fire("go");
        TestUtils.testInLeafState("state", sm);
    }

    /**
     * tests the permitReentry method for a non-leaf state, non-initial leaf.
     * Check the onentry/onexit messages visually
     */
    @Test
    public void test72PermitReentry() {
        config.configure("start");
        config.configure("parent");
        config.configure("subParent").substateOf("parent");
        config.configure("state1").substateOf("subParent");
        config.configure("state2").substateOf("subParent");
        config.configure("start").permit("init", "state2");
        config.configure("subParent").permitReentry("go", TestUtils.getTransitionAction("Reentry"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("start", sm);
        sm.fire("init");
        TestUtils.testInLeafState("state2", sm);
        sm.fire("go");
        TestUtils.testInLeafState("state1", sm);
    }

    /**
     * tests the permitReentry method for a non-leaf state in a parallel setting
     * Is not implemented yet, so should throw exception.
     */
    @Test(expected = IllegalStateException.class)
    public void test73PermitReentryParallel() {
        config.configure("parent");
        config.configure("subParent").substateOf("parent");
        config.parallel("parallel").substateOf("subParent");
        config.configure("p1").substateOf("parallel");
        config.configure("p2").substateOf("parallel");
        config.configure("subParent").permitReentry("go", TestUtils.getTransitionAction("Reentry"));
    }

    /**
     * but permitReentry in a nested statemachine not having any child state machines should not
     * be a problem.
     */
    @Test
    public void test74PermitReentryInParallelSub() {
        config.configure("parent");
        config.parallel("parallel").substateOf("parent");
        config.configure("p1").substateOf("parallel");
        config.configure("p2").substateOf("parallel");
        config.configure("state").substateOf("p1").permitReentry("go", TestUtils.getTransitionAction("Reentry"));
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        sm.fire("go");
        TestUtils.testInStates(sm, "parent", "parallel", "p1", "p2", "state");
    }

    /**
     * tests configuration in the wrong order: first a trigger is defined which points to a parent
     * state, so the initial state of that state is used to redirect the trigger destination.
     * Then the initial state of that parent state is reset.
     */
    @Test(expected = IllegalStateException.class)
    public void test81WrongOrderInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.configure("other").permit("toParent", "parent");
        config.configure("child1.1.2").initialStateOf("child1.1");
    }

    /**
     * tests the freezing of states.
     */
    @Test(expected = IllegalStateException.class)
    public void test82RunTimeInitial() {
        config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        final Func<String, String, String> func = (c, a) -> "parent";
        config.configure("other").permitDynamic("toParent", func,
                TestUtils.getTransitionAction("toParent"));
        config.setInitialState("other");
        final StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInLeafState("other", sm);
        sm.fire("toParent", new Object[] {});
        // we are explicitly pointing the destination to parent, but still it should have entered the initial child states.
        TestUtils.testInLeafState("child1.1.1", sm);
    }

    /**
     * tests the failedConditionTriggerAction setting on the state machine.
     */
    @Test
    public void test83customFailedConditionTriggerAction() {
        final Used used = new Used();
        config.configure("state");
        config.configure("target1");
        config.configure("state").permitIf("go", "target1", TestUtils.randomGuard(0.0),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertTrue(used.used);
    }

    /**
     * tests ignored trigger
     */
    @Test
    public void test90IgnoredTrigger() {
        config.configure("state");
        config.configure("target1");
        config.configure("state").ignore("go");
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
    }

    /**
     * tests ignored trigger with condition passing
     */
    @SuppressWarnings("hiding")
    @Test
    public void test91IgnoredTriggerIfPassing() {
        config.configure("state");
        config.configure("target1");
        config.configure("state").ignoreIf("go", (context, args) -> Boolean.TRUE);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
    }

    /**
     * tests ignored trigger with condition passing
     */
    @SuppressWarnings("hiding")
    @Test(expected = IllegalStateException.class)
    public void test92IgnoredTriggerIfFailing() {
        config.configure("state");
        config.configure("target1");
        config.configure("state").ignoreIf("go", (context, args) -> Boolean.FALSE);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
    }

    /**
     * tests ignored trigger with action
     */
    @Test
    public void test93IgnoredTriggerAction() {
        final Used used = new Used();
        config.configure("state");
        config.configure("target1");
        config.configure("state").ignore("go", (c, t, p, a) -> used.used = true);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertTrue(used.used);
    }

    /**
     * tests ignored trigger with guard passing and action
     */
    @Test
    public void test94IgnoredTriggerActionPassingGuard() {
        final Used used = new Used();
        config.configure("state");
        config.configure("target1");
        config.configure("state").ignoreIf("go",
                (cntxt, args) -> Boolean.TRUE,
                (c, t, p, a) -> used.used = true);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertTrue(used.used);
    }

    /**
     * tests ignored trigger with guard failing and action
     */
    @Test
    public void test95IgnoredTriggerActionFalingGuard() {
        final Used used = new Used();
        config.onFailedConditionTrigger((c, t, p, a) -> {
        });
        config.configure("state");
        config.configure("target1");
        config.configure("state").ignoreIf("go",
                (cntxt, args) -> Boolean.FALSE,
                (c, t, p, a) -> used.used = true);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
    }

    /**
     * ignored trigger in deep parallel
     */
    @Test
    public void test96IgnoredTriggerDeepParallel() {
        final Used used = new Used();
        config.parallel("parallelParent");
        config.configure("p1").substateOf("parallelParent");
        config.configure("p2").substateOf("parallelParent");
        config.configure("state").substateOf("p1");
        config.configure("state").ignoreIf("go",
                (cntxt, args) -> Boolean.TRUE,
                (c, t, p, a) -> used.used = true);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertTrue(used.used);
    }

    /**
     * tests context property and a listener; sets the listener to the context.
     */
    @SuppressWarnings("boxing")
    @Test
    public void testz100Property() {
        config.configure("parent");
        config.configure("state1").substateOf("parent");
        config.configure("state2").substateOf("parent");
        config.configure("state1").permit("go", "state2", (cntxt, t, p, a) -> cntxt.setAttribute("aap", 3));
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        final IContextPropertyListener listener = new ExampleContextPropertyListener();
        context.addListener("aap", listener);
        machine.fire("go");
    }

    /**
     * tests context property and a listener; sets the listener to the config.
     */
    @SuppressWarnings("boxing")
    @Test
    public void testz101Property() {
        config.configure("parent");
        config.configure("state1").substateOf("parent");
        config.configure("state2").substateOf("parent");
        config.configure("state1").permit("go", "state2", (cntxt, t, p, a) -> cntxt.setAttribute("aap", 3));
        final IContextPropertyListener listener = new ExampleContextPropertyListener();
        config.addListener("aap", listener);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
    }

    /**
     * tests context collection property and a listener; sets the listener to the context.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testz102CollectionProperty() {
        config.configure("parent");
        config.configure("state1").substateOf("parent");
        config.configure("state2").substateOf("parent");
        config.configure("state3").substateOf("parent");
        String[] add = { "aap", "beer" };
        Set<String> coll = Sets.newHashSet(add);
        config.configure("state1").permit("go", "state2",
                (cntxt, t, p, a) -> cntxt.changeCollectionAttribute("coll", coll, null));
        String[] rmv = { "aap" };
        Set<String> rmvColl = Sets.newHashSet(rmv);
        config.configure("state2").permit("go2", "state3",
                (cntxt, t, p, a) -> cntxt.changeCollectionAttribute("coll", null, rmvColl));
        config.configure("state3").permit("go3", "state1",
                (cntxt, t, p, a) -> cntxt.clearCollectionAttribute("coll"));

        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        final IContextPropertyListener listener = new ExampleContextPropertyListener();
        context.addListener("coll", listener);
        machine.fire("go");
        Collection<String> stored = (Collection<String>) machine.getStateMachineContext().getAttribute("coll");
        assertTrue(stored.contains("aap"));
        assertEquals(2, stored.size());
        machine.fire("go2");
        stored = (Collection<String>) machine.getStateMachineContext().getAttribute("coll");
        assertEquals(1, stored.size());
        machine.fire("go3");
        stored = (Collection<String>) machine.getStateMachineContext().getAttribute("coll");
        assertEquals(0, stored.size());
    }

}
