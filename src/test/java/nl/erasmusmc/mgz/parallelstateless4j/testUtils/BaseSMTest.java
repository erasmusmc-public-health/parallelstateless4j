/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.testUtils;

import org.junit.Before;

import nl.erasmusmc.mgz.parallelstateless4j.DefaultStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineConfigurer;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.StateConfigurer;

/**
 * Parent class of all state machine tests
 *
 * @author rinke
 *
 */
public class BaseSMTest {

    public class Used {
        public boolean used = false;
    }

    protected class TestMachineConfigurer extends MachineConfigurer<String, String> {

        private TestMachineConfigurer() {
            super();
        }

        /**
         * calls the standard configuere(state), but adds an entering and exiting action for logging.
         * Note that multiple calls to configure(state) with the same state, will lead to multiple
         * equal log messages. Anticipating on this in the code would slow down the tests considerably.
         */
        @Override
        public StateConfigurer<String, String> configure(final String state) {
            StateConfigurer<String, String> result = super.configure(state);
            result.onEntry(TestUtils.logAction("entering"))
                    .onExit(TestUtils.logAction("exiting"));
            return result;
        }

        @Override
        public StateConfigurer<String, String> parallel(final String state) {
            StateConfigurer<String, String> result = super.parallel(state);
            result.onEntry(TestUtils.logAction("enteringParallel"))
                    .onExit(TestUtils.logAction("exitingParallel"));
            return result;
        }

    }

    protected TestMachineConfigurer                config;
    protected IStateMachineContext<String, String> context = new DefaultStateMachineContext<>();

    @Before
    public void setup() {
        config = new TestMachineConfigurer();
    }

}
