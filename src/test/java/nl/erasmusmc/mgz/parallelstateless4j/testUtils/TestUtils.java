/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.testUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.StateMachine;
import nl.erasmusmc.mgz.parallelstateless4j.StateMachineStatus;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.StateRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;

/**
 * Utility methods to use in test classes. These are general utility methods, so they have no knowledge
 * about any of the other modules, not even EventManager.
 *
 * @author rinke
 */
public class TestUtils implements Serializable {

    private static final long   serialVersionUID = -9081081605995962928L;

    private static final String formatPattern    = "#,##0.000";

    public static final Logger  LOGGER           = LoggerFactory.getLogger(TestUtils.class);

    // 0.001 level, 2 sided
    public static final double  Z_LEVEL          = 3.2905;

    public static final double  DELTA            = 0.000001;

    private static Random       random           = new Random(0);

    /**
     * Allows to run a private method on a class. Any encountered exception will result in failing
     * the test from which this method is called.
     *
     * @param claz - the class on which the private method is declared.
     * @param instance - the instance of the class on which the method is to be invoked. Use null
     * in case of a static method.
     * @param methodName - the name of the method.
     * @param parameterTypes - an array indicating the types of the parameters.
     * @param args - the actual parameters which are passed to the method.
     * @return the method result, or null if the action didn't succeed.
     */
    public static <T> Object executePrivateMethod(Class<T> claz, T instance, String methodName,
            Class<?>[] parameterTypes, Object... args) {
        Method m = null;
        try {
            m = claz.getDeclaredMethod(methodName, parameterTypes);
            m.setAccessible(true);
            return m.invoke(instance, args);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            e.printStackTrace();
            throw new TestReflectionException();
        }
    }

    /**
     * creates a guard function that always returns true or false, depending on the argument.
     */
    public static Func<Boolean, String, String> fixedGuard(final boolean result) {
        return (context, args) -> result;
    }

    /**
     * formats a double as a string, to the "#,##0.000" pattern.
     */
    public final static String format(double value) {
        return format(value, formatPattern);
    }

    /**
     * formats a double to a string, according to the given pattern. The pattern
     * is used via the java.text.DecimalFormat class.
     * @param value
     * @param pattern
     */
    public final static String format(double value, String pattern) {
        final DecimalFormat myFormatter = new DecimalFormat(pattern);
        final String output = myFormatter.format(value);
        return output;
    }

    /**
     * gets the value of a private field via reflection. Fails the test from which it is called
     * in case of any exception encountered.
     * <p>
     * Usage example:<br>
     * <code>HashSet<String> exitedStates = (HashSet<String>) getPrivateFieldValue(Human.class, (Human) female, "exitedStates");</code>
     *
     * @param claz - the class on which the private field is declared.
     * @param instance - the instance of that class from which the private field
     * value is to be retrieved. Note that the declared class must
     * match the first parameter; if not, cast it to the first
     * parameter. May be null, in which case a static field is accessed.
     * @param fieldName - the name of the field which value must be retrieved.
     * @return the value of the private field value, or null in case the action did not succeed.
     */
    public static <T> Object getPrivateFieldValue(Class<T> claz, T instance, String fieldName) {
        Field field = null;
        try {
            field = claz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(instance);
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException ex) {
            ex.printStackTrace();
            throw new TestReflectionException();
        }
    }

    /**
     * returns the stacktrace of the calling point up to n lines back. This method
     * makes it possible to print stacktraces without exceptions being thrown.
     *
     * @param n
     */
    public static String getStackTrace(int n) {
        final StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        final StringBuilder str = new StringBuilder("StackTrace: \n\r");
        int i = 0;
        for (final StackTraceElement element : stackTraceElements) {
            if (++i > n) {
                break;
            }
            str.append("     ");
            str.append(element.getClassName()).append("#");
            str.append(element.getMethodName()).append(":");
            str.append(element.getLineNumber()).append("\n\r");
        }
        str.append("\n\r");
        return str.toString();
    }

    public static Action<String, String> getTransitionAction(String trigger) {
        return (c, t, p, a) -> {
            LOGGER.info("firing trigger " + trigger);
        };
    }

    public static Action<String, String> logAction(String message) {
        return (c, t, p, a) -> LOGGER.info("State " + p.toString() + ": " + message);
    }

    /**
     * creates a guard function which passes <code>fraction</code> of the cases.
     */
    public static Func<Boolean, String, String> randomGuard(double fraction) {
        return (context, args) -> {
            final double rand = random.nextDouble();
            return rand < fraction;
        };
    }

    /**
     * sets the value of a private field via reflection.
     *
     * @param claz the class of the object from which the private field is to be retrieved.
     * @param instance - the instance of that class from which the private field value is to be retrieved. Note that the
     * declared class must match the first parameter; if not, cast it to the first parameter. Use null in case of static fields.
     * @param fieldName the name of the field which value must be retrieved.
     * @param value the value to be set to the field, as an object.
     */
    public static <T> void setPrivateFieldValue(Class<T> claz, T instance, String fieldName, Object value) {
        Field field = null;
        try {
            field = claz.getDeclaredField(fieldName);
            field.setAccessible(true);
        } catch (NoSuchFieldException | SecurityException ex) {
            ex.printStackTrace();
            throw new TestReflectionException();
        }
        try {
            field.set(instance, value);
        } catch (final IllegalAccessException ex) {
            throw new TestReflectionException("Cannot set field value via reflection for test");
        }
    }

    public static void testInLeafState(String expectedState, StateMachine<String, String> stateMachine) {
        final StateRegistry<String, String> rep = stateMachine.getCurrentStateRegistryNotNull();
        assertEquals(expectedState, rep.getUnderlyingState());
    }

    /**
     * tests that the state machine is in the state; doesn't have to be a leaf state.
     */
    public static void testInState(String expectedState, StateMachine<String, String> stateMachine) {
        final StateMachineStatus<String> status = stateMachine.getStateMachineStatus();
        assertTrue(status.isInState(expectedState));
    }

    /**
     * tests that the statemachine is in ALL given states.
     */
    public static void testInStates(StateMachine<String, String> stateMachine, String... states) {
        final StateMachineStatus<String> status = stateMachine.getStateMachineStatus();
        for (final String state : states) {
            assertTrue(status.isInState(state));
        }
    }

    private TestUtils() {
    }

}
