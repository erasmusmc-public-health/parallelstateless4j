/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.function.Function;

import org.junit.Before;
import org.junit.Test;

import nl.erasmusmc.mgz.parallelstateless4j.DefaultStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.StateMachine;
import nl.erasmusmc.mgz.parallelstateless4j.testUtils.TestUtils;

/**
 * tests the configuration of parallel states.
 * <p>
 * The following fields are involved in parallelism; they always need to be tested on the right settings:
 * <ul>
 * <li>StateMachineConfig:
 * <ul>
 * <li>stateConfiguration map must have correct states (no states in substatemachines)
 * <li>childStateMachines multimap must have correct elements
 * <li>initialstate must belong to this configuration.
 * </ul>
 * <li>ParallelStateMachineConfig:
 * <ul>
 * <li>all fields of the StateMachineConfig super class must be correct
 * <li>parentConfiguration must point to correct parent
 * <li>hostingStateInParent must be set correctly.
 * <li>intial state must be set correctly
 * </ul>
 * <li>StateConfigurationHelper:
 * <ul>
 * <li>hostingConfig must be set correctly
 * <li>lookup must point to the correct hosting config.
 * </ul>
 * <li>StateRepresentation
 * <ul>
 * <li>substates must be correct
 * <li>superstate ....
 * <li>parentStateMachineSuperState
 * <li>initialState
 * <li>parallelStateMachineConfigs
 * </ul>
 * </ul>
 *
 *
 * @author rinke
 *
 */
public class ParallelConfigurationTest {

    private MachineConfigurer<String, String>    config;
    private IStateMachineContext<String, String> context = new DefaultStateMachineContext<>();
    private MachineRegistry<String, String>      reg;

    @Before
    public void setup() {
        config = new MachineConfigurer<>();
        reg = config.getRegistry();
    }

    /**
     * tests parallel states retrieval of a state machine with nested state machines.
     * This tests the most simple configuration, where the state machine initializes with
     * 2 parallel states.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void test01simpleInitial() {
        setupSimpleParallelInitial();
        // test StateMachineConfig/Registry fields
        assertEquals(1, reg.getStates().size());
        assertEquals(3, reg.getAllStates().size());
        assertEquals(2, reg.getSubStateMachineStates().size());
        assertTrue(reg.isParallelState("simpleParallel"));
        assertFalse(reg.isParallelState("sp2"));
        assertEquals("simpleParallel", reg.getInitialStateRegistry().getUnderlyingState());
        assertNull(config.getParallelContainingStateEntry(""));
        assertNull(config.getParallelContainingStateEntry("simpleParallel"));
        assertEquals("simpleParallel", config.getParallelContainingStateEntry("sp1").getKey());
        // test ParallelStateMachineConfig/Registry fields
        ParallelMachineConfigurer<String, String> parallelConfig = config.getParallelContainingStateEntry("sp1").getValue();
        ParallelRegistry<String, String> parallelReg = parallelConfig.getRegistry();
        assertEquals(1, parallelReg.getStates().size());
        assertEquals(2, parallelReg.getAllStates().size());
        assertEquals(1, parallelReg.getAllStatesNotParent().size());
        assertEquals(0, parallelReg.getSubStateMachineStates().size());
        assertEquals("sp1", parallelReg.getInitialStateRegistry().getUnderlyingState());
        assertFalse(parallelReg.isParallelState("simpleParallel"));
        assertFalse(parallelReg.isParallelState("sp1"));
        assertEquals(config, parallelConfig.getParentConfiguration());
        assertEquals("simpleParallel", parallelReg.getHostingStateInParent());
        // test StateConfigurationHelper fields (hostingconfig, lookup)
        StateConfigurer<String, String> helper = config.configure("sp1");
        MachineConfigurer tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(
                StateConfigurer.class,
                helper, "hostingConfig");
        assertEquals(parallelConfig, tempConfig);
        Function lookup = (Function) TestUtils.getPrivateFieldValue(StateConfigurer.class, helper, "lookup");
        assertEquals("sp1", ((StateRegistry<String, String>) lookup.apply("sp1")).getUnderlyingState());
        // test StateRepresentation fields
        StateRegistry sp1 = config.getOrCreateStateRegistry("sp1");
        assertNull(sp1.getDirectSuperstate());
        assertEquals("simpleParallel", sp1.getSuperstateIncludingParentStateMachine().getUnderlyingState());
        StateRegistry simpleParallel = config.getOrCreateStateRegistry("simpleParallel");
        assertEquals(0, simpleParallel.getDirectSubstates().size());
        assertEquals(2, simpleParallel.getParallelRegistries().size());
        assertNull(simpleParallel.getInitialState());
        // test instance
        StateMachine<String, String> instance = instantiate();
        assertEquals(3, instance.getStateMachineStatus().getStates().size());
    }

    /**
     * tests parallel states retrieval of a state machine with nested state machines.
     * This tests the same as previous test, only the parallel states are reached via a
     * transition to the parent state.
     */
    @Test
    public void test02simple() {
        setupSimpleParallelViaTrigger();
        // test StateMachineConfig fields
        assertEquals(2, reg.getStates().size());
        assertEquals(4, reg.getAllStates().size());
        assertEquals(2, reg.getSubStateMachineStates().size());
        // test ParallelStateMachineConfig fields
        ParallelMachineConfigurer<String, String> parallelConfig = config.getParallelContainingStateEntry("sp1").getValue();
        ParallelRegistry<String, String> parallelReg = parallelConfig.getRegistry();
        assertEquals(1, parallelReg.getStates().size());
        assertEquals(3, parallelReg.getAllStates().size());
        assertEquals(1, parallelReg.getAllStatesNotParent().size());
        assertEquals(0, parallelReg.getSubStateMachineStates().size());
        // test instance
        StateMachine<String, String> instance = instantiate();
        assertEquals(1, instance.getStateMachineStatus().getStates().size());
        instance.fire("go");
        assertEquals(3, instance.getStateMachineStatus().getStates().size());
    }

    /**
     * tests parallel states as a substate of normal state; in an initial configuration.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void test03ChildInitial() {
        setupChildParallelInitial();
        // test StateMachineConfig fields
        assertEquals(2, reg.getStates().size());
        assertEquals(4, reg.getAllStates().size());
        assertEquals(2, reg.getSubStateMachineStates().size());
        assertTrue(reg.isParallelState("parallel"));
        assertFalse(reg.isParallelState("p2"));
        assertEquals("parent", reg.getInitialStateRegistry().getUnderlyingState());
        assertNull(config.getParallelContainingStateEntry(""));
        assertNull(config.getParallelContainingStateEntry("parallel"));
        assertEquals("parallel", config.getParallelContainingStateEntry("p1").getKey());
        // test ParallelStateMachineConfig fields
        ParallelMachineConfigurer<String, String> parallelConfig = config.getParallelContainingStateEntry("p1").getValue();
        ParallelRegistry<String, String> parallelReg = parallelConfig.getRegistry();
        assertEquals(1, parallelReg.getStates().size());
        assertEquals(3, parallelReg.getAllStates().size());
        assertEquals(1, parallelReg.getAllStatesNotParent().size());
        assertEquals(0, parallelReg.getSubStateMachineStates().size());
        assertEquals("p1", parallelReg.getInitialStateRegistry().getUnderlyingState());
        assertFalse(parallelReg.isParallelState("parallel"));
        assertFalse(parallelReg.isParallelState("p1"));
        assertEquals(config, parallelConfig.getParentConfiguration());
        assertEquals("parallel", parallelReg.getHostingStateInParent());
        // test StateConfigurationHelper fields (hostingconfig, lookup)
        StateConfigurer<String, String> helper = config.configure("p1");
        MachineConfigurer tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(
                StateConfigurer.class,
                helper, "hostingConfig");
        assertEquals(parallelConfig, tempConfig);
        Function lookup = (Function) TestUtils.getPrivateFieldValue(StateConfigurer.class, helper, "lookup");
        assertEquals("p1", ((StateRegistry<String, String>) lookup.apply("p1")).getUnderlyingState());
        // test StateRepresentation fields
        StateRegistry p1 = config.getOrCreateStateRegistry("p1");
        assertNull(p1.getDirectSuperstate());
        assertEquals("parallel", p1.getSuperstateIncludingParentStateMachine().getUnderlyingState());
        StateRegistry parallel = config.getOrCreateStateRegistry("parallel");
        assertEquals(0, parallel.getDirectSubstates().size());
        assertEquals(2, parallel.getParallelRegistries().size());
        assertNull(parallel.getInitialState());
        // test instance
        StateMachine<String, String> instance = instantiate();
        assertEquals(4, instance.getStateMachineStatus().getStates().size());
    }

    /**
     * tests parallel states as a substate of normal state.
     * This tests the same as previous test, only the parallel states are reached via a
     * transition to the parent state.
     *
     */
    @Test
    public void test04Child() {
        setupChildParallelViaTrigger();
        // test StateMachineConfig fields
        assertEquals(3, reg.getStates().size());
        assertEquals(5, reg.getAllStates().size());
        assertEquals(2, reg.getSubStateMachineStates().size());
        // test ParallelStateMachineConfig fields
        ParallelMachineConfigurer<String, String> parallelConfig = config.getParallelContainingStateEntry("p1").getValue();
        ParallelRegistry<String, String> parallelReg = parallelConfig.getRegistry();
        assertEquals(1, parallelReg.getStates().size());
        assertEquals(4, parallelReg.getAllStates().size());
        assertEquals(1, parallelReg.getAllStatesNotParent().size());
        assertEquals(0, parallelReg.getSubStateMachineStates().size());
        // test instance
        StateMachine<String, String> instance = instantiate();
        assertEquals(1, instance.getStateMachineStatus().getStates().size());
        instance.fire("go");
        assertEquals(4, instance.getStateMachineStatus().getStates().size());
    }

    /**
     * tests parallel states as a substate of parallel state; in an initial configuration.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void test05NestedParallelInitial() {
        setupDirectlyNestedParallelInitial();
        // test StateMachineConfig fields
        assertEquals(1, reg.getStates().size());
        assertEquals(5, reg.getAllStates().size());
        assertEquals(4, reg.getSubStateMachineStates().size());
        assertTrue(reg.isParallelState("parallel"));
        assertFalse(reg.isParallelState("nestedParallel"));
        assertEquals("parallel", reg.getInitialStateRegistry().getUnderlyingState());
        assertNull(config.getParallelContainingStateEntry(""));
        assertNull(config.getParallelContainingStateEntry("parallel"));
        assertEquals("parallel", config.getParallelContainingStateEntry("nestedParallel").getKey());
        assertEquals("parallel", config.getParallelContainingStateEntry("c2").getKey());
        // test ParallelStateMachineConfig fields
        ParallelMachineConfigurer<String, String> parallelConfig = config.getParallelContainingStateEntry("c1").getValue();
        ParallelRegistry<String, String> parallelReg = parallelConfig.getRegistry();
        ParallelMachineConfigurer<String, String> parallelConfig1 = config.getParallelContainingStateEntry("nestedParallel")
                .getValue();
        parallelConfig1.getRegistry();
        assertNotNull(parallelConfig);
        assertEquals(parallelConfig, parallelConfig1);
        assertEquals(1, parallelReg.getStates().size());
        assertEquals(4, parallelReg.getAllStates().size());
        assertEquals(3, parallelReg.getAllStatesNotParent().size());
        assertEquals(2, parallelReg.getSubStateMachineStates().size());
        assertEquals("nestedParallel", parallelReg.getInitialStateRegistry().getUnderlyingState());
        assertFalse(parallelReg.isParallelState("parallel"));
        assertFalse(parallelReg.isParallelState("c1"));
        assertTrue(parallelReg.isParallelState("nestedParallel"));
        assertEquals(config, parallelConfig.getParentConfiguration());
        assertEquals("parallel", parallelReg.getHostingStateInParent());
        // same, nested
        ParallelMachineConfigurer<String, String> nestedParallelConfig = parallelConfig.getParallelContainingStateEntry("c1")
                .getValue();
        ParallelRegistry<String, String> nestedParallelReg = nestedParallelConfig.getRegistry();

        assertEquals(1, nestedParallelReg.getStates().size());
        assertEquals(3, nestedParallelReg.getAllStates().size());
        assertEquals(1, nestedParallelReg.getAllStatesNotParent().size());
        assertEquals(0, nestedParallelReg.getSubStateMachineStates().size());
        assertEquals("c1", nestedParallelReg.getInitialStateRegistry().getUnderlyingState());
        assertEquals(parallelConfig, nestedParallelConfig.getParentConfiguration());
        assertEquals("nestedParallel", nestedParallelReg.getHostingStateInParent());

        // test StateConfigurationHelper fields (hostingconfig, lookup)
        StateConfigurer<String, String> helperC1 = config.configure("c1");
        MachineConfigurer tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(
                StateConfigurer.class,
                helperC1, "hostingConfig");
        assertEquals(nestedParallelConfig, tempConfig);
        Function lookup = (Function) TestUtils.getPrivateFieldValue(StateConfigurer.class, helperC1, "lookup");
        assertEquals("c1", ((StateRegistry<String, String>) lookup.apply("c1")).getUnderlyingState());

        StateConfigurer<String, String> helperNestedParallel = config.configure("nestedParallel");
        tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(StateConfigurer.class,
                helperNestedParallel, "hostingConfig");
        assertEquals(parallelConfig, tempConfig);

        // test StateRepresentation fields
        StateRegistry c1 = config.getOrCreateStateRegistry("c1");
        assertNull(c1.getDirectSuperstate());
        assertEquals("nestedParallel", c1.getSuperstateIncludingParentStateMachine().getUnderlyingState());

        StateRegistry nestedParallel = config.getOrCreateStateRegistry("nestedParallel");
        assertNull(nestedParallel.getDirectSuperstate());
        assertEquals("parallel", nestedParallel.getSuperstateIncludingParentStateMachine().getUnderlyingState());

        // test instance
        StateMachine<String, String> instance = instantiate();
        assertEquals(5, instance.getStateMachineStatus().getStates().size());
    }

    /**
     * tests a complex parallel configuration with multiple deeply nested parallel states.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void test06ComplexParallelInitial() {
        setupComplexParallelInitial();
        // test StateMachineConfig fields
        assertEquals(3, reg.getStates().size());
        assertEquals(12, reg.getAllStates().size());
        assertEquals(9, reg.getSubStateMachineStates().size());
        assertTrue(reg.isParallelState("deeperParallel"));
        assertFalse(reg.isParallelState("pc1"));
        assertFalse(reg.isParallelState("pc2.2"));
        assertEquals("toplevel", reg.getInitialStateRegistry().getUnderlyingState());
        assertEquals("deeperParallel", config.getParallelContainingStateEntry("pc1.2").getKey());
        assertEquals("deeperParallel", config.getParallelContainingStateEntry("pc2.2b").getKey());

        // test ParallelStateMachineConfig fields
        ParallelMachineConfigurer<String, String> parallelConfigPc1 = config.getParallelContainingStateEntry("pc1.2")
                .getValue();
        ParallelRegistry<String, String> parallelRegPc1 = parallelConfigPc1.getRegistry();
        assertEquals(3, parallelRegPc1.getStates().size());
        assertEquals(6, parallelRegPc1.getAllStates().size());
        assertEquals(3, parallelRegPc1.getAllStatesNotParent().size());
        assertEquals(0, parallelRegPc1.getSubStateMachineStates().size());
        assertEquals("pc1", parallelRegPc1.getInitialStateRegistry().getUnderlyingState());
        assertFalse(parallelRegPc1.isParallelState("parallel"));

        ParallelMachineConfigurer<String, String> parallelConfigPc2 = config.getParallelContainingStateEntry("pc2.1")
                .getValue();
        ParallelMachineConfigurer<String, String> tempParallelConfig = config.getParallelContainingStateEntry("pc2.2b")
                .getValue();
        ParallelRegistry<String, String> parallelRegPc2 = parallelConfigPc2.getRegistry();
        assertEquals(parallelConfigPc2, tempParallelConfig);
        assertEquals(3, parallelRegPc2.getStates().size());
        assertEquals(9, parallelRegPc2.getAllStates().size());
        assertEquals(6, parallelRegPc2.getAllStatesNotParent().size());
        assertEquals(3, parallelRegPc2.getSubStateMachineStates().size());
        assertEquals("pc2", parallelRegPc2.getInitialStateRegistry().getUnderlyingState());
        assertTrue(parallelRegPc2.isParallelState("pc2.2"));
        assertEquals(config, parallelConfigPc2.getParentConfiguration());
        assertEquals("deeperParallel", parallelRegPc2.getHostingStateInParent());

        ParallelMachineConfigurer<String, String> parallelConfigPc22b = parallelConfigPc2
                .getParallelContainingStateEntry("pc2.2b1").getValue();
        ParallelRegistry<String, String> parallelRegPc22b = parallelConfigPc22b.getRegistry();
        assertEquals(2, parallelRegPc22b.getStates().size());
        assertEquals(8, parallelRegPc22b.getAllStates().size());
        assertEquals(2, parallelRegPc22b.getAllStatesNotParent().size());
        assertEquals(0, parallelRegPc22b.getSubStateMachineStates().size());
        assertEquals("pc2.2b", parallelRegPc22b.getInitialStateRegistry().getUnderlyingState());
        assertEquals(parallelConfigPc2, parallelConfigPc22b.getParentConfiguration());
        assertEquals("pc2.2", parallelRegPc22b.getHostingStateInParent());

        // test StateConfigurationHelper fields (hostingconfig, lookup)
        StateConfigurer<String, String> helperParent = config.configure("parent");
        MachineConfigurer tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(
                StateConfigurer.class,
                helperParent, "hostingConfig");
        assertEquals(config, tempConfig);
        Function lookup = (Function) TestUtils.getPrivateFieldValue(StateConfigurer.class, helperParent, "lookup");
        assertEquals("parent", ((StateRegistry<String, String>) lookup.apply("parent")).getUnderlyingState());

        StateConfigurer<String, String> helperPc11 = config.configure("pc1.1");
        tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(StateConfigurer.class,
                helperPc11, "hostingConfig");
        assertEquals(parallelConfigPc1, tempConfig);
        lookup = (Function) TestUtils.getPrivateFieldValue(StateConfigurer.class, helperPc11, "lookup");
        assertEquals("pc1.1", ((StateRegistry<String, String>) lookup.apply("pc1.1")).getUnderlyingState());

        StateConfigurer<String, String> helperPc21 = config.configure("pc2.1");
        tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(StateConfigurer.class,
                helperPc21, "hostingConfig");
        assertEquals(parallelConfigPc2, tempConfig);
        lookup = (Function) TestUtils.getPrivateFieldValue(StateConfigurer.class, helperPc21, "lookup");
        assertEquals("pc2.1", ((StateRegistry<String, String>) lookup.apply("pc2.1")).getUnderlyingState());

        StateConfigurer<String, String> helperPc22b1 = config.configure("pc2.2b1");
        tempConfig = (MachineConfigurer) TestUtils.getPrivateFieldValue(StateConfigurer.class,
                helperPc22b1, "hostingConfig");
        assertEquals(parallelConfigPc22b, tempConfig);
        lookup = (Function) TestUtils.getPrivateFieldValue(StateConfigurer.class, helperPc22b1, "lookup");
        assertEquals("pc2.2b1", ((StateRegistry<String, String>) lookup.apply("pc2.2b1")).getUnderlyingState());

        // test StateRepresentation fields
        StateRegistry pc1 = config.getOrCreateStateRegistry("pc1");
        assertNull(pc1.getDirectSuperstate());
        assertEquals("deeperParallel", pc1.getSuperstateIncludingParentStateMachine().getUnderlyingState());

        // test instance
        StateMachine<String, String> instance = instantiate();
        assertEquals(7, instance.getStateMachineStatus().getStates().size());
    }

    /**
     * tests trying to make a state parallel while it is already configured.
     */
    @Test(expected = StateTypeInUseException.class)
    public void test10WrongConfig() {
        config.configure("state");
        config.parallel("state");
    }

    /**
     * as previous, deeper nested
     */
    @Test(expected = StateTypeInUseException.class)
    public void test11WrongConfig() {
        setupComplexParallelInitial();
        config.parallel("pc2.1");
    }

    /**
     * tests calling <code>config.configure</code> twice on the same state.
     */
    @Test
    public void test20DoubleConfig() {
        config.configure("toplevel");
        StateConfigurer<String, String> helperParent = config.configure("parent");
        helperParent.substateOf("toplevel");
        config.parallel("deeperParallel").substateOf("parent");
        StateConfigurer<String, String> helperPc2 = config.configure("pc2");
        helperPc2.substateOf("deeperParallel");
        config.configure("pc2.1").substateOf("pc2");
        config.parallel("pc2.2").substateOf("pc2");
        config.configure("pc2.2a").substateOf("pc2.2");
        StateConfigurer<String, String> helperPc22b = config.configure("pc2.2b");
        helperPc22b.substateOf("pc2.2");

        assertEquals(helperParent, config.configure("parent"));
        assertEquals(helperPc2, config.configure("pc2"));
        assertEquals(helperPc22b, config.configure("pc2.2b"));
    }

    /**
     * tests calling <code>config.parallel</code> twice on the same state.
     */
    @Test
    public void test21DoubleConfigParallel() {
        config.configure("toplevel");
        config.configure("parent").substateOf("toplevel");
        StateConfigurer<String, String> helperDeeperParallel = config.parallel("deeperParallel");
        helperDeeperParallel.substateOf("parent");
        config.configure("pc2").substateOf("deeperParallel");
        config.configure("pc2.1").substateOf("pc2");
        StateConfigurer<String, String> helperPc22 = config.parallel("pc2.2");
        helperPc22.substateOf("pc2");
        config.configure("pc2.2a").substateOf("pc2.2");
        config.configure("pc2.2b").substateOf("pc2.2");

        assertEquals(helperDeeperParallel, config.parallel("deeperParallel"));
        assertEquals(helperPc22, config.parallel("pc2.2"));
    }

    /**
     * tests calling <code>config.configure</code> after a call on <code>config.parallel</code> on the same state.
     */
    @Test
    public void test22ConfigureAfterParallel() {
        config.configure("toplevel");
        config.configure("parent").substateOf("toplevel");
        StateConfigurer<String, String> helperDeeperParallel = config.parallel("deeperParallel");
        helperDeeperParallel.substateOf("parent");
        config.configure("pc2").substateOf("deeperParallel");
        config.configure("pc2.1").substateOf("pc2");
        StateConfigurer<String, String> helperPc22 = config.parallel("pc2.2");
        helperPc22.substateOf("pc2");
        config.configure("pc2.2a").substateOf("pc2.2");
        config.configure("pc2.2b").substateOf("pc2.2");

        assertEquals(helperDeeperParallel, config.configure("deeperParallel"));
        assertEquals(helperPc22, config.configure("pc2.2"));
    }

    private StateMachine<String, String> instantiate() {
        return new StateMachine<String, String>(config, context);
    }

    /**
     * configures the following nested configuration:
     *
     * <state parent
     * ---<PARALLEL parallel
     * ------<state p1
     * ------<state p2
     */
    private void setupChildParallelInitial() {
        config.configure("parent");
        config.parallel("parallel").substateOf("parent");
        config.configure("p1").substateOf("parallel");
        config.configure("p2").substateOf("parallel");
    }

    private void setupChildParallelViaTrigger() {
        setupChildParallelInitial();
        config.setInitialState("start").permit("go", "parent");
    }

    /**
     * configures the following complex configuration:
     *
     * <state toplevel
     * ---<state parent
     * ------<PARALLEL deeperParallel
     * ---------<state pc1
     * ------------<state pc1.1
     * ------------<state pc1.2
     * ---------<state pc2
     * ------------<state pc2.1
     * ------------<PARALLEL pc2.2
     * ---------------<state pc2.2a
     * ---------------<state pc2.2b
     * ------------------<state pc2.2b1
     */
    private void setupComplexParallelInitial() {
        config.configure("toplevel");
        config.configure("parent").substateOf("toplevel");
        config.parallel("deeperParallel").substateOf("parent");
        config.configure("pc1").substateOf("deeperParallel");
        config.configure("pc1.1").substateOf("pc1");
        config.configure("pc1.2").substateOf("pc1");
        config.configure("pc2").substateOf("deeperParallel");
        config.configure("pc2.1").substateOf("pc2");
        config.parallel("pc2.2").substateOf("pc2");
        config.configure("pc2.2a").substateOf("pc2.2");
        config.configure("pc2.2b").substateOf("pc2.2");
        config.configure("pc2.2b1").substateOf("pc2.2b");
    }

    /**
     * configures the following nested configuration:
     *
     * <PARALLEL parallel
     * ---<PARALLEL nestedParallel
     * ------<state c1
     * ------<state c2
     * ---<state c
     */
    private void setupDirectlyNestedParallelInitial() {
        config.parallel("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("c1").substateOf("nestedParallel");
        config.configure("c2").substateOf("nestedParallel");
        config.configure("c").substateOf("parallel");
    }

    /**
     * creates a statemachine that looks like this:
     *
     * <PARALLEL simpleParallel
     * ---<state sp1
     * ---<state sp2
     */
    private void setupSimpleParallelInitial() {
        config.parallel("simpleParallel");
        config.configure("sp1").substateOf("simpleParallel");
        config.configure("sp2").substateOf("simpleParallel");
    }

    /**
     * creates a statemachine that looks like this:
     *
     * <state start -> trigger go to simpleParallel
     * <PARALLEL simpleParallel
     * ---<state sp1
     * ---<state sp2
     */
    private void setupSimpleParallelViaTrigger() {
        setupSimpleParallelInitial();
        config.setInitialState("start").permit("go", "simpleParallel");
    }

}
