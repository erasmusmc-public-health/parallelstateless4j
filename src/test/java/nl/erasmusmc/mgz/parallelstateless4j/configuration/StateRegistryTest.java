/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.stream.Collectors;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.testUtils.BaseSMTest;
import nl.erasmusmc.mgz.parallelstateless4j.testUtils.TestUtils;

/**
 * tests for the StateRegistry
 *
 * @author rinke
 *
 */
public class StateRegistryTest extends BaseSMTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateRegistryTest.class);

    /**
     * tests the includes method, single param version
     */
    @SuppressWarnings("unchecked")
    @Test
    public void test01Includes() {
        StateConfigurer<String, String> sc = config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.parallel("p").substateOf("child1.1.1");
        config.configure("p1").substateOf("p");
        config.configure("p2").substateOf("p");
        StateRegistry<String, String> reg = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, sc, "registry");
        assertTrue(reg.includes("parent"));
        assertTrue(reg.includes("child1.1"));
        assertTrue(reg.includes("child1.1.1"));
        assertTrue(reg.includes("p"));
        assertFalse(reg.includes("p1"));
    }

    /**
     * tests the includes method, two param version, false.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void test02IncludesFalse() {
        StateConfigurer<String, String> sc = config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.parallel("p").substateOf("child1.1.1");
        config.configure("p1").substateOf("p");
        config.configure("p2").substateOf("p");
        StateRegistry<String, String> reg = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, sc, "registry");
        assertTrue(reg.includes("parent", false));
        assertTrue(reg.includes("child1.1", false));
        assertTrue(reg.includes("child1.1.1", false));
        assertTrue(reg.includes("p", false));
        assertFalse(reg.includes("p1", false));
    }

    /**
     * tests the includes method, two param version, false.
     */
    @SuppressWarnings("unchecked")
    @Test
    public void test03IncludesTrue() {
        StateConfigurer<String, String> sc = config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.parallel("p").substateOf("child1.1.1");
        config.configure("p1").substateOf("p");
        config.configure("p2").substateOf("p");
        StateRegistry<String, String> reg = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, sc, "registry");
        assertTrue(reg.includes("parent", true));
        assertTrue(reg.includes("child1.1", true));
        assertTrue(reg.includes("child1.1.1", true));
        assertTrue(reg.includes("p", true));
        assertTrue(reg.includes("p1", true));
    }

    /**
     * tests the isRelatedState method
     */
    @SuppressWarnings("unchecked")
    @Test
    public void test04IsRelatedState() {
        StateConfigurer<String, String> scParent = config.configure("parent");
        config.configure("child1").substateOf("parent");
        StateConfigurer<String, String> scChild11 = config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.parallel("p").substateOf("child1.1.1");
        StateConfigurer<String, String> scp1 = config.configure("p1").substateOf("p");
        config.configure("p2").substateOf("p");
        StateConfigurer<String, String> scChild1Side = config.configure("child1.side").substateOf("child1");
        StateRegistry<String, String> regParent = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scParent, "registry");
        assertTrue(regParent.isRelatedState("parent"));
        assertTrue(regParent.isRelatedState("child1"));
        assertTrue(regParent.isRelatedState("child1.1.1"));
        assertTrue(regParent.isRelatedState("p"));
        assertTrue(regParent.isRelatedState("p2"));
        assertTrue(regParent.isRelatedState("child1.side"));
        StateRegistry<String, String> regChild11 = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scChild11, "registry");
        assertTrue(regChild11.isRelatedState("parent"));
        assertTrue(regChild11.isRelatedState("child1"));
        assertTrue(regChild11.isRelatedState("child1.1.1"));
        assertTrue(regChild11.isRelatedState("p"));
        assertTrue(regChild11.isRelatedState("p2"));
        assertFalse(regChild11.isRelatedState("child1.side"));
        StateRegistry<String, String> regP1 = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scp1, "registry");
        assertTrue(regP1.isRelatedState("parent"));
        assertTrue(regP1.isRelatedState("child1"));
        assertTrue(regP1.isRelatedState("child1.1.1"));
        assertTrue(regP1.isRelatedState("p"));
        assertTrue(regP1.isRelatedState("p1"));
        assertFalse(regP1.isRelatedState("p2"));
        assertFalse(regP1.isRelatedState("child1.side"));
        StateRegistry<String, String> regChild1Side = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scChild1Side, "registry");
        assertTrue(regChild1Side.isRelatedState("parent"));
        assertTrue(regChild1Side.isRelatedState("child1"));
        assertFalse(regChild1Side.isRelatedState("child1.1.1"));
        assertFalse(regChild1Side.isRelatedState("p"));
        assertFalse(regChild1Side.isRelatedState("p2"));
        assertTrue(regChild1Side.isRelatedState("child1.side"));
    }

    /**
     * tests the getAllRelatedStates method
     */
    @SuppressWarnings("unchecked")
    @Test
    public void test05getAllRelatedStates() {
        StateConfigurer<String, String> scParent = config.configure("parent");
        config.configure("child1").substateOf("parent");
        StateConfigurer<String, String> scChild11 = config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.parallel("p").substateOf("child1.1.1");
        StateConfigurer<String, String> scp1 = config.configure("p1").substateOf("p");
        config.configure("p2").substateOf("p");
        StateConfigurer<String, String> scChild1Side = config.configure("child1.side").substateOf("child1");
        StateRegistry<String, String> regParent = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scParent, "registry");
        Collection<String> relatedParent = regParent.getAllRelatedStates();
        assertTrue(relatedParent.contains("parent"));
        assertTrue(relatedParent.contains("child1"));
        assertTrue(relatedParent.contains("child1.1.1"));
        assertTrue(relatedParent.contains("p"));
        assertTrue(relatedParent.contains("p2"));
        assertTrue(relatedParent.contains("child1.side"));
        StateRegistry<String, String> regChild11 = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scChild11, "registry");
        Collection<String> relatedChild11 = regChild11.getAllRelatedStates();
        assertTrue(relatedChild11.contains("parent"));
        assertTrue(relatedChild11.contains("child1"));
        assertTrue(relatedChild11.contains("child1.1.1"));
        assertTrue(relatedChild11.contains("p"));
        assertTrue(relatedChild11.contains("p2"));
        assertFalse(relatedChild11.contains("child1.side"));
        StateRegistry<String, String> regP1 = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scp1, "registry");
        Collection<String> relatedP1 = regP1.getAllRelatedStates();
        assertTrue(relatedP1.contains("parent"));
        assertTrue(relatedP1.contains("child1"));
        assertTrue(relatedP1.contains("child1.1.1"));
        assertTrue(relatedP1.contains("p"));
        assertTrue(relatedP1.contains("p1"));
        assertFalse(relatedP1.contains("p2"));
        assertFalse(relatedP1.contains("child1.side"));
        StateRegistry<String, String> regChild1Side = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, scChild1Side, "registry");
        Collection<String> relatedChild1Side = regChild1Side.getAllRelatedStates();
        assertTrue(relatedChild1Side.contains("parent"));
        assertTrue(relatedChild1Side.contains("child1"));
        assertFalse(relatedChild1Side.contains("child1.1.1"));
        assertFalse(relatedChild1Side.contains("p"));
        assertFalse(relatedChild1Side.contains("p2"));
        assertTrue(relatedChild1Side.contains("child1.side"));
    }

    /**
     * tests the getLeafChildStates method
     */
    @SuppressWarnings("unchecked")
    @Test
    public void test11GetLeafChildStates() {
        StateConfigurer<String, String> sc = config.configure("parent");
        config.configure("child1").substateOf("parent");
        config.configure("child1.1").substateOf("child1");
        config.configure("child1.1.1").substateOf("child1.1");
        config.parallel("p").substateOf("child1.1.1");
        config.configure("p1").substateOf("p");
        StateConfigurer<String, String> p2 = config.configure("p2").substateOf("p");
        StateRegistry<String, String> reg = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, sc, "registry");
        Collection<String> leafsParent = reg.getLeafChildStates().stream().map(StateRegistry::getUnderlyingState)
                .collect(Collectors.toList());
        assertEquals(2, leafsParent.size());
        assertTrue(leafsParent.contains("p1"));
        assertTrue(leafsParent.contains("p2"));
        StateRegistry<String, String> regP2 = (StateRegistry<String, String>) TestUtils.getPrivateFieldValue(
                StateConfigurer.class, p2, "registry");
        Collection<String> leafsP2 = regP2.getLeafChildStates().stream().map(StateRegistry::getUnderlyingState)
                .collect(Collectors.toList());
        assertEquals(1, leafsP2.size());
        assertTrue(leafsP2.contains("p2"));
    }

}
