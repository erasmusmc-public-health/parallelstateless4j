/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import nl.erasmusmc.mgz.parallelstateless4j.testUtils.BaseSMTest;
import nl.erasmusmc.mgz.parallelstateless4j.testUtils.TestUtils;

/**
 * tests the state machine in parallel settings.
 * On all tests: note that double entry/exit messages are an artifact of the testing classes.
 *
 * @author rinke
 *
 */
public class ParallelStateMachineTest extends BaseSMTest {

    /**
     * tests a transition that is internal inside a nested parallel state machine.
     */
    @Test
    public void test01InternalInNested() {
        config.parallel("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("dummy").substateOf("parallel");
        config.configure("state1").substateOf("nestedParallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.configure("state1.1").substateOf("state1");
        config.configure("state1.2").substateOf("state1");
        config.configure("state1.1").permit("go", "state1.2", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "parallel", "nestedParallel", "dummy", "state1", "nestedDummy", "state1.1");
        sm.fire("go");
        TestUtils.testInStates(sm, "parallel", "nestedParallel", "dummy", "state1", "nestedDummy", "state1.2");
    }

    /**
     * tests a transition into a deeply nested SM
     */
    @Test
    public void test11aIntoDeeplyNested() {
        config.configure("start");
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("start").permit("go", "target.2", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "start");
        sm.fire("go");
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.2");
    }

    /**
     * as 11a, but not to leaf state.
     */
    @Test
    public void test11bIntoDeeplyNested() {
        config.configure("start");
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("start").permit("go", "target", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "start");
        sm.fire("go");
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.1");
    }

    /**
     * Into an intermediate level SM
     */
    @Test
    public void test12IntoIntermediateLevelSM() {
        config.configure("start");
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("start").permit("go", "nestedParallel", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "start");
        sm.fire("go");
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.1");
    }

    /**
     * tests a three level parallel setup, trigger from deepest nested to top.
     * Expected Behaviour: move to target1.
     *
     * <pre>
     * . PARALLEL
     * . - NESTEDPARALLEL
     * . - - state1  => go: target = target1
     * . - - nesteddummy
     * . - dummy
     * . target1
     * </pre>
     */
    @Test
    public void test21threeLevelParallel() {
        config.parallel("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("state1").substateOf("nestedParallel");
        config.configure("dummy").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.configure("target1");
        config.configure("state1").permit("go", "target1", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        // test all states before trigger fire
        assertTrue(machine.getStateMachineStatus().isInState("parallel"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedParallel"));
        assertTrue(machine.getStateMachineStatus().isInState("state1"));
        assertTrue(machine.getStateMachineStatus().isInState("dummy"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedDummy"));
        assertFalse(machine.getStateMachineStatus().isInState("target1"));
        machine.fire("go");
        assertFalse(machine.getStateMachineStatus().isInState("parallel"));
        assertFalse(machine.getStateMachineStatus().isInState("nestedParallel"));
        assertFalse(machine.getStateMachineStatus().isInState("state1"));
        assertFalse(machine.getStateMachineStatus().isInState("dummy"));
        assertFalse(machine.getStateMachineStatus().isInState("nestedDummy"));
        assertTrue(machine.getStateMachineStatus().isInState("target1"));
    }

    /**
     * tests a three level parallel setup, trigger from intermediate nested to top.
     * Expected Behaviour: move to target1.
     *
     * <pre>
     * . PARALLEL
     * . - NESTEDPARALLEL => go: target = target1
     * . - - state1
     * . - - nestedDummy
     * . - dummy
     * . target1
     * </pre>
     */
    @Test
    public void test22threeLevelParallel() {
        config.parallel("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("state1").substateOf("nestedParallel");
        config.configure("dummy").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.configure("target1");
        config.configure("nestedParallel").permit("go", "target1", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        // test all states before trigger fire
        assertTrue(machine.getStateMachineStatus().isInState("parallel"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedParallel"));
        assertTrue(machine.getStateMachineStatus().isInState("state1"));
        assertTrue(machine.getStateMachineStatus().isInState("dummy"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedDummy"));
        assertFalse(machine.getStateMachineStatus().isInState("target1"));
        machine.fire("go");
        assertFalse(machine.getStateMachineStatus().isInState("parallel"));
        assertFalse(machine.getStateMachineStatus().isInState("nestedParallel"));
        assertFalse(machine.getStateMachineStatus().isInState("state1"));
        assertFalse(machine.getStateMachineStatus().isInState("dummy"));
        assertFalse(machine.getStateMachineStatus().isInState("nestedDummy"));
        assertTrue(machine.getStateMachineStatus().isInState("target1"));
    }

    /**
     * tests a three level parallel setup; trigger from deepest nested state we are not in to top.
     * Expected Behaviour: don't move, stay in same states.
     *
     * <pre>
     * . PARALLEL
     * . - NESTEDPARALLEL
     * . - - state1
     * . - - - leafIn
     * . - - - leafNotIn  => go: target = target1
     * . - - nestedDummy
     * . - dummy
     * . target1
     * </pre>
     */
    @Test
    public void test23threeLevelParallel() {
        config.parallel("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("state1").substateOf("nestedParallel");
        config.configure("leafIn").substateOf("state1");
        config.configure("leafNotIn").substateOf("state1");
        config.configure("dummy").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.configure("target1");
        config.configure("leafNotIn").permit("go", "target1", TestUtils.getTransitionAction("go"));
        config.onUnhandledTrigger(null);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        // test all states before trigger fire
        assertTrue(machine.getStateMachineStatus().isInState("parallel"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedParallel"));
        assertTrue(machine.getStateMachineStatus().isInState("state1"));
        assertTrue(machine.getStateMachineStatus().isInState("dummy"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedDummy"));
        assertFalse(machine.getStateMachineStatus().isInState("target1"));
        machine.fire("go");
        assertTrue(machine.getStateMachineStatus().isInState("parallel"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedParallel"));
        assertTrue(machine.getStateMachineStatus().isInState("state1"));
        assertTrue(machine.getStateMachineStatus().isInState("dummy"));
        assertTrue(machine.getStateMachineStatus().isInState("nestedDummy"));
        assertFalse(machine.getStateMachineStatus().isInState("target1"));
    }

    /**
     * Leaf state to self in deep SM
     */
    @Test(expected = IllegalStateException.class)
    public void test30LeafToStelfDeeplyNested() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target.1").permit("go", "target.1", TestUtils.getTransitionAction("go"));
    }

    /**
     * NonLeaf state to self in deep SM
     */
    @Test(expected = IllegalStateException.class)
    public void test31NonLeafToStelfDeeplyNested() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target").permit("go", "target", TestUtils.getTransitionAction("go"));
    }

    /**
     * Parent state to init child in deep SM. This one cannot be banned at config time,
     * because there is more than one substate of parent, and we don't know at config time
     * in which substate it is, so it might be valid.
     * The transition will be ignored.
     */
    @Test
    public void test32ParentToInitiChildDeeplyNested() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target").permit("go", "target.1", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        sm.fire("go");
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.1");
    }

    /**
     * Child init state to Own parent, deeply nested
     */
    @Test(expected = IllegalStateException.class)
    public void test33ChildInitToOwnParentDeeplyNested() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target.1").permit("go", "target", TestUtils.getTransitionAction("go"));
    }

    /**
     * leaf state to self in upper, there are deeper nested sm's below it.
     */
    @Test(expected = IllegalStateException.class)
    public void test34stateToSelfIntermediateNested() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("parallel").permit("go", "nestedParallel", TestUtils.getTransitionAction("go"));
    }

    /**
     * Parent SM to own leaf which is presently active.
     */
    @Test(expected = IllegalStateException.class)
    public void test40aParentSMtoOwnLeaf() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("parallel").permit("go", "target.1", TestUtils.getTransitionAction("go"));
    }

    /**
     * As 41, dynamic
     */
    @Test(expected = IllegalDynamicDestinationException.class)
    public void test40bParentSMtoOwnLeaf() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("parallel")
                .permitDynamic("go", (c, a) -> "target.1", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.1");
        sm.fire("go");
    }

    /**
     * Parent SM to own leaf in which it is presently not.
     */
    @Test(expected = IllegalStateException.class)
    public void test41aParentSMtoOwnLeaf() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("parallel").permit("go", "target.2", TestUtils.getTransitionAction("go"));
    }

    /**
     * Parent SM to own leaf in which it is presently not, dynamic
     */
    @Test(expected = IllegalDynamicDestinationException.class)
    public void test41bParentSMtoOwnLeaf() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("parallel")
                .permitDynamic("go", (c, a) -> "target.2", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.1");
        sm.fire("go");
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.2");
    }

    /**
     * cross-parallel transition
     */
    @Test(expected = DestinationNotFoundException.class)
    public void test42aCrossParallel() {
        config.parallel("parallel");
        config.configure("p1").substateOf("parallel");
        config.configure("p2").substateOf("parallel");
        config.configure("p1").permit("go", "p2", TestUtils.getTransitionAction("go"));
    }

    /**
     * cross-parallel transition, dynamic
     */
    @Test(expected = DestinationNotFoundException.class)
    public void test42bCrossParallel() {
        config.parallel("parallel");
        config.configure("p1").substateOf("parallel");
        config.configure("p2").substateOf("parallel");
        config.configure("p1")
                .permitDynamic("go", (c, a) -> "p2", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "parallel", "p1");
        sm.fire("go");
        TestUtils.testInStates(sm, "parallel", "p2");
    }

    /**
     * leaf in deepest SM to Parent in parent SM
     */
    @Test(expected = IllegalStateException.class)
    public void test43aLeafToOwnParent() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target.1").permit("go", "parallel", TestUtils.getTransitionAction("go"));
    }

    /**
     * leaf in deepest SM to Parent in parent SM, Dynamic
     */
    @Test(expected = IllegalDynamicDestinationException.class)
    public void test43bLeafToOwnParent() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target.1")
                .permitDynamic("go", (c, a) -> "parallel", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.1");
        sm.fire("go");
    }

    /**
     * As 43a, but now not to upper level parent, but intermediate parent
     */
    @Test(expected = IllegalStateException.class)
    public void test44aLeafToOwnParent() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target.1").permit("go", "nestedParallel", TestUtils.getTransitionAction("go"));
    }

    /**
     * As 44a, dynamic
     */
    @Test(expected = IllegalDynamicDestinationException.class)
    public void test44bLeafToOwnParent() {
        config.parallel("parallel");
        config.configure("dummy").substateOf("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("nestedParallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedNestedParallel");
        config.configure("target").substateOf("nestedNestedParallel");
        config.configure("target.1").substateOf("target");
        config.configure("target.2").substateOf("target");
        config.configure("target.1")
                .permitDynamic("go", (c, a) -> "nestedParallel", TestUtils.getTransitionAction("go"));
        StateMachine<String, String> sm = new StateMachine<>(config, context);
        TestUtils.testInStates(sm, "parallel", "dummy", "nestedParallel", "nestedDummy", "nestedNestedParallel",
                "nestedNestedDummy", "target", "target.1");
        sm.fire("go");
    }

    /**
     * tests the failedConditionTriggerAction setting on the state machine, but now in a parallel setting:
     * the trigger has a failed condition in the child state machine, and no trigger in the parent.
     */
    @Test
    public void test51customFailedConditionTriggerAction() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state").substateOf("parallel");
        config.configure("dummy").substateOf("parallel");
        config.configure("target1");
        config.configure("state").permitIf("go", "target1", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertTrue(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("state"));
    }

    /**
     * tests a failed condition in a child state machine, with a passing condition in the parent state machine.
     */
    @Test
    public void test52customFailedConditionTriggerAction() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state").substateOf("parallel");
        config.configure("dummy").substateOf("parallel");
        config.configure("target1");
        config.configure("target2");
        config.configure("state").permitIf("go", "target1", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.configure("parallel").permit("go", "target2", TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("target2"));
    }

    /**
     * as previous, both conditions failing.
     */
    @Test
    public void test53customFailedConditionTriggerAction() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state").substateOf("parallel");
        config.configure("dummy").substateOf("parallel");
        config.configure("target1");
        config.configure("target2");
        config.configure("state").permitIf("go", "target1", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.configure("parallel").permitIf("go", "target2", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertTrue(used.used);
    }

    /**
     * as previous, child passes condition, parent fails
     */
    @Test
    public void test54customFailedConditionTriggerAction() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state").substateOf("parallel");
        config.configure("dummy").substateOf("parallel");
        config.configure("target1");
        config.configure("target2");
        config.configure("state").permitIf("go", "target1", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.configure("parallel").permitIf("go", "target2", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("target1"));
    }

    /**
     * two child state machines 1 passes condition, 2nd fails.
     */
    @Test
    public void test55customFailedConditionTriggerAction() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state1").substateOf("parallel");
        config.configure("state2").substateOf("parallel");
        config.configure("target1");
        config.configure("target2");
        config.configure("state1").permitIf("go", "target1", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.configure("state2").permitIf("go", "target2", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("target1"));
    }

    /**
     * as previous, vice versa
     */
    @Test
    public void test56customFailedConditionTriggerAction() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state1").substateOf("parallel");
        config.configure("state2").substateOf("parallel");
        config.configure("target1");
        config.configure("target2");
        config.configure("state1").permitIf("go", "target1", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.configure("state2").permitIf("go", "target2", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("target2"));
    }

    /**
     * as previous, both valid
     */
    @Test
    public void test57customFailedConditionTriggerAction() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state2").substateOf("parallel");
        config.configure("state1").substateOf("parallel");
        config.configure("target1");
        config.configure("target2");
        config.configure("state1").permitIf("go", "target1", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.configure("state2").permitIf("go", "target2", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("target2"));
    }

    /**
     * out of 2 level deep SM, same triggers at level, only one passing.
     */
    @Test
    public void test58MultipleSameTriggerOnePassing() {
        Used used = new Used();
        config.parallel("parallel");
        config.configure("state2").substateOf("parallel");
        config.configure("state1").substateOf("parallel");
        config.configure("target1");
        config.configure("target2");
        config.configure("state1").permitIf("go", "target1", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.configure("state1").permitIf("go", "target2", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("target2"));
    }

    /**
     * As 58, deeper nested
     */
    @Test
    public void test59MultipleSameTriggerOnePassing() {
        Used used = new Used();
        config.parallel("parallel");
        config.parallel("nestedParallel").substateOf("parallel");
        config.configure("nestedDummy").substateOf("parallel");
        config.parallel("nestedNestedParallel").substateOf("nestedParallel");
        config.configure("nestedNestedDummy").substateOf("nestedParallel");
        config.configure("state1").substateOf("nestedNestedParallel");
        config.configure("state2").substateOf("nestedNestedParallel");
        config.configure("state1.1").substateOf("state1");
        config.configure("state1.2").substateOf("state1");
        config.configure("target1");
        config.configure("target2");
        config.configure("state1").permitIf("go", "target1", TestUtils.fixedGuard(false),
                TestUtils.getTransitionAction("go"));
        config.configure("state1").permitIf("go", "target2", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.configure("state1").permitIf("go", "target1", TestUtils.fixedGuard(true),
                TestUtils.getTransitionAction("go"));
        config.onFailedConditionTrigger((stateMachineContext, transition, parent, args) -> {
            used.used = true;
        });
        assertFalse(used.used);
        StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        assertFalse(used.used);
        assertTrue(machine.getStateMachineStatus().isInState("target2"));
    }

    /**
     * tests exit listener. The console should show a message that the state machine is exited,
     * but ONLY for the toplevel.
     */
    @Test
    public void test60ExitListener() {
        config.configure("parent");
        config.parallel("parallel").substateOf("parent");
        config.configure("state1").substateOf("parallel");
        config.configure("state2").substateOf("parallel");
        config.configure("other").substateOf("parent");
        config.configure("state1").permit("go", "other");
        final IStateMachineExitListener<String, String> listener = new ExampleExitListener<>();
        config.addListener(listener);
        final StateMachine<String, String> machine = new StateMachine<>(config, context);
        machine.fire("go");
        machine.exit(null, null);
    }

}
