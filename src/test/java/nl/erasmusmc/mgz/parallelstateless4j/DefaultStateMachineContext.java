/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Default implementation of {@link IStateMachineContext}.
 *
 * {@inheritDoc}
 * @author first8
 * @author rinke
 */
public class DefaultStateMachineContext<S, T> implements IStateMachineContext<S, T> {

    private final Map<String, Object>                  attributes = new HashMap<>();

    private IStateMachine<S, T>                        stateMachine;

    private Multimap<String, IContextPropertyListener> listeners;

    @Override
    public void addListener(String key, IContextPropertyListener listener) {
        if (listeners == null) {
            listeners = HashMultimap.create(2, 2);
        }
        listeners.put(key, listener);
    }

    @Override
    public Map<String, Object> asMap() {
        return new HashMap<String, Object>(attributes);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E> void changeCollectionAttribute(String key, Collection<E> additions, Collection<E> removals)
            throws ClassCastException {
        Collection<E> collection = (Collection<E>) getAttribute(key);
        boolean wasNull = false;
        if (collection == null) {
            collection = new HashSet<E>(4);
            attributes.put(key, collection);
            wasNull = true;
        }
        boolean changed = false;
        if (!wasNull && removals != null) {
            // so that not present elements are removed, and removals really contains only the actually to be removed items.
            removals.retainAll(collection);
            changed |= collection.removeAll(removals);
        }
        if (additions != null) {
            changed |= !additions.removeAll(collection);
            changed |= collection.addAll(additions);
        }
        if (listeners != null) {
            for (final IContextPropertyListener listener : listeners.get(key)) {
                if (changed) {
                    listener.onCollectionChange(this, key, additions, removals, collection);
                }
            }
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public boolean clearCollectionAttribute(String key) {
        Object rawValue = getAttribute(key);
        if (rawValue == null) {
            return false;
        }
        if (!(rawValue instanceof Collection)) {
            throw new ClassCastException("Attribute" + key + " on state machine context is NOT of a collection type");
        }
        Collection collection = (Collection) rawValue;
        if (listeners != null) {
            for (final IContextPropertyListener listener : listeners.get(key)) {
                listener.onCollectionChange(this, key, null, new ArrayList(collection), Collections.EMPTY_SET);
            }
        }
        collection.clear();
        return true;
    }

    @Override
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    @Override
    public Object getAttribute(String name, Object initialValue) {
        attributes.putIfAbsent(name, initialValue);
        return attributes.get(name);
    }

    @SuppressWarnings("unchecked")
    @Override
    public IStateMachine<S, T> getStateMachine() {
        return stateMachine;
    }

    @Override
    public Object removeAttribute(String key) {
        return attributes.remove(key);
    }

    @Override
    public void removeListener(String key, IContextPropertyListener listener) {
        if (listeners != null) {
            listeners.remove(key, listener);
        }
    }

    @Override
    public void removeListeners(String key) {
        if (listeners != null) {
            listeners.removeAll(key);
        }
    }

    @Override
    public void setAttribute(String key, Object value) {
        final Object oldValue = attributes.get(key);
        attributes.put(key, value);
        if (listeners != null) {
            for (final IContextPropertyListener listener : listeners.get(key)) {
                listener.onChange(this, key, oldValue, value);
            }
        }
    }

    @Override
    public void setListeners(Multimap<String, IContextPropertyListener> contextPropertyListeners) {
        listeners = contextPropertyListeners;
    }

    @Override
    public void setStateMachine(IStateMachine<S, T> contextedStateMachine) {
        stateMachine = contextedStateMachine;
    }

}
