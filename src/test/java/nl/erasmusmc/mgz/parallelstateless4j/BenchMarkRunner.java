/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

/**
 * Runs benchmark tests with JMH (the Java Microbenchmark Harness).
 *
 * Careful: because this goes via a test generated class, you need to do
 * a mvn clean install first before any changes in this class become effective.
 *
 * @see https://www.baeldung.com/java-microbenchmark-harness
 * @see https://jenkov.com/tutorials/java-performance/jmh.html
 */
public class BenchMarkRunner {

    @State(Scope.Benchmark)
    public static class MyState {
        public Double                               value = Double.valueOf(2.0);
        public IStateMachineContext<String, String> smc;

        public MyState() {
            smc = new DefaultStateMachineContext<>();
            smc.setAttribute("double", value);
            smc.setAttribute("koe", "koe");
            smc.setAttribute("aap", "aap");
            smc.setAttribute("beer", "beer");
            smc.setAttribute("hond", value);
            smc.setAttribute("varken", "varken");
            smc.setAttribute("konijn", value);
            smc.setAttribute("leeuw", "leeuw");
            smc.setAttribute("paard", "paard");
        }
    }

    public static void main(String[] args) throws Exception {
        org.openjdk.jmh.Main.main(args);
        // Options options = new OptionsBuilder()
        // .include(BenchMarkRunner.class.getSimpleName())
        // .build();
        // new Runner(options).run();
    }

    @Benchmark
    @Fork(value = 3, warmups = 2)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public double testPropertyGetter(MyState state) {
        return ((Double) state.smc.getAttribute("double")).doubleValue();
    }

    @Benchmark
    @Fork(value = 3, warmups = 2)
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void testPropertySetter(MyState state) {
        state.smc.setAttribute("double", state.value);
    }

}
