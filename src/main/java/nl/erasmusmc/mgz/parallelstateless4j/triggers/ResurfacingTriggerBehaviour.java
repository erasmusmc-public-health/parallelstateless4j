package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.LogSwitch;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;

/**
 * a special type of TransitioningTriggerBehaviour, which is used to catch triggers in the parent config which are
 * send from a child state machine via an {@link ExitingTriggerBehaviour}. A ResurfacingTriggerBehaviour can never act
 * on itself, but is always (a second) part of a set of transitions.
 * <p>
 * In case of parallelism, if a state machine
 * is in a nested/child parallel state, and if there is a transition triggered from the nested state machine to a state inside
 * the holding parent state machine, there are two TriggerBehaviours involved:
 * <ol>
 * <li>The transition out of the nested/child parallel state machine is handled via an ExitingTriggerBehaviour.
 * <li>If the control comes back at the hosting state machine, because the nested state machine is exited, the second part of the
 * transition is taken over by a ResurfacingTriggerBehaviour.
 * </ol>
 *
 * <p>
 * Example:
 * Consider this:
 *
 * <pre>
 * parallel
 *    state1
 *    state2
 * target
 * </pre>
 *
 * A transition from state1 to target is handled by two triggerBehaviours:
 * <ol>
 * <li>an ExitingTriggerBehaviour, valid for the child state machine hosting state1, which triggers
 * a transition from state1 out of the child state machine.\
 * <li>a ResurfacingTriggerBehaviour, valid for the parent state machine hosting parallel and target,
 * which triggers a transition from parallel to target.
 * </ol>
 *
 * @author rinke
 *
 * @param <S>
 * @param <T>
 */
public class ResurfacingTriggerBehaviour<S, T> extends TransitioningTriggerBehaviour<S, T> {

    private static final Logger logger = LoggerFactory.getLogger(ResurfacingTriggerBehaviour.class);

    public ResurfacingTriggerBehaviour(T trigger, S destination, Integer resurfacingId, Action<S, T> action) {
        super(trigger, destination, null, action, null);
        this.resurfacingId = resurfacingId;
    }

    @Override
    public void performAction(IStateMachineContext<S, T> context,
            Transition<S, T> transition, Object[] args) {
        if (LogSwitch.LOG) {
            if (logger.isDebugEnabled()) {
                MachineRegistry.agentIdToMDC(context);
                logger.debug("Resurfacing from child state machine, to state {} as result of trigger {}.",
                        getDestination(), getTrigger());
            }
        }
        S parent = (transition == null) ? null : transition.getDeclaredIn();
        getAction().doIt(context, transition, parent, args);
    }

    @Override
    public boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest) {
        dest.set(destination);
        return true;
    }
}
