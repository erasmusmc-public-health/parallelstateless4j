package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import static java.util.Objects.requireNonNull;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;

/**
 * DynamicTriggerBehaviour defines the TriggerBehaviour for transitions with a dynamic target.
 * <p>
 * <b>Warning:</b><br>
 * Dynamic triggers are NOT working when they go over the boundary of a parallel state machine.
 * They only work in a context where there is no parallelism involved.
 * <br>
 * In some future version this may be tackled.
 *
 */
public class DynamicTriggerBehaviour<S, T> extends TriggerBehaviour<S, T> {

    private final Func<S, S, T> destinationResolver;

    private final Action<S, T>  action;

    public DynamicTriggerBehaviour(T trigger, Func<S, S, T> destinationResolver,
            Func<Boolean, S, T> guard, Action<S, T> action, S declaredIn) {
        super(trigger, guard, declaredIn);
        requireNonNull(destinationResolver, "destination is null");
        this.destinationResolver = destinationResolver;
        this.action = action;
    }

    public Action<S, T> getAction() {
        return action;
    }

    public Func<S, S, T> getDestination() {
        return destinationResolver;
    }

    @Override
    public void performAction(IStateMachineContext<S, T> context,
            Transition<S, T> transition, Object[] args) {
        S parent = (transition == null) ? null : transition.getDeclaredIn();
        action.doIt(context, transition, parent, args);
    }

    @Override
    public boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest) {
        dest.set(destinationResolver.call(context, args));
        return true;
    }
}
