package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.LogSwitch;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;

/**
 * defines the behaviour for a transition from one state to another.
 * <p>
 * Note that some types of transition are not allowed, and the configurers will raise an exception if you
 * try to configure them. This means that the corresponding Triggerbehavior will not even be created.
 * However, as there exist also DynamicTriggers (whose destination is only resolved runtime, and not
 * configuration time), the TriggerBehaviours must also check on the same rules as the configurers.
 * <p>
 * The following rules apply to transitions:
 * <ul>
 * <li><b>General rules</b>:
 * <ul>
 * <li>Each state can have triggers defined. The reaction on a trigger is stored in a TriggerBehaviour object
 * which stores the trigger, the destination, actions to be taken, and conditions that apply for the trigger
 * to be accepted. TriggerBehaviours are stored on the state registry of the state they belong to.
 * <li>A state machine will not react on Triggers of TriggerBehaviours belonging to states that are
 * NOT active at the moment. Only triggerBehaviours of active states do count.
 * <li>A leaf state and all of its parent states count as active; so triggers defined on a state, also
 * count for all of the child- or parent states of that state.
 * <li>TriggerBehaviours defined in one state are also valid for all sub-states of that state (and thus,
 * for all sub-statemachines, if present).
 * <li>When a triggerBehaviour is accepted, a transition starts. A transition starts from a source state, which
 * is always the leaf state of a state machine (even when the triggerbehaviour is stored on a parent state of
 * the leaf state, and not the leaf state itself). The destination state is looked up on the triggerBehaviour.
 * For the transition all necessary states are exited up to the deepest level state that destination and source
 * share as a parent. Then, all states are entred between the common parent and the destination state (including
 * the destination state itself).
 * </ul>
 * <li><b>Rules concerning parallelism</b>:
 * <ul>
 * <li>Each direct substate of a parallel state has its own state machine. This is called a child state machine or
 * nested state machine.
 * <li>Cross- state machine triggers are NOT allowed. So transitions from one child of a parallel state to
 * another child of the same parallel parent state are not allowed.
 * <li>Triggers must be declared on the (child) state machine where they are valid. This means that you cannot
 * define a triggerbehaviour on a parent state machine which defines a transtion which is internal for one of
 * the child state machines. TriggerBehaviours are (in contrast to states) NOT delegated to child- or
 * parent state machines. Inside one state machine, they can be delegated to child- or parent states, as
 * long as they don't cross the (child) state machine boundary. Note however, there are two exceptions to
 * this rule, which will be covered in next two bullets.
 * <li><b>Incoming Triggers:</b> It is allowed to define a trigger into a child state machine which doesn't exist yet
 * (because the parallel state which hosts it is not active yet). As a result of this transition, the necessary
 * child state machines will be created as soon as their hosting parallel state is entered, and the triggerbehaviour
 * will take care that inside one of the newly created child state machines, the correct state in that machine
 * will be entered.<br>
 * Example:<br>
 *
 * <pre>
 * - start
 * - parallel
 * - - ps1
 * - - - ps1.1
 * - - - ps1.2
 * - - ps2
 * </pre>
 *
 * A triggerbehaviour on start, with destination ps1.2 is allowed, because it is an incoming trigger.<br>
 * A triggerbehaviour
 * on parallel, from ps1.1 to ps1.2 is NOT allowed, because the trigger for a transtion inside the state machine of ps1
 * is defined outside the scope of that state machine (on parallel), and the transition would NOT lead to the creation
 * of the state machine containing ps1.1 and ps1.2, because that state machine already exists when ps1.1 is active.
 * A trigger from ps1.1 to ps1.2 has to be declared on either ps1 or ps1.1. On all other states it is not allowed.
 * <li><b>Exiting triggers</b>: The exact opposite of the previous item is also allowed: it is allowed to define
 * a trigger on a child state machine which leads to a state outside that child state machine, if and only if that
 * destination state is NOT in some hosting state machine of the child state machine. As a result of this
 * transition, the direct hosting parallel state would be left, leading to dissolving/killing the child state machine.<br>
 * Example:<br>
 *
 * <pre>
 * - parallel
 * - - ps1
 * - - - ps1.1
 * - - - ps1.2
 * - - ps2
 * - target
 * </pre>
 *
 * A triggerbehaviour on ps1, ps1.1, ps1.2 or ps2 to target, would mean that the parallel state is left, which would lead
 * to destruction of all child state machines contained by the parallel states. Such exiting transitions are allowed.<br>
 * A triggerbehaviour on ps1.1 to ps2 would NOT be allowed, as the parent parallel state would not be left by this transition,
 * hence the child state machines would not be destructed.
 * <li>Note that dynamic triggers which go over parallel boundaries are NOT working yet. They will result in ignored
 * transitions. Maybe in some future version this issue will be tackled.
 * </ul>
 * <li><b>Multiple concurrent triggers</b> (so: different triggerBehaviours with the same trigger that are
 * valid at the same time for the present state of a state machine):
 * <ul>
 * <li><b>General:</b> a TriggerBehaviour whose guard is not fulfilled does NOT count as trigger
 * in the below rules, and as such looses any priority it might have had.
 * <li><b>General:</b> the fact that a triggerBehaviour will result in an ignored fireResult doesn't count in the
 * priorization process: such a trigger will keep its priority, even though its transition doesn't lead to
 * any changes.
 * <li><b>General:</b> Note however, that despite the fact that all situations described below this bullet
 * are possible and can be configured, using the same trigger for different, unrelated transitions is
 * explicitly discouraged as bad practice, as it will easily create confusion and will make the behaviour
 * of the state machine less obvious. We recommend using different triggers for all the below described
 * situations.
 * <li><b>same trigger on different levels</b>: If multiple exit transitions exist for the same trigger
 * for the present status of the state machine, triggerBehaviours declared in a leaf state have higher
 * priority over TriggerBehaviours declared in parent states. The closer to a top level state the
 * TriggerBehaviour is declared, the less priority it has.
 * <li><b>same trigger on same level in parallel state</b>: If multiple exit transitions with the same
 * trigger exist on different states which are both active (as children of a parallel state), and who are
 * the same hierarchy level, then the triggerbehaviour of the first declared state has
 * priority over the triggerbehaviour of later declared states.
 * <li><b>Same trigger on same state</b>: If multiple exit transitions with the same trigger exist for
 * a particular active state, then the triggers have priority in order of declaration. This means that
 * if no guards are defined, the later declared TriggerBehaviour will never be used. Beware that there
 * is no check or warning on such a configuration.
 * </ul>
 * <li><b>Self-targeted transitions</b>, so transtions that point to a state the state machine is already in.
 * <ul>
 * <li><b>self to self</b>: Self targeted triggerBehaviours are forbidden by the configurers. However,
 * if such a self targetted transition is encountered at runtime (e.g. via a Dynamic trigger), then it will
 * be ignored. So if a state machine is in a particular leaf state and it receives a trigger to go to that
 * same leaf state (which it is already in), the triggerBehaviour is ignored and nothing happens.
 * No UnhandledTriggerAction is executed.
 * <li><b>parent to substate already in</b>: When a transition defined on a higher level receives a trigger
 * targetting a substate, and the state machine is already in that particular substate, the event is swallowed,
 * and the substate will NOT be exited and entered again.
 * <li><b>leaf to parent</b>: When a state machine is in a particular leaf state, and it receives a trigger
 * to go to a parent state of that leaf state, there are two possibilities:
 * <ul>
 * <li>the present leaf state is the initial state of the parent state: in this case nothing will happen.
 * No state is exited, no state is entered.
 * <li>the present leaf state is not the initial state of the parent state: in this case the present
 * leaf state will be exited, and the state machine goes/stays in the parent state. As it cannot be in
 * a parent state without being in a leaf state, and as no particular leaf state is specified, it goes in the
 * initial leaf state of the parent state.
 * </ul>
 * </ul>
 * </ul>
 * <p>
 * During a transition, the following order of events is applied:
 * <ol>
 * <li>The leaf state is exited (and it's onexit action is executed).<br>
 * <br>
 * <li>all parents of the leaf state are exited, and their onexit actions are executed. This happens in
 * leaf-to-top-order, so first the direct parent of the leaf state, than the parent of that state, etc etc,
 * up to the top level state of the state machine. This procedure is repeated until the first parent state
 * which the source state and the destination state have in common is met, or until there is no parent
 * state defined anymore.<br>
 * <br>
 * <li>If the state machine is nested in a parallel state of the parent state machine, then:
 * <ol>
 * <li>the child state machine is exited, and it's reference nulled.
 * <li>the same procedure described in all previous points is repeated for all other child state machines
 * which belong to the same parallel state declared in the parent state machine. Child state machines are
 * handled in order of registration into the parent state machine.
 * <li>The parent state machine's parallel state is exited (and it's onexit actions are executed).
 * <li>Item 2 in the list is repeated: parent states are exited until the first common parent between
 * source and destination is encountered (or no parent is present anymore). If new parent state machines
 * are encountered on the way, the same procedure is repeated for them.
 * </ol>
 * <li>When all necessary exits have taken place, the transition's action is executed.<br>
 * <br>
 * <li>The destination is entered in order from parent to leaf state. If child state machines are encountered,
 * these are entered to according to their explicit targets, or if no explicit target is present in that
 * child state machine, according to their declared initial states. OnEntry actions are executed in the
 * same order.
 * </ol>
 *
 * @param <S>
 * @param <T>
 */
public class TransitioningTriggerBehaviour<S, T> extends TriggerBehaviour<S, T> {

    private static final Logger logger = LoggerFactory.getLogger(TransitioningTriggerBehaviour.class);

    protected final S           destination;
    private final Action<S, T>  action;

    public TransitioningTriggerBehaviour(T trigger, S destination,
            Func<Boolean, S, T> guard, Action<S, T> action, S declaredIn) {
        super(trigger, guard, declaredIn);
        this.destination = destination;
        this.action = action;
    }

    public Action<S, T> getAction() {
        return action;
    }

    public S getDestination() {
        return destination;
    }

    @Override
    public void performAction(final IStateMachineContext<S, T> context,
            final Transition<S, T> transition, Object[] args) {
        if (LogSwitch.LOG) {
            if (logger.isDebugEnabled()) {
                MachineRegistry.agentIdToMDC(context);
                logger.debug("being in transition to destination {} as result of trigger {}.",
                        getDestination(), getTrigger());
            }
        }
        S parent = (transition == null) ? null : transition.getDeclaredIn();
        action.doIt(context, transition, parent, args);
    }

    @Override
    public boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest) {
        dest.set(destination);
        return true;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(super.toString());
        result.append(";").append(destination.toString());
        return result.toString();
    }

}
