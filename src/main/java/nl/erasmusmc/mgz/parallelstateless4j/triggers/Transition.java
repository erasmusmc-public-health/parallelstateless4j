package nl.erasmusmc.mgz.parallelstateless4j.triggers;

public class Transition<S, T> {

    private final S source;
    private final S destination;
    private final T trigger;
    private final S declaredIn;

    /**
     * Construct a transition
     *
     * @param source The state transitioned from
     * @param destination The state transitioned to
     * @param trigger The trigger that caused the transition
     * @param declaredIn The state in which it was declared. may be null
     */
    public Transition(S source, S destination, T trigger, S declaredIn) {
        this.source = source;
        this.destination = destination;
        this.trigger = trigger;
        this.declaredIn = declaredIn;
    }

    /**
     * the state the trigger was declared in.
     */
    public S getDeclaredIn() {
        return declaredIn;
    }

    /**
     * The state transitioned to. The state machine behaves as such, that this is always the
     * leafmost state the state machine transitions to, even if the trigger was declared pointing
     * to a non-leaf state.
     * <p>
     * Exception is nested parallel state machines.
     * in case of transitioning into a nested state machine, the destination will be the
     * parallel state in the state machine of the source state, which hosts the declared destination state.
     * In case of exiting out of a nested state machine, there is no exception: the destination will
     * be the leafmost state in the submost state machine that is relevant.
     *
     *
     * @return The state transitioned to
     */
    public S getDestination() {
        return destination;
    }

    /**
     * The state transitioned from. In case of exiting out of a state machine, the
     * reported source is a state on the same state machine as the destination, and the (parallel)
     * state which holds the state machine of the original declared source.
     *
     * @return The state transitioned from
     */
    public S getSource() {
        return source;
    }

    /**
     * The trigger that caused the transition
     *
     * @return The trigger that caused the transition
     */
    public T getTrigger() {
        return trigger;
    }

    /**
     * True if the transition is a re-entry, i.e. the identity transition
     *
     * @return True if the transition is a re-entry
     */
    public boolean isReentry() {
        return getSource().equals(getDestination());
    }
}
