package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.LogSwitch;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;

/**
 * A TriggerBehaviour defines the behaviour of a trigger.
 *
 * @param <S>
 * @param <T>
 */
public abstract class TriggerBehaviour<S, T> {

    private static final Logger       logger = LoggerFactory.getLogger(TriggerBehaviour.class);
    private final T                   trigger;
    private final Func<Boolean, S, T> guard;
    protected boolean                 exiting;
    protected Integer                 resurfacingId;
    protected S                       declaredInState;

    protected TriggerBehaviour(T trigger, Func<Boolean, S, T> guard, S declaredIn) {
        this.trigger = trigger;
        this.guard = guard;
        declaredInState = declaredIn;
    }

    /**
     * gets the state in which this TriggerBehaviour is declared.
     */
    public S getDeclaredInState() {
        return declaredInState;
    }

    public Func<Boolean, S, T> getGuard() {
        return guard;
    }

    /**
     * This method is relevant in a parallel context, where a transition takes place out of a child state machine.
     * If this triggerbehaviour is a part of a complete transition, and it is the part that will find
     * the destination in the present level of parallelism, then it has a resurfacingId.
     * In that case, this TriggerBehaviour forms the last part of a complete transition,
     * where the first part is formed by an exiting triggerbehaviour.
     * The exiting TriggerBehaviour issues the resurfacingId, and the corresponding ResurfacingTriggerBehaviour
     * can be recognized by this id.
     * If this TriggerBehaviour is NOT resurfacing, then the result is null.
     *
     */
    public Integer getResurfacingId() {
        return resurfacingId;
    }

    public T getTrigger() {
        return trigger;
    }

    /**
     * @return true if this triggerbehaviour is a part of a complete transition, and it is the part that exits from
     * a child state machine.
     * @author rinke
     */
    public boolean isExiting() {
        return exiting;
    }

    public boolean isGuardConditionMet(IStateMachineContext<S, T> context, Object[] args) {
        if (guard == null) {
            return true;
        }
        try {
            boolean result = guard.call(context, args);
            if (LogSwitch.LOG) {
                if (!result && logger.isTraceEnabled()) {
                    logger.trace("Guard condition of TriggerBehaviour {} not met.", this.trigger.toString());
                }
            }
            return result;
        } catch (Exception e) {
            logger.warn("Failed to evaluate guard condition: {}, assuming false.", e.getMessage());
            return false;
        }
    }

    /**
     * @return true if this triggerbehaviour is a part of a complete transition, and it is the part that will find
     * the destination in the present level of parallelism. This part forms the last part of a complete transition,
     * where the first part is formed by an exiting triggerbehaviour.
     *
     */
    public boolean isResurfacing() {
        return (resurfacingId != null);
    }

    /**
     * performs the Action when the transition takes place. Has no parent argument,
     * as the parent is the state in which the trigger was declared, and this
     * is read from the transition object.
     *
     * @param context
     * @param transition
     * @param args
     */
    public abstract void performAction(IStateMachineContext<S, T> context,
            Transition<S, T> transition, Object[] args);

    public abstract boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest);

    @Override
    public String toString() {
        return "TriggerBehaviour:" + trigger.toString();
    }

}
