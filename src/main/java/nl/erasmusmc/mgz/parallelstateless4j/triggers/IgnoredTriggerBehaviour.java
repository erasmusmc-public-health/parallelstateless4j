package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.LogSwitch;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;

/**
 * TriggerBehaviour for triggers explicitly configured to be ignored. Doesn't make much sense in the
 * present configuration, but did make sense in a configuration where every not found trigger
 * resulted in an exception.
 * Left in the present version for backwards compatibility.
 *
 * @param <S>
 * @param <T>
 */
public class IgnoredTriggerBehaviour<S, T> extends TriggerBehaviour<S, T> {

    private static final Logger logger = LoggerFactory.getLogger(IgnoredTriggerBehaviour.class);
    private final Action<S, T>  action;

    public IgnoredTriggerBehaviour(T trigger, Func<Boolean, S, T> guard,
            Action<S, T> action, S declaredIn) {
        super(trigger, guard, declaredIn);
        this.action = action;
    }

    @Override
    public void performAction(IStateMachineContext<S, T> context,
            Transition<S, T> transition, Object[] args) {
        if (LogSwitch.LOG) {
            if (logger.isDebugEnabled()) {
                MachineRegistry.agentIdToMDC(context);
                logger.debug("Ignored trigger {} fired; "
                        + "stateMachine doesn't transition, but action is executed.",
                        getTrigger());
            }
        }
        action.doIt(context, transition, declaredInState, args);
    }

    @Override
    public boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest) {
        return false;
    }
}
