package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;

/**
 * A TriggerBehaviour which doesn't do anything because its condition did fail. This type was
 * created in order to distinguish between situations in which no triggerBehaviour was found,
 * and a situation in which a triggerbehavior was found, but its condition failed. Now it is possible
 * to define different behaviour for those different situations.
 * <p>
 * In general,the state machine tries to find a triggerbehaviour in the state. If it doesn't find
 * anything, it will look in the parent state. It will also look into the parent if it only finds a
 * triggerbehaviour with a failed condition. If in none of the parents a valid TriggerBehaviour is found,
 * and in the whole process at least one TriggerBehaviour with a failing condition was found, only then
 * the FailedCondtionTriggerBehaviour will be applied.
 * The class itself does nothing at all, but merely serves as a flag to mark such situations.
 *
 * @author rinke
 *
 * @param <S>
 * @param <T>
 */
public class FailedConditionTriggerBehaviour<S, T> extends TriggerBehaviour<S, T> {

    public FailedConditionTriggerBehaviour() {
        super(null, null, null);
    }

    @Override
    public void performAction(IStateMachineContext<S, T> context,
            Transition<S, T> transition, Object[] args) {
        // no need to do anything. This is never called (no transition => no action)
    }

    @Override
    public boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest) {
        return false;
    }
}
