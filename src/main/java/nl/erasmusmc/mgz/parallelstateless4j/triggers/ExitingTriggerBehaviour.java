package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.LogSwitch;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;

/**
 * a special type of TransitioningTriggerBehaviour which causes the leaving of a child state machine.
 * It has no destination and no Action (because actions are always executed after ALL states to be left
 * are exited, and before all destination states are entered; so Action must be executed in the parent).
 * It causes the state machine to go out of every state it is in, applying onExits. It doesn't enter
 * any new states in the state machine.
 * <p>
 *
 * Usually, after all states are exited, the state machine instance is ready to be destroyed. This type of
 * action is used when a child state machine is left, because a parallel state on the parent state machine is exited.
 *
 * @author rinke
 *
 * @param <S>
 * @param <T>
 */
public class ExitingTriggerBehaviour<S, T> extends TransitioningTriggerBehaviour<S, T> {

    private static final Logger logger = LoggerFactory.getLogger(ExitingTriggerBehaviour.class);
    private static int          nextId = 0;
    private boolean             resurfacing;

    /**
     * creates a normal ExitingTriggerBehaviour for the transition part that goes out of the most
     * deeply nested state machine.
     *
     */
    public ExitingTriggerBehaviour(T trigger, Func<Boolean, S, T> guard, S declaredIn) {
        super(trigger, null, guard, null, declaredIn);
        exiting = true;
        resurfacingId = Integer.valueOf(nextId++);
        // resurfacing is false, as it is the first TriggerBehaviour in the chain.
        // Note that the resurfacingId is NOT null though.
    }

    /**
     * creates a special case of an ExitingTriggerBehaviour, being an ExitingTriggerBehaviour which is
     * at the same time a ResurfacingTriggerBehaviour (in the sense that it is resurfacing).
     * This constructor is to be used in cases of nested parallels which go three or more levels deep. If
     * the deepest (3rd or more) nested statemachine points to a state in the highest (top) level, then:
     * <ul>
     * <li>the deepest nested state machine must use a normal ExitingTriggerBehaviour,
     * <li>the target (top) level state machine must use a ResurfacingTriggerBehaviour,
     * <li>Any in between layer of state machines must use a ResurfacingTriggerBehaviour that is also
     * an ExitingTriggerBehaviour, as that level is exited to from the child state machine, and must
     * pass control to its parent state machine.
     * </ul>
     *
     * This constructor is to be used for the latter case.
     *
     * @param trigger
     */
    public ExitingTriggerBehaviour(T trigger, Integer resurfacingId) {
        super(trigger, null, null, null, null);
        exiting = true;
        this.resurfacingId = resurfacingId;
        resurfacing = true;
    }

    /**
     * This method ensures that intermediate ExtingTriggerBehaviours can also be the ResurfacingTriggerbehaviour of
     * the next level up.
     */
    @Override
    public boolean isResurfacing() {
        return resurfacing;
    }

    @Override
    public void performAction(IStateMachineContext<S, T> context,
            Transition<S, T> transition, Object[] args) {
        if (LogSwitch.LOG) {
            if (logger.isDebugEnabled()) {
                MachineRegistry.agentIdToMDC(context);
                logger.debug("Exiting child state machine as result of trigger {}.",
                        getTrigger());
            }
        }
    }

    @Override
    public boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest) {
        return true;
    }
}
