package nl.erasmusmc.mgz.parallelstateless4j.triggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.LogSwitch;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;

/**
 * Special type of TriggerBehaviour used in case of reentry. When a trigger is targetted to a
 * state where it is declared (so targetting itself), and this is explicitly configured as a
 * reentry action, then the state machine will exit the targetted state, and immediately
 * enter it again. The direct parent state and super parent states of the targetted state will
 * not be entered or exited. Substates will of course be exited, as a state cannot be exited
 * without exiting its substates.
 * <p>
 * Note that (for the time being) parallel states are not allowed: when a reentry action encouters
 * some parallel state in some of the substates of the targetted state which must be exited and
 * reentered again, then an exception will be raised. Reentry inside a parallel state machine
 * (so: in a state which is a substate of some parallel state) is not a problem though.
 * <p>
 * If the targetted state is a leaf state, then the state machine status will not change. The
 * onentry and onexit actions of the state will be executed though.
 * <p>
 * If however the state targetted is a non-leaf state, then the state machine status MAY change,
 * depending on the fact if the state machine is in the initial state of the targetted state or not.
 * Reentering a state means that no child leaf state is specified, which means that the
 * initial child state will be entered. If the state machine was in a non-initial child state
 * of the targeted reentered state, then it means there will be an actual transition from
 * the non-initial child state to the initial child state.
 * <br>
 * example 1:<br>
 *
 * <pre>
 * config.configure("parent");
 * config.configure("state1").substateOf("parent");
 * config.configure("state2").substateOf("parent");
 * config.configure("parent").permitReentry(trigger);
 * </pre>
 * <p>
 * example 2:<br>
 *
 * <pre>
 * config.configure("parent");
 * config.configure("state1").substateOf("parent");
 * config.configure("state2").substateOf("parent");
 * config.configure("state2").permit("parent", trigger);
 * </pre>
 * <p>
 * The difference between the two examples is:
 * <ul>
 * <li>In example 1 the parent state is exited and entered again. In example 2 the parent state is never exited.
 * <li>when being in state2, the end result in both examples is the same (except that the onentry and onexit of
 * parent are executed in example 1). That end result is that the state machine ends up in state1.
 * <li>When being in state1 in example 1, there is no change in states at receiving the trigger, but the onentry
 * and onexit actions of state1 and parent are executed.
 * <li>In example 2 there is never a transition when in state1.
 * </ul>
 *
 * @param <S>
 * @param <T>
 */
public class ReentryTriggerBehaviour<S, T> extends TriggerBehaviour<S, T> {

    private static final Logger logger = LoggerFactory.getLogger(ReentryTriggerBehaviour.class);

    protected final S           destination;
    private final Action<S, T>  action;

    public ReentryTriggerBehaviour(T trigger, S destination, Func<Boolean, S, T> guard, Action<S, T> action) {
        super(trigger, guard, null);
        this.destination = destination;
        this.action = action;
    }

    public Action<S, T> getAction() {
        return action;
    }

    public S getDestination() {
        return destination;
    }

    @Override
    public void performAction(IStateMachineContext<S, T> context,
            Transition<S, T> transition, Object[] args) {
        if (LogSwitch.LOG) {
            if (logger.isDebugEnabled()) {
                MachineRegistry.agentIdToMDC(context);
                logger.debug("being in transition to destination {} as result of trigger {}.",
                        getDestination(), getTrigger());
            }
        }
        S parent = (transition == null) ? null : transition.getDeclaredIn();
        action.doIt(context, transition, parent, args);
    }

    @Override
    public boolean resultsInTransitionFrom(IStateMachineContext<S, T> context, S source, Object[] args, OutVar<S> dest) {
        dest.set(destination);
        return true;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(super.toString());
        result.append(";").append(destination.toString());
        return result.toString();
    }

}
