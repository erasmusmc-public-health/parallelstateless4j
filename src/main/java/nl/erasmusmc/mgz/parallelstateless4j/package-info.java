/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
/**
 * Main package for the ParallelStateless4j library.
 *
 * <h2>History</h2>
 * this project was forked from <a href="https://github.com/oxo42/stateless4j">Stateless4j</a>
 * by first8.nl for erasmusmc.nl. After that, erasmusmc.nl took over, and most adaptations
 * were done by Rinke Hoekstra of erasmusmc.nl.
 *
 * <h2>Goal</h2>
 * At erasmusmc we needed a fast state machine implementation in java, and stateless4j prooved
 * to be the fasted, but was seriously lacking some features. So we've added those
 * features by ourselves in this project.
 *
 * <h2>Added features</h2>
 * <p>
 * Added functionality in this project:
 * <ul>
 * <li>add a StateMachineContext, so the state machine can make changes to an underlying
 * object associated with it.
 * <li>add support for parallel state machines; the concept of parallel state machines is
 * described <a target="_blank" href="https://www.w3.org/TR/scxml/#parallel">here</a>. A
 * parallel state encapsulates a set of (sub)states which are all simultaneously active,
 * when the parallel state is active. See the javadoc of
 * {@link ParallelStateMachineConfig} for a description of the approach followed.
 * <li>stateless4j prior versions didn't know the concept of initial states.
 * An initial state is the (sub)state in which the state machine automatically goes
 * when no specific substate of a state is specified. This is the first registered
 * substate of a state, unless explicitly otherwise configured.
 * <li>stateless4j allowed the state machine being in a state, without being in one
 * of the substates of the state. This is not possible in the present fork. A State
 * Machine MUST always be in a leaf state (= a state with no substates). If no specific
 * leaf state is specified, it will go into the initial state(s) until a leaf state is
 * reached.
 * <li>Many performances improvements were applied to the code. For example the abundant use
 * of java 8 streams: java 8 streams are slow in the case of typical state machine scenarios
 * with relatively small collections (because it costs relatively a lot of time to
 * initialize them). All java 8 streams where replaced by plain collection iterations.
 * <li>Various code improvements, for example the use of guava's multimap in stead of the
 * tedious <code>Map<key, List<value>></code>.
 * <li>add improved logger possibilities.
 * <li>added an extra parameter <code>parent</code> to the Action interface, so that actions
 * can access the parent state in which they are hosted. This is needed for logging, and
 * for particular actions aimed at the parent state (or its hosting state machine).
 * <li>Heavy internal refactoring, to make a better separation between runtime and configuration
 * time classes.
 * </ul>
 *
 * <h2>This package</h2>
 *
 * Contains classes that hold the actual state machine object and its state, needed at runtime for a
 * state machine. Doesn't contain any configuration classes needed at designtime.
 * Contains both interfaces and implementations.
 * <p>
 * The {@link StateMachine} class is the entry class, which is usually passed a {@link MachineConfigurer} object
 * at construction time. The configurer itself is not used, except for the {@link MachineRegistry}getter.
 * The Registry gives access to the whole configured state machine structure. The <code>StateMachine</code>
 * class itself just maintains the actual state where the machine is in, via the <{@link StateMachineStatus} class.
 *
 *
 */
package nl.erasmusmc.mgz.parallelstateless4j;
