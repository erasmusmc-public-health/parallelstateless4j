/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.Collection;

/**
 * reports that a property has changed on the state machine context.
 * @author rinke
 *
 */
public interface IContextPropertyListener {

    /**
     * reports the change of a property on the state machine context.
     *
     * @param <S> the state type
     * @param <T> the trigger type
     * @param context the state machine context instance in which the change of the property took place. May not be null.
     * @param name the name of the property that changed. May not be null.
     * @param oldValue the old value of the property. May be null, in case that was the old value.
     * @param newValue the new value of the property. May be null, in case that is the new value.
     */
    <S, T> void onChange(IStateMachineContext<S, T> context, String name, Object oldValue, Object newValue);

    /**
     * reports the change of a collection property on the state machine context.
     *
     * @param <S> the state type
     * @param <T> the trigger type
     * @param <E> the type of the elements of the collection
     * @param context the state machine context instance in which the change of the property took place. May NOT be null.
     * @param name the name of the property that changed. This property has a Collection as value. May NOT be null.
     * @param additions a Collection containing elements that were added to the original collection. May be null.
     * @param removals a Collection containing elements that were removed from the original collection. May be null.
     * @param newValue the value of the property after the change, being a Collection. May be null, in case that is the new value.
     */
    <S, T, E> void onCollectionChange(IStateMachineContext<S, T> context, String name, Collection<E> additions,
            Collection<E> removals, Collection<E> newValue);

}
