/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.List;

import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.StateRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;

/**
 * General interface for the StateMachine object. Versions of Stateless4j lacked this interface.
 *
 * @param <S> The type used to represent the states of the contexted
 * state machine that this context belongs to
 * @param <T> The type used to represent the triggers that cause state
 * transitions of the contexted state machine that this context belongs to
 *
 * @author rinke
 */
public interface IStateMachine<S, T> {

    /**
     * exits all current states and leaves the state machine.
     * Also exits child state machines, if any.
     */
    void exit(T trigger, Object[] args);

    /**
     * Transition from the current state via the specified trigger.
     * The target state is determined by the configuration of the current state.
     * Actions associated with leaving the current state and entering the new one
     * will be invoked.
     *
     * @param trigger The trigger to fire
     * @param args the arguments that will be passed to entry and exit actions
     *
     */
    void fire(final T trigger, final Object... args);

    /**
     * gets the current {@link StateRegistry}. Returns null if the state machine is exited.
     */
    StateRegistry<S, T> getCurrentStateRegistry();

    /**
     * gets the current {@link StateRegistry}. Throws an exception if the registry appears to be null
     * (in which case the state machine is exited.
     * @throws IllegalStateException if the state machine is exited.
     */
    StateRegistry<S, T> getCurrentStateRegistryNotNull();

    /**
     * The currently-permissible trigger values, for the current state.
     *
     * @return The currently-permissible trigger values
     */
    List<T> getPermittedTriggers(Object... args);

    /**
     * Get the {@link IStateMachineContext} of this contexted state machine.
     * This context allows for the storing of context information for this state
     * machine.
     *
     * @return the context of this state machine
     */
    IStateMachineContext<S, T> getStateMachineContext();

    /**
     * Retrieves the current state of the state machine including all parallel states active.
     * The result is cached. As long as the state machine doesn't change,
     * the cache is returned.
     *
     * @return StateMachineState the state of the state machine. Returns null if the state
     * machine has been left, and there is no state anymore.
     *
     */
    StateMachineStatus<S> getStateMachineStatus();

    /**
     * returns true if this state machine is exited, that is: if it is not
     * in any state anymore, and can be killed.
     *
     */
    boolean isExited();

    /**
     * Sets the behaviour at encountering a transtion guard (condition) which is failing.
     * It sets this for this particular <code>StateMachine</code> instance. By default,
     * this behaviour is defined by the corresponding setting in the Registry object, which
     * sets it for all instances. This method however allows you to override this default
     * behaviour for only this instance. <br>
     * A null action may be specified, which means that the instance falls back to the
     * value defined in the registry object.
     *
     */
    void onFailedConditionTrigger(final Action<S, T> failedConditionTriggerAction);

    /**
     * Sets the behaviour on encountering an unhandled trigger for this particular
     * <code>StateMachine</code> instance. By default, this behaviour is defined by the
     * corresponding setting in the Registry object, which sets it for all instances.
     * This method however allows you to override this default behaviour for only this
     * instance. <br>
     * A null action may be specified, which means that the instance falls back to the
     * value defined in the registry object.
     *
     * @param unhandledTriggerAction An action to call when an unhandled trigger is fired
     */
    void onUnhandledTrigger(final Action<S, T> unhandledTriggerAction);

    /**
     * gets the registry, the object which stores the configuration for runtime use.
     * @return
     */
    MachineRegistry<S, T> registry();

    /**
     * checks if the state machine changed. A state machine only will be
     * regarded unchanged, if the cachedState equals the current underlying state,
     * and if any relevant parallel child state machine did not change.
     * Parent state machines are not relevant for this.
     */
    boolean unChanged();

}
