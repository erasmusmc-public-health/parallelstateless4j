package nl.erasmusmc.mgz.parallelstateless4j.delegates;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;

/**
 * Represents a function that produces a result.
 *
 * @param <R> Result type
 */
public interface Func<R, S, T> {

    /**
     * Applies this function to the state machine context, adds some optional arguments,
     * and then produces some result.
     *
     * @return Result
     */
    R call(IStateMachineContext<S, T> context, Object... args);
}
