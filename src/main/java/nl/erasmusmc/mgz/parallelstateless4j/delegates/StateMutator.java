package nl.erasmusmc.mgz.parallelstateless4j.delegates;

/**
 * functional interface for a state acceptor, usually the state setter.
 * @param <S>
 */
@FunctionalInterface
public interface StateMutator<S> {

    void accept(S state);
}
