package nl.erasmusmc.mgz.parallelstateless4j.delegates;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.Transition;

/**
 * Represents an operation that returns no result.
 */
public interface Action<S, T> {

    /**
     * performs the passed operation of this action.
     * @param context - the stateMachineContext.
     * @param transition - the transition causing the state change. May be null.
     * @param parent - the state on which the action is declared, in case of onEntry or onExit actions.
     * If the action is attached to a triggerBehaviour, this will be null.
     * @param args - arguments passed, usually with the {@link StateMachine#fire(T,Object..)} method.
     */
    void doIt(IStateMachineContext<S, T> context, Transition<S, T> transition, S parent, Object... args);
}
