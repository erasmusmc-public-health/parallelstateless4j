package nl.erasmusmc.mgz.parallelstateless4j.delegates;

/**
 * Functional interface for a State getter.
 *
 * @param <S>
 */
@FunctionalInterface
public interface StateAccessor<S> {

    S call();
}
