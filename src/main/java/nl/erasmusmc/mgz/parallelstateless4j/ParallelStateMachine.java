/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.ParallelRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ReentryTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.Transition;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TransitioningTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TriggerBehaviour;

/**
 * A subtype of StateMachine, used for running StateMachines inside a parallel state. This is
 * a child state machine which runs inside a parent StateMachine.
 * <p>
 * A Parallel State is a state machine state which holds substates, where each substate
 * will be entered simultaneously when the parallel state becomes active.
 * For each of those substates, a nested state machine object will be instantiated when
 * the parallel state becomes active.
 * <p>
 * Parallel states can be nested. Though technically possible, it doesnt' make sense to declare
 * a parallel state with less then two substates. Doing so is strongly discouraged, as it
 * won't give you any functionality above using normal states, but it will make your code slower.
 * <p>
 * Parallel states follow the same rules as normal states, see the javadoc of {@link StateMachine}.<br>
 * The javadoc of {@link ParallelMachineConfigurer} contains details on the technical aspects of
 * the parallel implementations. <br>
 * The javadoc of {@link TransitioningTriggerBehaviour} contains rules on transitions, including
 * rules for transitions over parallel boundaries of state machines.
 * <p>
 * Note that Dynamic triggers are not tested yet in a parallel context. It is likely that they will fail.
 * <p>
 * ParallelStateMachines do not inherit any exitlisteners from the parent state machine.
 *
 * @param <S> The type used to represent the states
 * @param <T> The type used to represent the triggers that cause state transitions
 *
 * @author rinke
 */
class ParallelStateMachine<S, T> extends StateMachine<S, T> {

    @SuppressWarnings("unused")
    private static final Logger      logger = LoggerFactory.getLogger(ParallelStateMachine.class);
    private final StateMachine<S, T> parentStateMachine;

    // ##################################### CONSTRUCTORS ###############################################
    // ###################################################################################################

    /**
     * Construct a state machine with a parent state machine. This is usually done for a parallel setting.
     * This constructor creates a state machine with internal state storage via a StateReference object.
     */
    public ParallelStateMachine(final MachineRegistry<S, T> registry, final IStateMachineContext<S, T> context,
            StateMachine<S, T> parentStateMachine) {
        this(registry, context, parentStateMachine, null);
    }

    /**
     * constructs a state machine with a parent state machine, which is immediately put into a target state
     * by the trigger which is passed as argument. So this constructor doesn't follow the normal initial state,
     * but uses a trigger to send it to the correct state.
     *
     */
    ParallelStateMachine(final MachineRegistry<S, T> registry, final IStateMachineContext<S, T> context,
            StateMachine<S, T> parentStateMachine, final T trigger, Object... args) {
        super(registry, context, null, null);
        this.parentStateMachine = parentStateMachine;
        // we do NOT initialize to first state here. See https://www.pivotaltracker.com/story/show/181349061
        // initializeToFirstState(trigger, args);
    }

    // ##################################### other methods ###############################################
    // ###################################################################################################

    @Override
    public void fire(final T trigger, final Object... args) {
        throw new UnsupportedOperationException("This method can only be called for top-level state machines. ");
    }

    @Override
    public void initializeToFirstState() {
        // does nothing as taken over by overloaded version.
    }

    @Override
    public void onFailedConditionTrigger(final Action<S, T> failedConditionTriggerAction) {
        throw new UnsupportedOperationException("This method can only be called for top-level state machines. ");
    }

    @Override
    public void onUnhandledTrigger(final Action<S, T> unhandledTriggerAction) {
        throw new UnsupportedOperationException("This method can only be called for top-level state machines. ");
    }

    /**
     * {@inheritDoc}
     *
     * @param uses this trigger to send the state machine to the state targeted by the trigger.
     * If this is null, the state machine uses the initial state for initialization.
     *
     */
    protected void initializeToFirstState(T trigger, Object... args) {
        DestinationRecord destinationRecord = null;
        TriggerBehaviour<S, T> triggerBehaviour = null;
        // this block handles incoming triggers from parent state machine
        if (trigger != null) {
            // parameter checking not needed, is already done in oringial
            final ParallelRegistry<S, T> presentRegCastedAsParallel = (ParallelRegistry<S, T>) registry;
            triggerBehaviour = presentRegCastedAsParallel.tryFindHandler(stateMachineContext, trigger, args);
            if (triggerBehaviour != null && !(triggerBehaviour instanceof ReentryTriggerBehaviour)) {
                destinationRecord = resolveDestination(triggerBehaviour, args);
            }
        }
        // initialState is the destinationRecord in case of a specified trigger, or the nominal initialState.
        final S initialState = (destinationRecord == null)
                ? registry.retrieveInitialState()
                : destinationRecord.destination;

        // if (triggerBehaviour != null) {
        // triggerBehaviour.performAction(stateMachineContext, args);
        // }
        stateMutator.accept(initialState);
        if (stateMachineContext.getStateMachine() == null) {
            stateMachineContext.setStateMachine(this);
        }

        if (registry.isEntryActionOfInitialStateEnabled()) {
            final Transition<S, T> initialTransition = new Transition<>(initialState, initialState, null, null);
            getCurrentStateRegistryNotNull().initEnter(stateMachineContext, initialTransition, args);
        }
        // .. then in children.
        initializeChildStateMachines(destinationRecord, args);
        cachedState = stateAccessor.call();
    }

    @Override
    protected StateMachine<S, T>.DestinationRecord resolveDestinationInParent(OutVar<S> destination,
            TriggerBehaviour<S, T> triggerBehaviour, Object... args) {
        DestinationRecord result = new DestinationRecord();
        result = parentStateMachine.resolveDestination(triggerBehaviour, args);
        result.targetRegistry = parentStateMachine.registry;
        result.targetStateMachineTrigger = triggerBehaviour.getTrigger();
        return result;
    }

    @Override
    protected void setExitTarget(StateMachine<S, T>.DestinationRecord result) {
        result.targetRegistry = parentStateMachine.registry;
    }

    /**
     * @author Ejnar
     */
    StateMachine<S, T> getParentStateMachine() {
        return parentStateMachine;
    }

}
