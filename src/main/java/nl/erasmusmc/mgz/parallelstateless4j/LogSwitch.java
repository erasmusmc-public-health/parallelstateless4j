/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

/**
 * Common switch for logging.
 * <p>
 * The idea is to wrap every log message in an <code>if (LogSwitch.LOG) {...</code>.
 * The field is usually set to <code>false</code>, especially when publishing definitive
 * code. Because the field is <code>static final</code>, the compiler will strip all
 * logging statements from the eventual code, as long as they are wrapped in the if.
 * This prevents the logging (which is mainly used for debugging) from slowing down
 * published code.
 * <p>
 * However, when you want logging enabled, create a special version with this field set to
 * <code>true</code>. This version could be used for debugging.
 *
 * @author rinke
 */
public final class LogSwitch {

    public static final boolean LOG = false;
}
