/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.Set;

/**
 * gives information on the actual states a State Machine is in.
 *
 * @author rinke
 *
 * @param <S> Type of state.
 *
 */
public interface IStateMachineStatus<S> {

    /**
     * gets all the leaf states a state machine is in.
     */
    Set<S> getLeafStates();

    /**
     * gets all the leaf states a state machine is in. Difference with
     * <code>getLeafStates()</code> is that this gives the direct underlying
     * field, which thus could be modified. The advantage is that it is faster.
     * Only use in cases where the underlying Set cannot be changed and
     * where performance is essential, for example inside a loop over a whole population.
     */
    Set<S> getLeafStatesUnsafe();

    /**
     * gets all the states and superstates a statemachine is in.
     */
    Set<S> getStates();

    /**
     * Analogous to {@linkplain #getLeafStatesUnsafe()}; see explanation there.
     */
    Set<S> getStatesUnsafe();

    /**
     * returns true if the state machine is in the specified state.
     */
    boolean isInState(S state);

    /**
     * returns true if the state machine is in any of the specified states.
     * @param states - if null or empty, always returns true.
     * @return
     */
    @SuppressWarnings("unchecked")
    default boolean isInStates(S... states) {
        if (states == null || states.length == 0) {
            return true;
        }
        for (S state : states) {
            if (isInState(state)) {
                return true;
            }
        }
        return false;
    }

    /**
     * returns true if any of the states a state machine is in, is a parallel state. This means that there
     * is more than one leaf state.
     */
    default boolean isParallel() {
        return getLeafStates().size() > 1;
    }

}
