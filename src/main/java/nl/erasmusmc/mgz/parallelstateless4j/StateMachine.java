/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;

import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineConfigurer;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.ParallelRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.configuration.StateRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.StateAccessor;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.StateMutator;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.DynamicTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ExitingTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.FailedConditionTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.IgnoredTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ParameterizedTrigger;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ReentryTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.Transition;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TransitioningTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TriggerBehaviour;

/**
 * Models behaviour as transitions between a finite set of states
 * <p>
 * The following general rules apply for simple, one level deep state machines (so without parallelism):
 * <ul>
 * <li>A state machine is always in exactly one leaf state. A leaf state is a state without any substates.
 * <li>The present state where the state machine is in is called the active state.
 * <li>Each state can have 0 to many substates.
 * <li>each substate has exactly one parent state, which is called the superstate. The superstate is the
 * state containing the substate.
 * <li>A state machine cannot go into a state without going into one of its substates.
 * <li>One of the substates of a state is the initial state. The initial state is the default substate
 * of a state. The state machine will go into this initial state, if it is entering the state without
 * having specified which substate it should go into.
 * </ul>
 *
 * For rules on transitions, see the javadoc of {@link TransitioningTriggerBehaviour}.<br>
 * For rules on parallel states, see the javadoc on {@link ParallelStateMachine}.
 *
 *
 * @param <S> The type used to represent the states
 * @param <T> The type used to represent the triggers that cause state transitions
 */
public class StateMachine<S, T> implements IStateMachine<S, T> {

    /**
     * a simple container holding all necessary objects needed to determine a transition destination.
     * No record, as fields must be set separately from each other.
     */
    protected class DestinationRecord {
        S                     destination;
        MachineRegistry<S, T> targetRegistry;
        T                     targetStateMachineTrigger;

        @Override
        public String toString() {
            return destination.toString();
        }
    }

    /**
     * an enum with possible results of a trigger fire.
     */
    protected enum FireResult {
        /**
         * trigger is successfully fired and transition completely handled
         */
        HANDLED,
        /**
         * trigger is fired, transition is partially completed in a child state machine, but still to be handled in the parent.
         */
        PARTIAL,
        /**
         * transition failed.
         */
        FAILED,
        /**
         * Transition is not (yet) handled. This is the null situation.
         */
        UNHANDLED,
        /**
         * The trigger is ignored, and nothing changes in the state machine, because the state machine was
         * already in the state which was the target. This is not the same as handled, because nothing happend.
         * It is also not the same as failed or unhandled, because the state machine is eventually at the
         * desired state.
         */
        IGNORED;

        protected static FireResult[] VALUES = FireResult.values();

    }

    private static final Logger                   logger                = LoggerFactory.getLogger(StateMachine.class);

    protected final MachineRegistry<S, T>         registry;
    protected StateAccessor<S>                    stateAccessor;
    protected StateMutator<S>                     stateMutator;
    protected final IStateMachineContext<S, T>    stateMachineContext;
    private final List<StateMachine<S, T>>        parallelStateMachines = new ArrayList<>(5);
    protected S                                   cachedState;
    private StateMachineStatus<S>                 cachedStatus;
    /**
     * An exit code is generated if a state machine is left due to an ExitingTriggerBehaviour.
     * The exit code must be communicated to the containing state machine, which is this object,
     * one level higher up in the parallelism tree. This field is used to set that exitcode
     * for communication from {@link #privateFireChildren(StateRegistry, Object, Object...)} to
     * {@link #privateFire(Object, Object...)}.
     */
    private Integer                               exitCode;

    /**
     * local unhandledTriggerAction, see {@link MachineRegistry#getUnhandledTriggerAction()}.
     * The field/getter at <code>StateMachineRegistry</code> controls the behaviour for all
     * state machine instances, while this field in StateMachine allows to override the
     * behaviour for a particular instance.
     */
    private Action<S, T>                          unhandledTriggerAction;

    /**
     * local failedConditionTriggerAction, see {@link MachineRegistry#getFailedConditionTriggerAction()}.
     * The field/getter at <code>StateMachineRegistry</code> controls the behaviour for all
     * state machine instances, while this field in StateMachine allows to override the
     * behaviour for a particular instance.
     */
    private Action<S, T>                          failedConditionTriggerAction;

    private List<IStateMachineExitListener<S, T>> exitListeners;

    private boolean                               initialized           = false;

    /**
     * construct a state machine with internal state storage.
     *
     * @author rinke
     */
    public StateMachine(final MachineConfigurer<S, T> config, IStateMachineContext<S, T> context) {
        this(config, context, true);
    }

    /**
     * construct a state machine with internal state storage.
     * If <code>initializeToFirstState</code> is false, you need to
     * explicitly set it to the first state by a call to {@linkplain #initializeToFirstState()}.
     *
     *
     * @author rinke
     */
    public StateMachine(final MachineConfigurer<S, T> config, IStateMachineContext<S, T> context,
            boolean initializeToFirstState) {
        this(config.getRegistry(), context, config.getContextListeners(),
                config.getExitListeners(), initializeToFirstState);
    }

    /**
     * constructs a state machine with external state storage
     *
     * @author rinke
     *
     */
    public StateMachine(final StateAccessor<S> stateAccessor, final StateMutator<S> stateMutator,
            final MachineConfigurer<S, T> config, IStateMachineContext<S, T> context) {
        registry = config.getRegistry();
        stateMachineContext = context;
        this.stateAccessor = stateAccessor;
        this.stateMutator = stateMutator;
        context.setListeners(config.getContextListeners());
        exitListeners = config.getExitListeners();
        initializeToFirstState();
    }

    /**
     * constructs a state machine from a child class, by giving the registry directly.
     *
     * @author rinke
     * @param multimap
     */
    protected StateMachine(final MachineRegistry<S, T> registry, IStateMachineContext<S, T> context,
            Multimap<String, IContextPropertyListener> contextPropertyListeners,
            List<IStateMachineExitListener<S, T>> exitListeners) {
        this(registry, context, contextPropertyListeners, exitListeners, true);
    }

    /**
     * constructs a state machine from a child class, by giving the registry directly.
     * If <code>initializeToFirstState</code> is false, you need to
     * explicitly set it to the first state by a call to {@linkplain #initializeToFirstState()}.
     *
     * @author rinke
     * @param multimap
     */
    protected StateMachine(final MachineRegistry<S, T> registry, IStateMachineContext<S, T> context,
            Multimap<String, IContextPropertyListener> contextPropertyListeners,
            List<IStateMachineExitListener<S, T>> exitListeners, boolean initializeToFirstState) {
        this.registry = registry;
        stateMachineContext = context;
        final StateReference<S> reference = new StateReference<>();
        stateAccessor = reference::getState;
        stateMutator = reference::setState;
        context.setListeners(contextPropertyListeners);
        this.exitListeners = exitListeners;
        if (initializeToFirstState) {
            initializeToFirstState();
        }
    }

    /**
     * {@inheritDoc}
     *
     * All ExitListeners are notified upon exit, but usually this happens only for top level state machines.
     * sub-statemachines do not report on exit.
     *
     * @author rinke
     */
    @Override
    public void exit(T trigger, Object[] args) {
        if (!isExited()) {
            killSubStateMachines(trigger, args);

            if (LogSwitch.LOG) {
                if (logger.isDebugEnabled()) {
                    MachineRegistry.agentIdToMDC(stateMachineContext);
                    logger.debug("Exiting complete sub-statemachine of state {} due to trigger {}",
                            this.getState(), trigger.toString());
                }
            }

            final Transition<S, T> transition = new Transition<>(getState(), null, trigger, null);
            final StateRegistry<S, T> current = getCurrentStateRegistryNotNull();
            current.exitTree(stateMachineContext, transition, args);
            setState(null);
            if ((exitListeners != null) && !exitListeners.isEmpty()) {
                for (int i = 0, iSize = exitListeners.size(); i < iSize; i++) {
                    IStateMachineExitListener<S, T> exitListener = exitListeners.get(i);
                    exitListener.onExit(this);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * This method calls {@link #privateFire(Object, Object...)} for execution. The only
     * difference is that this method calls the <code>unhandledTriggerAction</code>
     * if the firing fails.
     *
     * @author rinke (rewritten, allowing swallowing trigger)
     */
    @Override
    public void fire(final T trigger, final Object... args) {
        requireNonNull(trigger, "trigger is null");
        final FireResult result = privateFire(trigger, args);
        switch (result) {
            case FAILED:
                doFailedConditionTriggerAction(trigger);
                break;
            case IGNORED, HANDLED:
                // do nothing
                break;
            case PARTIAL, UNHANDLED:
            default:
                final S state = getCurrentStateRegistryNotNull().getUnderlyingState();
                final Transition<S, T> transition = new Transition<>(state, null, trigger, null);
                getUnhandledTriggerAction().doIt(stateMachineContext, transition, null);

        }
    }

    @Override
    public StateRegistry<S, T> getCurrentStateRegistry() {
        if (getState() == null) {
            return null;
        }
        final StateRegistry<S, T> stateRegistry = registry.getStateRegistry(getState());
        return stateRegistry == null ? new StateRegistry<S, T>(getState()) : stateRegistry;
    }

    @Override
    public StateRegistry<S, T> getCurrentStateRegistryNotNull() {
        if (getState() == null) {
            throw new IllegalStateException("State machine is exited, and thus has no state anymore.");
        }
        final StateRegistry<S, T> stateRegistry = registry.getStateRegistry(getState());
        return stateRegistry == null ? new StateRegistry<S, T>(getState()) : stateRegistry;
    }

    @Override
    public List<T> getPermittedTriggers(Object... args) {
        final StateRegistry<S, T> reg = getCurrentStateRegistry();
        if (reg == null) {
            return Collections.emptyList();
        }
        return getCurrentStateRegistry().getPermittedTriggers(stateMachineContext, args);
    }

    @Override
    public IStateMachineContext<S, T> getStateMachineContext() {
        return stateMachineContext;
    }

    /**
     * {@inheritDoc}
     *
     * @author rinke
     */
    @Override
    public StateMachineStatus<S> getStateMachineStatus() {
        if (isExited()) {
            return null;
        }
        if (cachedStatus == null) {
            return createAndCacheStatus();
        }
        // there is a cachedStatus, but it is only usable if the state hasn't changed.
        if (unChanged()) {
            return cachedStatus;
        }
        // recache
        return createAndCacheStatus();
    }

    /**
     * initializes the state machine to its initial state.
     *
     * @author rinke
     */
    public void initializeToFirstState() {
        if (initialized) {
            throw new IllegalStateException("You tried to initialize a state machine which is already initialized.");
        }
        final S initialState = registry.retrieveInitialState();
        stateMutator.accept(initialState);
        if (stateMachineContext.getStateMachine() == null) {
            stateMachineContext.setStateMachine(this);
        }

        if (registry.isEntryActionOfInitialStateEnabled()) {
            final Transition<S, T> initialTransition = new Transition<>(initialState, initialState, null, null);
            getCurrentStateRegistryNotNull().initEnter(stateMachineContext, initialTransition);
        }
        initializeChildStateMachines(null);
        final S oldCachedState = cachedState;
        cachedState = stateAccessor.call();
        // if initializing to first state involves a raise to another state, this goes
        // wrong with the caching. If we explicitly reset the status to null again, it will
        // be reread, and everything will be handled correctly.
        if (oldCachedState != cachedState && cachedStatus != null) {
            cachedStatus = null;
        }
        initialized = true;
    }

    /**
     * {@inheritDoc}
     *
     * @author rinke
     */
    @Override
    public boolean isExited() {
        return (getState() == null);
    }

    /**
     * {@inheritDoc}
     *
     * @author rinke
     *
     */
    @Override
    public void onFailedConditionTrigger(final Action<S, T> _failedConditionTriggerAction) {
        failedConditionTriggerAction = _failedConditionTriggerAction;
    }

    @Override
    public void onUnhandledTrigger(final Action<S, T> _unhandledTriggerAction) {
        unhandledTriggerAction = _unhandledTriggerAction;
    }

    @Override
    public MachineRegistry<S, T> registry() {
        return registry;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("StateMachine:");
        sb.append(hashCode());
        sb.append(" in ").append(getStateMachineStatus().toString());
        return sb.toString();
    }

    /**
     * {@inheritDoc}
     *
     * @author rinke
     */
    @Override
    public boolean unChanged() {
        if (cachedState == null || cachedState != stateAccessor.call()) {
            cachedState = null;
            return false;
        }
        final boolean result = true;
        for (int i = 0, iSize = parallelStateMachines.size(); i < iSize; i++) {
            final StateMachine<S, T> childStateMachine = parallelStateMachines.get(i);
            if (!childStateMachine.unChanged()) {
                return false;
            }
            // cachedStatus is definitely NOT null here, so we can check it
            // it might be that something inside a parallel was not updated to the upper cachedStatus.
            // this check should find that out.
            if (cachedStatus != null && childStateMachine.cachedState != null
                    && !cachedStatus.getStates().contains(childStateMachine.cachedState)) {
                return false;
            }
        }
        return result;
    }

    /**
     * gives the nominal state where the state machine is in. This should always return a leaf state.
     */
    protected S getState() {
        return stateAccessor.call();
    }

    /**
     * initializes the child stateMachines which are all part of a parallel state.
     *
     * @author Ejnar (original)
     * @author rinke (heavily rewritten)
     */
    protected void initializeChildStateMachines(DestinationRecord destinationRecord, Object... args) {
        final StateRegistry<S, T> currentStateRegistry = getCurrentStateRegistry();
        if ((currentStateRegistry != null) && (currentStateRegistry.isParallel())) {
            List<ParallelRegistry<S, T>> parallelRegs = getCurrentStateRegistry().getParallelRegistries();
            for (int i = 0, iSize = parallelRegs.size(); i < iSize; i++) {
                final ParallelRegistry<S, T> parallelReg = parallelRegs.get(i);
                final ParallelRegistry<S, T> targetedReg = (destinationRecord == null)
                        ? null
                        : (ParallelRegistry<S, T>) destinationRecord.targetRegistry;
                StateMachine<S, T> newMachine = null;
                final T trigger = (destinationRecord == null) ? null : destinationRecord.targetStateMachineTrigger;
                if (targetedReg != null) {
                    // we have 1 explicit targeted child state machine,
                    if (parallelReg.equals(targetedReg)) {
                        // that one uses the trigger explicitly
                        newMachine = createParallelStateMachine(parallelReg, trigger, args);
                    } else {
                        // and all the others explicitly use NOT the trigger.
                        newMachine = createParallelStateMachine(parallelReg, null);
                    }
                } else {
                    // no explicitly targeted child state machine, but the same trigger may be available
                    // in any child state machine... We can just pass the trigger. if it is null,
                    // it is the same as not passing, and if it is passed to the wrong child state machine
                    // it will not find the behaviour and fall back to normal initialization.
                    newMachine = createParallelStateMachine(parallelReg, trigger, args);
                }
                parallelStateMachines.add(newMachine);
                // only now, because if it is not yet in the parallelStateMachines list, then we will miss implicit triggers used to initialize
                ((ParallelStateMachine<S, T>) newMachine).initializeToFirstState(trigger, args);
            }
            cachedState = null;
            cachedStatus = null;
        }
    }

    /**
     * retrieves and configures the destination for a trigger.
     * <p>
     * The present version of stateless4j uses config-time destination resolving, which means that a
     * state must be configured first before it can be used as a transition destination. This means that
     * all involved states are frozen in their position in the state tree as soon as a trigger behaviour
     * is defined on them. In this way, the destination of a triggerbehaviour can be resolved at config-time,
     * which is much more efficient than resolving it at runtime.<br>
     * Because of config-time destination resolving this method could be fairly simple: trigger behaviours
     * configured that way will always find their destination immediately, without having to look everywhere.
     * However, since stateless4j also allows the use of Dynamic triggers, which are by definition only
     * resolvable at runtime, we still must define code which does not know beforehand where to find
     * the destination. This makes this method rather complex.
     *
     * <p>
     * The following procedure applies:
     *
     * <ol>
     * <li>take the destination of the triggerBehaviour.
     * <li>if destination is null, return.
     * <li>find the stateRegistry belonging to the declared destination.
     * <li>if the stateRegistry is found: if the destination state is not a leaf state or
     * a parallel state, go down the tree via initial states until a leaf state or a parallel state
     * is found. This is the new destination.
     * <li>if the stateRegistry is NOT found: check in the list of all associated states (including
     * parent and child state machines contained states) if the state is known there.
     * <li>if the stateRegistry is NOT found at all: throw exception.
     * <li>if the stateRegistry is found in a child or parent state machine: look up the child state machine,
     * and try and find the destination there.
     * <li>if destination not found in child state machine, check if there exists a parent state machine.
     * <li>if no parent state machine exists, throw not found exception
     * <li>if parent state machine exists, call method recursively on parent state machine, and exit.
     * <li>if destination found in child state machine: look up triggerBehaviour in there.
     * If not found, create a new one. If found, use it.
     * If not found, create a new one and add it as triggerBehaviour to the child state machine.
     * In either case, set the destination to the containing parallel state.
     * </ol>
     *
     *
     * @return a DestinationRecord. The destination field is always set; If any child state machine is
     * involved, also the two other fields are set. returns null if the TriggerBehaviour does not result in
     * a transtion, for example because the guard condition is not met.
     *
     * @author rinke
     */
    protected DestinationRecord resolveDestination(TriggerBehaviour<S, T> triggerBehaviour, final Object... args) {
        final DestinationRecord result = new DestinationRecord();
        if (triggerBehaviour instanceof ExitingTriggerBehaviour) {
            setExitTarget(result);
            // exiting triggerBehaviour, already correctly configured, just exit.
            return result;
        }
        final S source = getState();
        final OutVar<S> destination = new OutVar<>();
        final T trigger = triggerBehaviour.getTrigger();
        if (triggerBehaviour.resultsInTransitionFrom(stateMachineContext, source, args, destination)) {
            if (destination.get() != null) {
                StateRegistry<S, T> destinationStateReg = registry.getStateRegistry(destination.get());
                // the destination may not have been found on the present config
                if (destinationStateReg != null) {
                    // go into initial states until a leaf state is found,.
                    while (!destinationStateReg.isLeafState() && !destinationStateReg.hasParallelState()) {
                        destinationStateReg = destinationStateReg.getInitialState();
                    }
                    if (destinationStateReg.getUnderlyingState() != destination.get()) {
                        // changed, reset OutVar
                        destination.set(destinationStateReg.getUnderlyingState());
                    }
                    result.destination = destination.get();
                    // OK, destination was this state, but it might be a parallel state
                    if (destinationStateReg.hasParallelState()) {
                        // the destination is a parallel state. It might be that the transition has to
                        // be passed on in a submachine, but we don't know which. So we set the trigger
                        // to the record, but not the sub machine.
                        result.targetStateMachineTrigger = trigger;
                    }
                    return result;
                }

                if (registry.getAllStates().contains(destination.get())) {
                    // destination not found on present config. but it is found in some child state machine (or parent).
                    // so direct the destination to the state containing this state machine
                    final Map.Entry<S, ParallelRegistry<S, T>> entry = registry
                            .getParallelContainingStateEntry(destination.get());
                    if (entry == null) {
                        // not found in parallel child state machine. Look in parent as our last hope
                        return resolveDestinationInParent(destination, triggerBehaviour, args);
                    }
                    // continueing with entry not null: matching child state machine found
                    // Set the destination for the hosting state machine to the parallel state
                    result.destination = entry.getKey();
                    result.targetRegistry = entry.getValue();
                    result.targetStateMachineTrigger = trigger;
                    // is there an existing incoming triggerBehaviour for the child state machine that matches?
                    TriggerBehaviour<S, T> tg = entry.getValue().tryFindHandler(stateMachineContext, trigger, args);
                    if (tg == null) {
                        // no known ingoing triggerBehaviour for the child state machine. Create one.
                        final Func<Boolean, S, T> guard = (c, a) -> Boolean.TRUE;
                        Action<S, T> action = (c, t, p, a) -> {
                        };
                        if (triggerBehaviour instanceof TransitioningTriggerBehaviour<S, T> transitioningTriggerBehaviour) {
                            action = transitioningTriggerBehaviour.getAction();
                        }
                        tg = new TransitioningTriggerBehaviour<>(trigger, destination.get(), guard, action, null);
                        entry.getValue().addTriggerBehaviour(tg);
                    }
                    return result;
                }
                // no destination found
                throw new DestinationNotFoundException(destination.get());
            }
            // distination is null
            throw new DestinationNotFoundException("no destination specified in target");
        } // if triggerBehaviour.resultsIN....
        return null;
    }

    /**
     * as {@link #resolveDestination(TriggerBehaviour, Object...)}, but looks in the parent state machine.
     */
    protected StateMachine<S, T>.DestinationRecord resolveDestinationInParent(OutVar<S> destination,
            TriggerBehaviour<S, T> triggerBehaviour, Object... args) {
        // no parent state machine, so not found anywhere...
        throw new DestinationNotFoundException(destination.get());
    }

    /**
     * sets the target on the argument in case of an ExitingTriggerBehaviour
     */
    protected void setExitTarget(StateMachine<S, T>.DestinationRecord result) {
        // as this is the top level state machine, there is no target when exiting, so does nothing.
    }

    /**
     * gets the now active parallel state machines.
     * @author Ejnar
     */
    Collection<StateMachine<S, T>> getParallelStateMachines() {
        return parallelStateMachines;
    }

    /**
     * Checks if the trigger is aimed at self, which means that the fired trigger would result
     * in no different status of the statemachine then the statemachine already is in at present.
     * Usually, the configurers don't allow this kind of triggers, but this can not always be determined
     * at designtime - for example in case of Dynamic Triggers.
     * So this method checks on self targeted triggers at runtime.
     * <p>
     * There are 3 possible options for targeting self:
     * <ul>
     * <li>state to self: a state's trigger is literally targeted at itself
     * <li>parent to child: a parent state's trigger is targeted at a child state of the parent state,
     * while the state machine is already in that child state.
     * <li>child to parent: a substate's trigger is targeted at the parent state, AND if (and only if)
     * the present substate the state machine is in, is on the initial path of the parent state, so
     * that going to the parent would automatically lead to ending up in the child state (in which it
     * already is).
     * </ul>
     *
     * In all these three cases, the FireResult is set to {@link FireResult#IGNORED}. In any other case,
     * the method returns {@link FireResult#UNHANDLED}.
     *
     * @return true if a self-transition, false otherwise.
     */
    private boolean checkForSelf(final StateRegistry<S, T> current, final S destination, final FireResult fireResult) {
        final StateRegistry<S, T> destinationStateReg = registry.getStateRegistry(destination);
        // this will find literal self transtions, but also parent to child and child to parent, because
        // current and destination will always be pointing at leaf states. So this is the only check
        // we need
        if (current.getUnderlyingState().equals(destinationStateReg.getUnderlyingState())) {
            if (fireResult == FireResult.PARTIAL) {
                // impossible, child state machine is partial, but turns out to be self
                throw new IllegalDynamicDestinationException();
            }
            // we have a true hit here...
            if (LogSwitch.LOG) {
                if (logger.isDebugEnabled()) {
                    MachineRegistry.agentIdToMDC(stateMachineContext);
                    logger.debug("Ignored transition to {} because was already in this state.", destination.toString());
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Creates a {@link StateMachineStatus} and caches it.
     *
     * @author rinke
     */
    private StateMachineStatus<S> createAndCacheStatus() {
        final StateMachineStatus<S> result = new StateMachineStatus<>(this);
        cachedStatus = result;
        cachedState = stateAccessor.call();
        return result;
    }

    /**
     * creates a state machine instantiation for a state in a parallel tag. It always creates them in the
     * current StateRegistry
     * @author Ejnar
     * @author rinke (trigger acceptance)
     */
    private StateMachine<S, T> createParallelStateMachine(ParallelRegistry<S, T> parallelRegistry,
            T trigger, Object... args) {
        StateMachine<S, T> stateMachine = null;
        if (trigger != null) {
            stateMachine = new ParallelStateMachine<>(parallelRegistry, getStateMachineContext(), this, trigger, args);
        } else {
            stateMachine = new ParallelStateMachine<>(parallelRegistry, getStateMachineContext(), this);
        }
        return stateMachine;
    }

    /**
     * executes the failedConditionTriggerAction.
     * @author rinke
     */
    private void doFailedConditionTriggerAction(T trigger) {
        Action<S, T> action = failedConditionTriggerAction;
        if (failedConditionTriggerAction == null) {
            action = registry.getFailedConditionTriggerAction();
        }
        final S state = getCurrentStateRegistryNotNull().getUnderlyingState();
        final Transition<S, T> transition = new Transition<>(state, null, trigger, null);
        action.doIt(stateMachineContext, transition, null);
    }

    /**
     * gets the unhandledTriggerAction. If the local field is null, gets the equally named field on the registry object.
     * @return
     */
    private Action<S, T> getUnhandledTriggerAction() {
        if (unhandledTriggerAction == null) {
            return registry.getUnhandledTriggerAction();
        }
        return unhandledTriggerAction;
    }

    private void killSubStateMachines(final T trigger, final Object... args) {
        for (int i = 0, iSize = parallelStateMachines.size(); i < iSize; i++) {
            final StateMachine<S, T> childSM = parallelStateMachines.get(i);
            childSM.exit(trigger, args);
        }
        parallelStateMachines.clear();
    }

    /**
     * handles the firing of a trigger:
     * <ul>
     * <li>looks up the trigger in the configuration
     * <li>determines the destination
     * <li>exits the present state
     * <li>performs associated actions
     * <li>enters the new state
     * <li>handles parallel state machines if involved.
     * </ul>
     *
     * Or, in detail:
     * <ol>
     * <li>First checks if current state is a parallel state with direct child states under it in their own parallel
     * state machine. If this is the case:
     * <ol>
     * <li>Call method <code>privateFire</code> for each nested state machine.
     * <li>If the nested state machine is exited, removes it from the list of parallel state machines.
     * </ol>
     * <li>looks up a triggerbehaviour belonging to the trigger argument, in the current or parent state machine.
     * If a triggerbehaviour is found, but its condition fails, a FailedConditionTriggerBehaviour is passed.
     * <li>In case of a FailedConditionTriggerBehaviour: Does the action belonging to a FailedCondition, and
     * then exits the method by returning true for handled.
     * <li>resolves the destination of the triggerbehaviour, see {@link #resolveDestination(TriggerBehaviour, Object...)}.
     * <li>if the destination is in some parent, the state machine is exited, and the method exits returning
     * a true for handled.
     * <li>current state is exited
     * <li>Action belonging to the found triggerBehaviour is executed
     * <li>destination state is entered
     * <li>if necessary, child state machines are created and entered.
     * </ol>
     *
     * @author rinke
     */
    private FireResult privateFire(final T trigger, final Object... args) {
        FireResult handled = FireResult.UNHANDLED;

        final StateRegistry<S, T> current = getCurrentStateRegistryNotNull();
        // delegate to child state machines.
        // the correct order is: first handle deepest child state machines, then handle sm's higher in the line.
        handled = privateFireChildren(current, trigger, args);
        /*
         * We have 5 possibilities for handled now, after all children have run privateFire:
         * * UNHANDLED: -> just continue one level higher
         * * FAILED: -> just continue, if nothing else is found, FAILED will not be overwritten, and handled by caller method;
         * * PARTIAL: -> tryFindHandler will produce the correct ResurfacingTriggerBehaviour. If not: IllegalStateException at the end.
         * * HANDLED: -> next lines will stop and exit immediately.
         * * IGNORED: -> next lines will stop and exit immediately.
         */
        if (handled == FireResult.HANDLED || handled == FireResult.IGNORED) {
            return handled;
        }

        boolean mustExitCurrent = false;
        if (handled == FireResult.PARTIAL && registry.isParallelState(getState())) {
            if (parallelStateMachines.isEmpty()) {
                // no child state machines alive anymore, while there should be. so it was an exit transition
                mustExitCurrent = true;
            }
        }

        // if the triggerconfiguration uses custom parameters, validate them.
        final ParameterizedTrigger<S, T> configuration = registry.getTriggerConfiguration(trigger);
        if (configuration != null) {
            configuration.validateParameters(args);
        }

        // find handler in current or super state machines. Logically, don't look in subs, as present state is always leaf state.
        final Integer resurfacingId = (handled == FireResult.PARTIAL) ? exitCode : null;
        final TriggerBehaviour<S, T> triggerBehaviour = current.tryFindHandler(stateMachineContext, trigger,
                resurfacingId, args);

        // if the condition of the correct trigger was not met, do the appropriate action and exit method.
        if (triggerBehaviour instanceof FailedConditionTriggerBehaviour) {
            return FireResult.FAILED;
        }
        if (triggerBehaviour instanceof IgnoredTriggerBehaviour) {
            triggerBehaviour.performAction(stateMachineContext, null, args);
            return FireResult.IGNORED;
        }

        // found a triggerBehaviour...
        if (triggerBehaviour != null) {

            if (triggerBehaviour instanceof ReentryTriggerBehaviour<S, T> reentryTB) {
                final S declaredReentryState = reentryTB.getDestination();
                final Transition<S, T> transition = new Transition<>(declaredReentryState,
                        declaredReentryState, trigger, triggerBehaviour.getDeclaredInState());
                if (declaredReentryState.equals(getState())) {
                    // simple: just exit and enter again
                    getCurrentStateRegistry().exit(stateMachineContext, transition, args);
                    triggerBehaviour.performAction(stateMachineContext, transition, args);
                    getCurrentStateRegistry().enter(stateMachineContext, transition, args);
                } else {
                    // exit from current until we meet declaredReentryState as parent.
                    StateRegistry<S, T> tempRegistry = registry.getStateRegistry(getState());
                    boolean firstLoop = true;
                    do {
                        if (!firstLoop) {
                            tempRegistry = tempRegistry.getDirectSuperstate();
                        }
                        firstLoop = false;
                        tempRegistry.exit(stateMachineContext, transition, args);
                    } while (!tempRegistry.getUnderlyingState().equals(declaredReentryState));
                    triggerBehaviour.performAction(stateMachineContext, transition, args);
                    // enter again, starting with last
                    while (!tempRegistry.isLeafState() && !tempRegistry.hasParallelState()) {
                        tempRegistry.enter(stateMachineContext, transition, args);
                        tempRegistry = tempRegistry.getInitialState();
                    }
                    setState(tempRegistry.getUnderlyingState());
                    tempRegistry.enter(stateMachineContext, transition, args);
                }
                return FireResult.HANDLED;
            }

            if (triggerBehaviour.isExiting()) {
                exitCode = triggerBehaviour.getResurfacingId();
                exit(triggerBehaviour.getTrigger(), args);
                return FireResult.PARTIAL;
            }

            final DestinationRecord destinationRecord = resolveDestination(triggerBehaviour, args);
            // if found destination, this block resolves it in case destination is in the same state machine as current.
            if (destinationRecord != null) {
                if (triggerBehaviour instanceof DynamicTriggerBehaviour) {
                    // in case of DynamicTriggerBehaviour: It cannot go over parallel state boundaries
                    if (destinationRecord.targetRegistry != null) {
                        throw new IllegalDynamicDestinationException(trigger);
                    }
                }
                // check if it is a self targeted transition
                if (checkForSelf(current, destinationRecord.destination, handled)) {
                    return FireResult.IGNORED;
                }
                final S destination = destinationRecord.destination;
                final Transition<S, T> transition = new Transition<>(this.getState(), destination,
                        trigger, triggerBehaviour.getDeclaredInState());
                final StateRegistry<S, T> topmostExitState = registry.findTopmostExitState(current, destination);
                // if there are any substatemachines to be killed, do it now.
                killSubStateMachines(trigger, args);
                topmostExitState.exit(stateMachineContext, transition, args);
                triggerBehaviour.performAction(stateMachineContext, transition, args);
                setState(destination);
                getCurrentStateRegistry().enter(stateMachineContext, transition, args);
                initializeChildStateMachines(destinationRecord, args);
                handled = FireResult.HANDLED;
            }
        } // if triggerbehaviour != null

        if (mustExitCurrent) {
            // a child state machine was exited, so the previously current state in this state machine must have been exited too...
            if (current.getUnderlyingState().equals(getCurrentStateRegistry().getUnderlyingState())) {
                // ai, they are still equal...
                throw new IllegalStateException("child state machine was exited, but hosting parent not");
            }
        }

        return handled;
    }

    /**
     * delegates the private fire to all child state machines, and kills them if appropriate.
     *
     * @author rinke
     */
    private FireResult privateFireChildren(final StateRegistry<S, T> current, final T trigger, final Object... args) {
        FireResult handled = FireResult.UNHANDLED;
        if (current.hasParallelState()) {
            // at least there are parallel submachines... search them and try to fire
            boolean killChildStateMachines = false;
            boolean breakLoop = false;
            for (int i = 0, iSize = parallelStateMachines.size(); i < iSize; i++) {
                final StateMachine<S, T> childSM = parallelStateMachines.get(i);
                final FireResult childHandled = childSM.privateFire(trigger, args);
                // combine results of childHandled with handled
                switch (childHandled) {
                    case UNHANDLED:
                        // in all cases do nothing, handled of previous checked childSM is stronger.
                        break;
                    case FAILED:
                        // if previous checked childSM was partial or handled, we wouldn't be here;
                        // in all other cases FAILED is stronger.
                        handled = childHandled;
                        break;
                    case PARTIAL:
                        killChildStateMachines = childSM.isExited();
                        exitCode = childSM.exitCode;
                        childSM.exitCode = null;
                    case HANDLED:
                        // something in the childSM changed, so kill the cache
                        cachedState = null;
                    case IGNORED:
                        handled = childHandled;
                        breakLoop = true;
                }
                if (breakLoop) {
                    // we encountered partial or handled, so no need to check any other childSM's, as
                    // configuring 2 state machine parallel childs with the same trigger is forbidden.
                    break;
                }
            }
            if (killChildStateMachines) {
                // it might be that a trigger is failed in one substatemachine, and exit in another,
                // and check if they should be treated differently, but aren't
                killSubStateMachines(trigger, args);
            }
        }
        return handled;
    }

    private void setState(S value) {
        stateMutator.accept(value);
    }
}
