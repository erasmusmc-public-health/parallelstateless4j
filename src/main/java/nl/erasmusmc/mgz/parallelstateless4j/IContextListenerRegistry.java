/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

/**
 * Registers all IStateMachineContextListeners. These are Listeners registered to the StateMachineContext.
 * When a property changes, the listeners will be notified.
 */
public interface IContextListenerRegistry {

    /**
     * adds a listener for the given property having key <code>key</code>.
     * If the underlying property changes in value, the implementation should
     * notify the listeners.
     *
     * @param key
     * @param listener
     */
    void addListener(String key, IContextPropertyListener listener);

    /**
     * removes the given property listener.
     * @param key
     * @param listener
     */
    void removeListener(String key, IContextPropertyListener listener);

    /**
     * removes all property listeners from the property with the given key.
     *
     * @param key
     */
    void removeListeners(String key);

}
