/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import nl.erasmusmc.mgz.parallelstateless4j.configuration.MachineConfigurer;

/**
 * Reports that a state machine has been exited, and is thus in no state at all.
 * Implementations of this class can define special actions to undertake when this happens.
 * There is a {@link IStateMachineListenerRegistry} class which allows you to register this
 * to the state machine you want to listen to.
 * <p>
 * The implementation logic is as follows: <br>
 * The {@link MachineConfigurer} class implements the registry. Listeners like
 * {@link ExampleExitListener} are registered to this class. When a
 * {@link StateMachine} is constructed, the listeners are passed to the state machine
 * implementation via the <code>MachineConfigurer</code> implementation which is passed as a
 * constructor parameter. This logic will add the listeners to <b>EVERY</b> state machine
 * instance.
 * <p>
 * By using another constructor of <code>StateMachine</code>, you can set different listener
 * implementations to different state machine instances.
 *
 * @author rinke
 *
 */
public interface IStateMachineExitListener<S, T> {

    /**
     * reports the exit out of a StateMachine object.
     *
     * @param stateMachine - the state machine object which has been exited. Note that this may be a subStateMachine in a parallel setting.
     */
    void onExit(IStateMachine<S, T> stateMachine);

}
