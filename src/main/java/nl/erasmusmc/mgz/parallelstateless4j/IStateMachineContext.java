/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.Collection;
import java.util.Map;

import com.google.common.collect.Multimap;

/**
 * This interface describes a state machine context, which can be used by
 * a {@link StateMachine} to store context information. Via a context the underlying
 * object which contains the state machine can be accessed and altered.
 * At present, this context interface supports the creation of properties, and allows
 * to add listeners to these properties.
 *
 * @param <S> The type used to represent the states of the contexted
 * state machine that this context belongs to
 * @param <T> The type used to represent the triggers that cause state
 * transitions of the contexted state machine that this context belongs to
 */
public interface IStateMachineContext<S, T> extends IStateMachineHolder<S, T>, IContextListenerRegistry {

    /**
     * returns the context as a plain Map.
     */
    Map<String, Object> asMap();

    /**
     * Changes an attribute which has a Collection as value.
     *
     * @param <E> The type of the elements of the Collection.
     * @param key the key under which the collection is stored as property/attribute.
     * @param additions A collection containing all elements which are to be added to the collection attribute.
     * @param removals A collection containing all elements which are to be removed from the collection attribute.
     * @throws ClassCastException in case the attribute's value is not a collection.
     */
    <E> void changeCollectionAttribute(String key, Collection<E> additions, Collection<E> removals) throws ClassCastException;

    /**
     * Clears an attribute which has a Collection as value. The result of this action is that the value of
     * the attribute is now an empty collection (which does allow addition of new elements).
     * Note that this default implementation doesn't handle the listeners.
     *
     * @param key
     * @return true if the attribute was cleared; false if it was not found.
     * @throws ClassCastException
     */
    @SuppressWarnings("rawtypes")
    default boolean clearCollectionAttribute(String key) {
        Object rawValue = getAttribute(key);
        if (rawValue == null) {
            return false;
        }
        if (!(rawValue instanceof Collection)) {
            throw new ClassCastException("Attribute" + key + " on state machine context is NOT of a collection type");
        }
        ((Collection) rawValue).clear();
        return true;
    }

    /**
     * Get a specific attribute from the context attribute map, using the specified key.
     *
     * @param key the key of the specified attribute
     * @return the value, if any, of the specified key. Null if not found.
     */
    Object getAttribute(String key);

    /**
     * Get a specific attribute from the context attribute map, using the specified key. If
     * the key has no value it is initialized with the given initial value. Note that the implementation
     * must take care that the initial value is saved.
     *
     * @param key the key of the specified attribute
     * @param initialValue the initial value if key is absent
     * @return the value of the specified key, null if initialValue is null and key was absent.
     */
    Object getAttribute(String key, Object initialValue);

    /**
     * Removes the attribute with the specified key from the context attribute map.
     * @param key the name of the key to use for the attribute
     * @return the Object which was previously associated as value to the key, and which was removed from the map. Returns null if
     * the value belonging to the key was null, or if the key was not found.
     */
    Object removeAttribute(String key);

    /**
     * Set an attribute on the context attribute map, using the specified key.
     *
     * @param key the name of the key to use for the attribute
     * @param value the value of the attribute to add
     */
    void setAttribute(String key, Object value);

    /**
     * sets all ContextPropertyListeners in one go.
     *
     * @param contextPropertyListeners
     */
    void setListeners(Multimap<String, IContextPropertyListener> contextPropertyListeners);

    /**
     * Set the {@link StateMachine} that this context belongs to.
     *
     * @param stateMachine The state machine to set
     */
    void setStateMachine(IStateMachine<S, T> stateMachine);

}
