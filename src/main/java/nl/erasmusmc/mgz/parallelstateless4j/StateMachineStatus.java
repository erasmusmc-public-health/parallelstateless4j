/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import nl.erasmusmc.mgz.parallelstateless4j.configuration.StateRegistry;

/**
 * A wrapper object around the actual states a {@link StateMachine} is currently in.
 * Contains the actual leaf states, plus all the above super states belonging to the leaf states.
 * If one of the parent states is a parallel state, then multiple leaf states will be present.
 *
 * @param <S> Type of state.
 *
 * @author rinke
 */
public class StateMachineStatus<S> implements IStateMachineStatus<S> {

    private Set<S> states     = new HashSet<>();
    private Set<S> leafStates = new HashSet<>();

    <T> StateMachineStatus(StateMachine<S, T> currentStateMachine) {
        addStateMachine(currentStateMachine);
    }

    @Override
    public Set<S> getLeafStates() {
        return Collections.unmodifiableSet(leafStates);
    }

    @Override
    public Set<S> getLeafStatesUnsafe() {
        return leafStates;
    }

    @Override
    public Set<S> getStates() {
        return Collections.unmodifiableSet(states);
    }

    @Override
    public Set<S> getStatesUnsafe() {
        return states;
    }

    @Override
    public boolean isInState(S state) {
        return states.contains(state);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Iterator<S> it = leafStates.iterator();
        for (int i = 0, iSize = leafStates.size(); i < iSize; i++) {
            S state = it.next();
            sb.append(state.toString()).append(",");
        }
        return sb.toString();
    }

    /**
     * if this currentStateMachine argument has direct parallel substates, those are processed
     * via this method.
     */
    private <T> void addParallels(StateMachine<S, T> currentStateMachine) {
        StateRegistry<S, T> stateRegistry = currentStateMachine.getCurrentStateRegistryNotNull();
        if (stateRegistry.hasParallelState()) {
            Collection<StateMachine<S, T>> parallelStateMachines = currentStateMachine.getParallelStateMachines();
            Iterator<StateMachine<S, T>> it = parallelStateMachines.iterator();
            for (int i = 0, iSize = parallelStateMachines.size(); i < iSize; i++) {
                StateMachine<S, T> subStateMachine = it.next();
                if (!subStateMachine.isExited()) {
                    addStateMachine(subStateMachine);
                }
            }
        }
    }

    /**
     * adds all the current states according to the given state machine.
     * The underlying state of the current representation is added, plus
     * the same procedure is repeated for all substates would the current state be a parallel state.
     * All superStates are added too, but only for as far not already added. As soon as,
     * while going up, the <code>alreadyAdded</code> stateRegistry is reached, iteration stops.
     *
     */
    private <T> void addStateMachine(StateMachine<S, T> currentStateMachine) {
        StateRegistry<S, T> stateRegistry = currentStateMachine.getCurrentStateRegistryNotNull();
        addUnderlying(stateRegistry);

        addParallels(currentStateMachine);

        // add all supers
        StateRegistry<S, T> temp = stateRegistry;
        while (temp.getDirectSuperstate() != null) {
            temp = temp.getDirectSuperstate();
            states.add(temp.getUnderlyingState());
            // if the super state is parallel, we must add all child state machines other than
            // the state machine of the present state.
            if (temp.hasParallelState()) {
                ParallelStateMachine<S, T> casted = (ParallelStateMachine<S, T>) currentStateMachine;
                StateMachine<S, T> parentStateMachine = casted.getParentStateMachine();
                // add all child state machines of the parent state machine...
                Collection<StateMachine<S, T>> parallelParentMachines = parentStateMachine.getParallelStateMachines();
                Iterator<StateMachine<S, T>> it = parallelParentMachines.iterator();
                for (int i = 0, iSize = parallelParentMachines.size(); i < iSize; i++) {
                    StateMachine<S, T> childOfParent = it.next();
                    if (childOfParent.equals(currentStateMachine)) {
                        // already added this one, pass...
                        continue;
                    }
                    addStateMachine(childOfParent);
                }
            }
        }
    }

    /**
     * adds just the underlying state of this stateRegistry
     */
    private <T> void addUnderlying(StateRegistry<S, T> stateRegistry) {
        S state = stateRegistry.getUnderlyingState();
        states.add(state);
        if (stateRegistry.isLeafState()) {
            leafStates.add(state);
        }
    }

}
