/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Multimap;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.LogSwitch;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.FailedConditionTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ParameterizedTrigger;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ResurfacingTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.Transition;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TriggerBehaviour;

/**
 * This class stores all information of a configured state, specifically aimed at runtime use for a StateMachine.
 * <p>
 * Contains the following information on the state:
 * <ul>
 * <li>S: state: they underlying state.
 * <li>triggerBehaviours: all associated triggers and their behaviour
 * <li>entry and exit actions
 * <li>substates and superstate
 * <li>initial state
 * <li>parallelisme info
 * </ul>
 *
 * @param <S> - the type of a state
 * @param <T> - the type of a trigger
 */
public class StateRegistry<S, T> {

    private static final Logger                   logger                       = LoggerFactory
            .getLogger(StateRegistry.class);

    private final S                               state;
    private final TriggerBehaviourContainer<S, T> triggerBehaviours            = new TriggerBehaviourContainer<>();
    /**
     * contains only {@link ResurfacingTriggerBehaviour}s.
     */
    private final TriggerBehaviourContainer<S, T> resurfacingTriggerBehaviours = new TriggerBehaviourContainer<>();
    private final List<Action<S, T>>              entryActions                 = new ArrayList<>();
    private final List<Action<S, T>>              exitActions                  = new ArrayList<>();
    /**
     * substates within the own config, so no substates which are children of a parallel state, and thus hold themselves a nested state machine
     */
    private final List<StateRegistry<S, T>>       substates                    = new ArrayList<>();
    /**
     * the superstate; is null if it is the top level state IN ITS OWN config. So this doesn't look over the borders of nested state machines.
     */
    private StateRegistry<S, T>                   superstate;
    /**
     * the hosting state in a parent state machine, if that is available.
     */
    private StateRegistry<S, T>                   parentStateMachineSuperState;
    /**
     * the initial state, so the first substate to be entered when no explicit substate is specified. Is null in
     * case the underlying state is configured as parallel.
     */
    private StateRegistry<S, T>                   initialState;
    private final List<ParallelRegistry<S, T>>    parallelRegistries           = new ArrayList<>();
    private boolean                               frozen;

    /**
     * true if the underlying state is configured as parallel, so if it has sub-statemachines.
     */
    private final boolean                         parallel;

    public StateRegistry(S state) {
        this.state = state;
        parallel = false;
    }

    StateRegistry(S state, boolean asParallel) {
        this.state = state;
        parallel = asParallel;
    }

    /**
     * returns true if this state can handle the trigger. False if it doesn't know the trigger.
     */
    @SuppressWarnings("boxing")
    public Boolean canHandle(IStateMachineContext<S, T> context, T trigger, Object... args) {
        return tryFindHandler(context, trigger, null, args) != null;
    }

    /**
     * executes this method when the state is entered as a result of a transition. It performs a few
     * checks, and then executes entry actions.<br>
     * If the present stateRegistry or one of its substates is the source of the transition,
     * then nothing happens.
     * <p>
     * <code>enter</code> also takes care of <code>enter</code>s of superstates,
     * but only in its own state machine, not in parent state machines.
     *
     * @author rinke (rewritten)
     */
    public void enter(IStateMachineContext<S, T> context, Transition<S, T> transition, Object... entryArgs) {
        requireNonNull(transition, "transition is null");

        if (transition.isReentry()) {
            executeEntryActions(context, transition, entryArgs);
        } else if (!includes(transition.getSource())) {
            if (superstate != null) {
                superstate.enter(context, transition, entryArgs);
            }
            executeEntryActions(context, transition, entryArgs);
        }
    }

    /**
     * executes this method if the state is exited as a result of a transition.
     * <p>
     * <code>exit</code> also takes care of <code>exit</code>s of substates,
     * but only in its own state machine, not in other state machines.
     *
     * @author rinke (rewritten)
     */
    public void exit(IStateMachineContext<S, T> context, Transition<S, T> transition, Object... exitArgs) {
        requireNonNull(transition, "transition is null");

        if (context.getStateMachine().getStateMachineStatus().isInState(state)) {
            // only do something if actually in the underlying state.
            if (transition.isReentry() || (!includes(transition.getDestination()))) {
                // if this state is exited, all substates must be exited too, because the substates cannot
                // be active when the parent is not active anymore.
                for (int i = 0, iSize = substates.size(); i < iSize; i++) {
                    final StateRegistry<S, T> subState = substates.get(i);
                    // subState.exit itself checks if the SM is actually in the state.
                    subState.exit(context, transition, exitArgs);
                }
                executeExitActions(context, transition, exitArgs);
            }
        }

    }

    /**
     * exits all the states in the tree of the actual state machine, from deepest nested child state to toplevel
     * parent state. To be called when killing a state machine. Assumes that it starts from the deepest level
     * leaf state, so doesn't look further down.
     *
     * @author rinke
     */
    public void exitTree(IStateMachineContext<S, T> context, Transition<S, T> transition, Object[] args) {
        executeExitActions(context, transition, args);
        if (superstate != null) {
            superstate.exitTree(context, transition, args);
        }
    }

    /**
     * gets all related states of the underlying state.
     * See {@link #isRelatedState(Object)}. So in practice this gets
     * a list with all parents of the <code>state</code>, and all possible children, even
     * looking over parallel boundaries in the tree.
     */
    public Collection<S> getAllRelatedStates() {
        final Collection<S> result = new HashSet<>();
        Collection<StateRegistry<S, T>> leafChildStates = getLeafChildStates();
        Iterator<StateRegistry<S, T>> it = leafChildStates.iterator();
        for (int i = 0, iSize = leafChildStates.size(); i < iSize; i++) {
            StateRegistry<S, T> atomic = it.next();
            do {
                result.add(atomic.getUnderlyingState());
                atomic = atomic.getSuperstateIncludingParentStateMachine();
            } while (atomic != null);
        }
        return result;
    }

    /**
     * gets all direct substates of the present stateRegistry. It doesn't
     * get nested substates (substates more than one level deeper), nor does
     * it get parallel children states.
     */
    public List<StateRegistry<S, T>> getDirectSubstates() {
        return substates;
    }

    /**
     * gets the direct parent of the present stateRegistry.
     * Doesn't get parents of the direct parent state.
     * Only looks in the present state machine, not into any
     * parent state machine.
     */
    public StateRegistry<S, T> getDirectSuperstate() {
        return superstate;
    }

    /**
     * gets the nominal initial state of this state. The initial state is the substate in which the
     * state machine automatically moves, in case no explicit substate was mentioned as target.
     * It usually is the first substate which is configured, though any state can be explicitly
     * set as initial state.
     *
     * @author rinke
     */
    public StateRegistry<S, T> getInitialState() {
        return initialState;
    }

    /**
     * gets all Leaf (child) State Registries of the underlying state.
     * This means:
     * <ul>
     * <li>if the underlying state is a leaf state, returns the state itself.
     * <li>if the underlying state is <i>not</i> a leaf state, returns a Collection
     * with leaf child states (internally repeating the procedure for each child state which is not leaf).
     * </ul>
     * The method also finds leaf states in nested parallel states.
     *
     * @param state
     * @return a Collection with all leaf child state registries, no matter how deeply nested.
     */
    // tested
    public Collection<StateRegistry<S, T>> getLeafChildStates() {
        if (isLeafState()) {
            return Collections.singleton(this);
        }
        final Collection<StateRegistry<S, T>> result = new HashSet<>();
        if (isParallel()) {
            List<ParallelRegistry<S, T>> parallelRegs = getParallelRegistries();
            for (int i = 0, iSize = parallelRegs.size(); i < iSize; i++) {
                final ParallelRegistry<S, T> parallelReg = parallelRegs.get(i);
                result.addAll(parallelReg.getInitialStateRegistry().getLeafChildStates());
            }
        } else {
            // no leaf state itself; loop children
            List<StateRegistry<S, T>> directSubstates = getDirectSubstates();
            for (int i = 0, iSize = directSubstates.size(); i < iSize; i++) {
                final StateRegistry<S, T> childReg = directSubstates.get(i);
                result.addAll(childReg.getLeafChildStates());
            }
        }
        return result;
    }

    /**
     * gets the parallel state sub-registries of the current presentation. Only gets
     * direct children if the current state is a parallel state; if the parallel states are nested
     * on a deeper level, these will not be returned by this method.
     * @return
     * @author rinke
     */
    public List<ParallelRegistry<S, T>> getParallelRegistries() {
        return parallelRegistries;
    }

    /**
     * gets all the triggers that this state can react to, at present. It also checks the
     * conditions: if a condition of a trigger is not met, it is not included in the list.
     * It also checks parent states in parent state machines for this.<br>
     * Doesn't consider ResurfacingTriggerBehaviours.
     *
     */
    public List<T> getPermittedTriggers(IStateMachineContext<S, T> context, Object... args) {
        final Set<T> result = new HashSet<>();
        Set<T> keys = triggerBehaviours.keySet();
        Iterator<T> it = keys.iterator();
        for (int i = 0, iSize = keys.size(); i < iSize; i++) {
            final T t = it.next();
            Collection<TriggerBehaviour<S, T>> tempColl = triggerBehaviours.get(t);
            Iterator<TriggerBehaviour<S, T>> it2 = tempColl.iterator();
            for (int j = 0, jSize = tempColl.size(); j < jSize; j++) {
                final TriggerBehaviour<S, T> v = it2.next();
                if (v.isGuardConditionMet(context, args)) {
                    result.add(t);
                    break;
                }
            }
        }

        if (getDirectSuperstate() != null) {
            result.addAll(getDirectSuperstate().getPermittedTriggers(context, args));
        }
        if (parentStateMachineSuperState != null) {
            result.addAll(parentStateMachineSuperState.getPermittedTriggers(context, args));
        }

        return new ArrayList<>(result);
    }

    /**
     * gets the direct parent of the present stateRegistry. If this
     * Registry is a toplevelstate in a state machine which is nested
     * in another state machine, it returns the parallel state in the
     * parentStateMachine which hosts this state machine.
     */
    public StateRegistry<S, T> getSuperstateIncludingParentStateMachine() {
        if (superstate != null) {
            return superstate;
        }
        return parentStateMachineSuperState;
    }

    /**
     * same method as getState()
     * @return
     */
    public S getUnderlyingState() {
        return state;
    }

    /**
     * returns true if the present stateRegistry has any parallel states in its
     * substate-tree down to the bottom. This includes the present registry itself:
     * it will return false if the present state registry is parallel.
     *
     * @author rinke
     */
    public boolean hasNestedParallels() {
        if (isParallel()) {
            return true;
        }
        for (int i = 0, iSize = substates.size(); i < iSize; i++) {
            final StateRegistry<S, T> substateRegistry = substates.get(i);
            if (substateRegistry.isParallel()) {
                return true;
            }
        }
        return false;
    }

    /**
     * returns true if the present stateRegistry has direct children states
     * exactly one level lower which run in parallel.
     * It doesn't report any parallel children which are on deeper levels.
     * It reports false if the state represented is a parallel state, but does
     * not have child states configured yet. If you want to test on the single
     * fact that the state is configured as a parallel, use {@link #isParallel()}.
     * At configuration time, the result of these methods may differ. When configuration
     * is done, they should have the same result.
     *
     * @author rinke
     */
    public boolean hasParallelState() {
        return !parallelRegistries.isEmpty();
    }

    /**
     * checks if the argument is a substate of the underlying state, or if it is equal to the underlying state.
     * Returns true in case of all possible substates of the underlying states, no matter how deeply nested.
     * However, it doesn't look into any child state machines.
     */
    // tested
    public boolean includes(S stateToCheck) {
        for (int i = 0, iSize = substates.size(); i < iSize; i++) {
            final StateRegistry<S, T> s = substates.get(i);
            if (s.includes(stateToCheck)) {
                return true;
            }
        }
        return state.equals(stateToCheck);
    }

    /**
     * checks if the argument is equal to the underlying state, or if it is a substate of the
     * underlyhing state, no matter how deeply nested. Checking of nested parallel substates
     * depends on the value of the <code>parallel</code> argument.
     * @param parallel - if true, also checks child state machines if the <code>stateToCheck</code>.
     * if false, doesn't look into sub-statemachines present in substates which are parallel. If false,
     * it just calls {@link #includes(Object)}.
     */
    // tested
    public boolean includes(S stateToCheck, boolean _parallel) {
        // easy check first
        if (state.equals(stateToCheck)) {
            return true;
        }
        if (_parallel) {
            for (int i = 0, iSize = parallelRegistries.size(); i < iSize; i++) {
                final ParallelRegistry<S, T> parallelReg = parallelRegistries.get(i);
                if (parallelReg.getAllStatesNotParent().contains(stateToCheck)) {
                    return true;
                }
            }
        }
        for (int i = 0, iSize = substates.size(); i < iSize; i++) {
            final StateRegistry<S, T> s = substates.get(i);
            if (s.includes(stateToCheck, _parallel)) {
                return true;
            }
        }
        return false;
    }

    /**
     * basically the same as the enter method, but with less checks. Specifically used to enter the
     * initial state of a state machine, so coming from nothing.
     *
     * @author rinke
     */
    public void initEnter(IStateMachineContext<S, T> context, Transition<S, T> transition, Object... args) {
        requireNonNull(transition, "transition is null");
        if (superstate != null) {
            superstate.initEnter(context, transition, args);
        }
        executeEntryActions(context, transition, args);
    }

    /**
     * returns true if this is a leaf state, that is: if it has no substates.
     * A state having parallel states is no leaf state.
     *
     * @author rinke
     */
    public boolean isLeafState() {
        return substates.isEmpty() && parallelRegistries.isEmpty();
    }

    /**
     * returns true if the state is configured as a parallel state, regardless of the fact that
     * it has substates or not. A state is a parallel state if it holds substates which are configured as
     * nested state machines.
     *
     * @author rinke
     */
    public boolean isParallel() {
        return parallel;
    }

    /**
     * checks if the underlying state is related to <code>otherState</code>. The states are related if
     * (and only if):
     * <ul>
     * <li>otherState is not null
     * <li>and one of the following holds true:
     * <ul>
     * <li>the underlying state and otherState are equal
     * <li>the underlying state is a descendant (substate) of otherState
     * <li>OtherState is a descendant (substate) of the underlying state state
     * </ul>
     * </ul>
     * It also looks into parallel (sub)statemachines.
     *
     * @param otherState
     * @return
     */
    // tested
    public boolean isRelatedState(S otherState) {
        if (state == null || otherState == null) {
            return false;
        }
        // look down
        if (includes(otherState, true)) {
            return true;
        }
        // look up
        StateRegistry<S, T> superReg = getSuperstateIncludingParentStateMachine();
        do {
            if (superReg.getUnderlyingState().equals(otherState)) {
                return true;
            }
            superReg = superReg.getSuperstateIncludingParentStateMachine();
        } while (superReg != null);
        return false;
    }

    @Override
    public String toString() {
        return getUnderlyingState().toString();
    }

    /**
     * returns the triggerbehavior for the given trigger. A triggerBehaviour will be returned if the trigger
     * parameter is registered to the state, and if there is a condition defined, this condition must evaluate to true.
     * If no triggerbehaviour is found on this underlying state, it will look in the superstate.
     * This excludes looking in super states which are part of a parent state machine.
     *
     * @param context
     * @param trigger
     * @param resurfacingId - if not null, only returns resurfacing TriggerBehaviours. If null, Resurfacing TriggerBehaviours are
     * EXCLUDED from the result. A partial transition is a transition which starts in a child state machine, leaves the child
     * state machine, and goes to a state in the parent state machine. The part inside the parent state machine is called a
     * resurfacingTriggerBehaviour.
     *
     * @author rinke
     */
    public TriggerBehaviour<S, T> tryFindHandler(IStateMachineContext<S, T> context, T trigger, Integer resurfacingId,
            Object... args) {
        TriggerBehaviour<S, T> result = tryFindLocalHandler(context, trigger, resurfacingId, args);
        result = tryFindHandlerInSomeSuper(context, trigger, result, superstate, resurfacingId, args);
        return result;
    }

    void addEntryAction(final Action<S, T> action) {
        requireNonNull(action, "action is null");
        entryActions.add(action);
    }

    /**
     * adds the action as an entry action if the specified trigger is equal to the transition trigger that fires the
     * action at runtime. If the specified trigger is null, it just adds the action without any check.
     */
    void addEntryAction(final Action<S, T> action, final ParameterizedTrigger<S, T> trigger) {
        requireNonNull(action, "action is null");

        entryActions.add((context, t, p, args) -> {
            if (trigger != null) {
                final T trans_trigger = t.getTrigger();
                if (trans_trigger != null && trans_trigger.equals(trigger.getTrigger())) {
                    action.doIt(context, t, p, args);
                }
            } else {
                action.doIt(context, t, p, args);
            }
        });
    }

    void addExitAction(Action<S, T> action) {
        requireNonNull(action, "action is null");
        exitActions.add(action);
    }

    /**
     * adds a parallel state with its own state machine as a child of the represented state.
     *
     */
    void addParallelRegistry(ParallelRegistry<S, T> stateMachineConfig) {
        requireNonNull(stateMachineConfig, "config is null");
        parallelRegistries.add(stateMachineConfig);
    }

    /**
     * adds a substate to the represented state. If this is the first substate present,
     * it is also marked as initialState.
     *
     */
    void addSubstate(StateRegistry<S, T> substate) {
        requireNonNull(substate, "substate is null");
        if (substates.isEmpty() && !isParallel()) {
            // set the initial state, but first check if it's frozen.
            if (frozen) {
                throw new IllegalStateException("You cannot change the state tree structure when it has "
                        + "been used already for configuring a trigger.");
            }
            // we don't call the setter, because that would create an endless loop. Modify the field directly.
            initialState = substate;
        }
        substates.add(substate);
    }

    /**
     * adds a trigger behaviour to the represented state.
     */
    void addTriggerBehaviour(TriggerBehaviour<S, T> triggerBehaviour) {
        freeze();
        if (triggerBehaviour.isResurfacing()) {
            resurfacingTriggerBehaviours.add(triggerBehaviour);
        } else {
            triggerBehaviours.add(triggerBehaviour);
        }
    }

    /**
     * freezes the present configuration of super- and substates of this state. This means that the state
     * cannot change its superstates, its initial state, and its present substates. New substates can be
     * added though, as long as this doesn't change the existing substate and initial state configuration.
     * Any attempt to change the superstate, initial state or existing substates once the state is set
     * <code>frozen</code> must lead to an exception.<br>
     * This method freezes the underlying state, and its superstate, even if that is in a parent state machine.
     * It also freezes all known substates.
     * <p>
     * This method is used to ensure that relations between states don't change anymore (due to further
     * configuration) after it has been used to determine trigger destination.
     * If a trigger would point to a destination which is still subject to changes, this could severly mess
     * up the correct handling of triggers, hence the need to freeze the configuration, specifically in case
     * of parallelism.
     * <p>
     * In previous versions of stateless4j you could configure a destination of a transition without first
     * having configured/registered the destination state itself. This is not possible anymore.
     * You MUST now first register the destination (via a call to {@link MachineConfigurer#configure(Object)} or
     * {@link MachineConfigurer#parallel(Object)}) before you can make the state the destination of
     * a transition. The recommended order is to first register all states, and only after that register/configure
     * all transitions.
     * <p>
     * Note that freezing a state will only block changes in the sub- and superstate configuration. Freezing
     * a state will never prevent you from configuring new transitions from the state, or setting onentries or
     * onexits.
     *
     * @author rinke
     */
    void freeze() {
        if (frozen) {
            return;
        }
        frozen = true;
        if (superstate != null) {
            superstate.freeze();
        }
        if (parentStateMachineSuperState != null) {
            parentStateMachineSuperState.freeze();
        }
        for (int i = 0, iSize = substates.size(); i < iSize; i++) {
            final StateRegistry<S, T> subState = substates.get(i);
            subState.freeze(); // first check in this method will prevent endless loop
        }

    }

    /**
     * gets all triggerBehaviours connected to this state (except ResurfacingTriggerBehaviours).
     *
     * @author rinke - adapted from original by applying Multimap
     */
    Multimap<T, TriggerBehaviour<S, T>> getTriggerBehaviours() {
        return triggerBehaviours.getMultimap();
    }

    /**
     * returns true if this state or any of its substates has triggers configured.
     * @return
     */
    boolean hasNestedTriggersConfigured() {
        if (hasTriggersConfigured()) {
            return true;
        }
        for (int i = 0, iSize = substates.size(); i < iSize; i++) {
            final StateRegistry<S, T> subState = substates.get(i);
            if (subState.hasTriggersConfigured()) {
                return true;
            }
        }
        return false;
    }

    /**
     * if a state is frozen, it means that its existing super- and substate configuration is fixed. This means the state
     * cannot be put anymore under another substate, nor can it change its existing substates, or its initial state.
     * New substates can be added, as long as this doesn't change the initial state.
     */
    boolean isFrozen() {
        return frozen;
    }

    /**
     * sets the argument as the initial state of this state. The initial state is the substate in which
     * the state machine automatically moves in case no explicit substate was mentioned as target.
     * This call will replace any marking as initial state which already may have exsisted.
     * If the argument was not yet added as substate, this call will also add it as a substate.
     *
     * @param substate - mark this state as the initial state of the stateRegistry.
     * @throws IllegalStateException - when the state is frozen (for example in case the previous
     * initial state has already been used for trigger configuration), the initialState is frozen
     * and cannot be reset. In such a case, an IllegalStateException is thrown.
     *
     * @author rinke
     */
    void setInitialState(StateRegistry<S, T> substate) {
        if (frozen) {
            throw new IllegalStateException("You cannot reset an initial state when it has "
                    + "been used already for configuring a trigger.");
        }
        initialState = substate;
        if (!substates.contains(substate)) {
            this.addSubstate(substate);
        }
    }

    /**
     * sets the parent state of the present presentation, but that
     * parent state is a state of the super state machine.
     *
     * @author rinke
     */
    void setParentStateMachineSuperstate(StateRegistry<S, T> value) {
        if (frozen) {
            throw new IllegalStateException(
                    "You cannot set a superstate on a state that has already been used in a trigger configuration.");
        }
        parentStateMachineSuperState = value;
    }

    /**
     * Sets the parent state of the present stateRegistry.
     */
    void setSuperstate(StateRegistry<S, T> value) {
        if (frozen) {
            throw new IllegalStateException(
                    "You cannot set a superstate on a state that has already been used in a trigger configuration.");
        }
        superstate = value;
        // if the state has a superstate, it is no topLevelState anymore, so reset next field
        parentStateMachineSuperState = null;
    }

    /**
     * executes all entry actions of this state.
     *
     * @author rinke (adapted from original by adding logger)
     */
    private void executeEntryActions(IStateMachineContext<S, T> context, Transition<S, T> transition, Object[] entryArgs) {
        requireNonNull(transition, "transition is null");
        requireNonNull(entryArgs, "entryArgs is null");
        if (LogSwitch.LOG) {
            if (logger.isDebugEnabled()) {
                MachineRegistry.agentIdToMDC(context);
                logger.debug("entering state {}", getUnderlyingState());
            }
        }
        for (int i = 0, iSize = entryActions.size(); i < iSize; i++) {
            final Action<S, T> action = entryActions.get(i);
            action.doIt(context, transition, getUnderlyingState(), entryArgs);
        }
    }

    /**
     * executes all exit actions of this state.
     *
     * @author rinke (adapted from original by adding logger)
     */
    private void executeExitActions(IStateMachineContext<S, T> context, Transition<S, T> transition, Object[] exitArgs) {
        requireNonNull(transition, "transition is null");
        if (LogSwitch.LOG) {
            if (logger.isDebugEnabled()) {
                MachineRegistry.agentIdToMDC(context);
                logger.debug("exiting state {}", getUnderlyingState());
            }
        }
        for (int i = 0, iSize = exitActions.size(); i < iSize; i++) {
            final Action<S, T> action = exitActions.get(i);
            action.doIt(context, transition, getUnderlyingState(), exitArgs);
        }
    }

    /**
     * returns true if it has any triggers configured directly on this state.
     * @return
     */
    private boolean hasTriggersConfigured() {
        return !triggerBehaviours.isEmpty() || !resurfacingTriggerBehaviours.isEmpty();
    }

    /**
     * tries to find the handler in some super state (in the same state machine).
     * @author rinke
     */
    private TriggerBehaviour<S, T> tryFindHandlerInSomeSuper(IStateMachineContext<S, T> context, T trigger,
            TriggerBehaviour<S, T> result, StateRegistry<S, T> someSuper, Integer resurfacingId, Object... args) {
        if (result == null || result instanceof FailedConditionTriggerBehaviour) {
            if (someSuper != null) {
                // look in superstate
                final TriggerBehaviour<S, T> temp = someSuper.tryFindHandler(context, trigger, resurfacingId, args);
                if (temp != null) {
                    // only set to result if not null, otherwise we might overwrite a FailedConditionTriggerBehaviour
                    result = temp;
                }
            }
        }
        return result;
    }

    /**
     * {@link TriggerBehaviourContainer#tryFindHandler(IStateMachineContext, Object, Object, Object[])}
     *
     * @param resurfacingId - The id for a resurfacing. If Not null, will return only resurfacingTriggerBehaviours
     * with the given resurfacingId; if null, will return all but ResurfacingTriggerBehaviours.
     *
     * @author rinke (adapted/simplified original)
     */
    private TriggerBehaviour<S, T> tryFindLocalHandler(IStateMachineContext<S, T> context, T trigger, Integer resurfacingId,
            Object... args) {
        if (resurfacingId != null) {
            return resurfacingTriggerBehaviours.tryFindResurfacingTrigger(trigger, resurfacingId);
        }
        return triggerBehaviours.tryFindHandler(context, trigger, args);
    }

}
