/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.FailedConditionTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TriggerBehaviour;

/**
 * A Container around a Multimap with TriggerBehaviours, with convenience
 * methods to store and retrieve elements.
 *
 * @author rinke
 *
 */
class TriggerBehaviourContainer<S, T> {

    private final Multimap<T, TriggerBehaviour<S, T>>   triggerBehaviours               = ArrayListMultimap.create();
    private final FailedConditionTriggerBehaviour<S, T> failedConditionTriggerBehaviour = new FailedConditionTriggerBehaviour<>();

    /**
     * adds a triggerbehaviour to the container.
     */
    void add(TriggerBehaviour<S, T> triggerBehaviour) {
        requireNonNull(triggerBehaviour, "triggerBehaviour is null");
        triggerBehaviours.put(triggerBehaviour.getTrigger(), triggerBehaviour);
    }

    /**
     * gets all the stored triggerbehaviours with the given trigger.
     */
    Collection<TriggerBehaviour<S, T>> get(T key) {
        return triggerBehaviours.get(key);
    }

    /**
     * gets the complete internal Multimap with all triggerbehaviours, keyed by trigger.
     * Note that this method is NOT safe: changes made to the resulting object will be
     * applied to the container too.
     * @return
     */
    Multimap<T, TriggerBehaviour<S, T>> getMultimap() {
        return triggerBehaviours;
    }

    boolean isEmpty() {
        return triggerBehaviours.isEmpty();
    }

    Set<T> keySet() {
        return triggerBehaviours.keySet();
    }

    /**
     * Remove a {@link TriggerBehaviour} from this container.
     */
    boolean remove(T trigger, TriggerBehaviour<S, T> triggerBehaviour) {
        return triggerBehaviours.remove(trigger, triggerBehaviour);
    }

    /**
     * returns the triggerBehaviour for the given trigger. A triggerBehaviour will be returned if the trigger
     * parameter is registered (to the state), and if there is a condition defined, this condition must evaluate to true.
     * This method looks only at the given underlying state; not at its sub- or superstates.
     *
     * @param resurface - if true, only returns resurfacing TriggerBehaviours. If false, Resurfacing TriggerBehaviours are
     * EXCLUDED from the result. A partial transition is a transition which starts in a child state machine, leaves the child
     * state machine, and goes to a state in the parent state machine. The part inside the parent state machine is called a
     * resurfacingTriggerBehaviour.
     *
     *
     * @throws IllegalStateException if more than one trigger behaviour is found.
     *
     */
    TriggerBehaviour<S, T> tryFindHandler(IStateMachineContext<S, T> context, T trigger, Object[] args) {
        Collection<TriggerBehaviour<S, T>> possibles = triggerBehaviours.get(trigger);

        if (possibles == null || possibles.isEmpty()) {
            return null;
        }

        List<TriggerBehaviour<S, T>> actual = new ArrayList<>();

        Iterator<TriggerBehaviour<S, T>> it = possibles.iterator();
        for (int i = 0, iSize = possibles.size(); i < iSize; i++) {
            TriggerBehaviour<S, T> triggerBehaviour = it.next();
            if (triggerBehaviour.isGuardConditionMet(context, args)) {
                actual.add(triggerBehaviour);
            }
        }

        if (actual.isEmpty() && !possibles.isEmpty()) {
            // there are triggerbehaviours, but no condition is met
            return failedConditionTriggerBehaviour;
        }

        // POLICY CHANGED: in case of multiple exit transitions, we just use the first.
        return actual.isEmpty() ? null : actual.get(0);
    }

    /**
     * Returns the TriggerBehaviour for a Resurfacing trigger, by its resurfacingId.
     * An extra check is performed if the trigger matches. The guard is never checked with a resurfacing
     * trigger, as that should have been done already on a lower (child) level.
     * @throws IllegalStateException if no matching triggerBehaviour was found
     * @throws IllegalStateException if more than one matching triggerBehaviour was found.
     */
    TriggerBehaviour<S, T> tryFindResurfacingTrigger(T trigger, Integer resurfacingId) {
        Collection<TriggerBehaviour<S, T>> possibles = triggerBehaviours.get(trigger);

        if (possibles == null || possibles.isEmpty()) {
            return null;
        }

        List<TriggerBehaviour<S, T>> actual = new ArrayList<>();
        Iterator<TriggerBehaviour<S, T>> it = possibles.iterator();
        for (int i = 0, iSize = possibles.size(); i < iSize; i++) {
            TriggerBehaviour<S, T> triggerBehaviour = it.next();
            if (triggerBehaviour.getResurfacingId() != null && triggerBehaviour.getResurfacingId().equals(resurfacingId)) {
                actual.add(triggerBehaviour);
            }
        }

        if (possibles.isEmpty()) {
            // not found
            throw new IllegalStateException("No resurfacing TriggerBehaviour found in parent state machine. ");
        }
        if (actual.size() > 1) {
            throw new IllegalStateException("More than one resurfacing TriggerBehaviour found in parent state machine. "
                    + "You cannot use the same trigger for exiting different substatemachines in different parallel states.");
        }

        return actual.get(0);
    }

}
