/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import static java.util.Objects.requireNonNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Supplier;

import org.slf4j.MDC;

import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ParameterizedTrigger;

/**
 * Class that stores the actual configuration settings of the state machine. It doesn't do the
 * configuration itself, it just stores the results of the configuration process.
 * <p>
 * It basically stores the following:
 * <ul>
 * <li>containedStates: a map with all contained states plus their Registry.
 * <li>trigger configuration: a state machine can react on triggers, for example
 * with a transition from one state to another. The trigger configuration
 * stores how the state machine reacts to which trigger.
 * <li>initial state: See {@link StateConfigurer} for details.
 * <li>a supplier to get reference to the nestedConfigs in the MachineConfiguration.
 * nestedConfigs is information on nested state machines in parallel states under this
 * particular state machine.
 * <li>some specific settings and actions.
 * </ul>
 *
 * @author rinke
 *
 */
public class MachineRegistry<S, T> {

    /**
     * method to log the underlying agent's id, if available.
     */
    public static <S, T> void agentIdToMDC(IStateMachineContext<S, T> context) {
        Long id = (Long) context.getAttribute("id");
        if (id == null) {
            String alt = context.getStateMachine().toString();
            alt = alt.substring(alt.indexOf("@") + 1);
            MDC.put("id", alt);
        } else {
            MDC.put("id", id.toString());
        }
    }

    /**
     * map with TState as key and StateRegistry as value. Every state, substate or parallel state known by the
     * state machine will be registered here. SuperStates known to a hosting state machine at super level will
     * NOT be known.
     */
    final Map<S, StateRegistry<S, T>>                                    containedStates                  = new HashMap<>();
    final Map<T, ParameterizedTrigger<S, T>>                             triggerConfiguration             = new HashMap<>();
    protected S                                                          initialState;
    /**
     * as the Multimap with nested configs is needed for both the configurer and the registry, it is placed in the
     * configurer, but its getter has a link via this supplier, so that the registry can use the Multimap with
     * nested configurations of the configurer.
     */
    final private Supplier<Multimap<S, ParallelMachineConfigurer<S, T>>> nestedConfigsSupplier;

    /**
     * Defines that entry actions for initial states are always executed.
     * <p>
     * Added in stateless4j 2.5.2.
     * Default was false in stateless4j, for backward compatibility reasons. Prior to 2.5.2,
     * entering the initial state never fires its entry action.
     * <p>
     * In parallelstateless4j, the default is changed to true, because that seems to be the
     * most logical behaviour.
     * The setting is removed, and the <code>MachineConfigurer</code> doesn't allow setting
     * this anymore. The only reason that this field is maintained and set to final, is for
     * clarity reasons, so that this decision is clearly documented.
     */
    private final boolean                                                entryActionOfInitialStateEnabled = true;
    private Action<S, T>                                                 unhandledTriggerAction;
    private Action<S, T>                                                 failedConditionTriggerAction;

    MachineRegistry(Supplier<Multimap<S, ParallelMachineConfigurer<S, T>>> supplier) {
        nestedConfigsSupplier = supplier;
    }

    /**
     * checks if two states are related to each other.
     * See {@link StateRegistry#isRelatedState(Object)} for a definition
     * of "related".
     *
     * @param oneState - may be null, in which case <code>false</code> is returned
     * @param otherState - may be null, in which case <code>false</code> is returned
     * @return <code>true</code> if the states are related; <code>false</code> otherwise.
     */
    @Deprecated // This method wasn't used so far. Leave it here until there is an occasion to use it,
    // and then un-deprecated. But as it hasn't been tested either, it also will need a decent test before using it.
    public boolean areRelatedStates(S oneState, S otherState) {
        StateRegistry<S, T> reg = getPossiblyNestedStateRegistry(oneState);
        if (reg == null) {
            return false;
        }
        return reg.isRelatedState(otherState);
    }

    /**
     * In case of a transition, states are exited until the common ancester of the current state and
     * the destination state is found. This method finds the state just below the common ancester, which is
     * the topmost state which needs to be exited in order to perform the transition to the destination.
     *
     * @param current
     * @param destination
     *
     * @return the state closest to toplevel which needs to be exited in order to perform the transition
     * to the destination.
     */
    public StateRegistry<S, T> findTopmostExitState(StateRegistry<S, T> current, S destination) {
        StateRegistry<S, T> destinationStateRegistry = getStateRegistry(destination);
        if (destinationStateRegistry == null) {
            return null;
        }
        if (current.includes(destination)) {
            // destination is a substate of current, no need to exit anything
            return null;
        }
        StateRegistry<S, T> previous = current;
        StateRegistry<S, T> temp = current.getDirectSuperstate();
        while (temp != null && !temp.includes(destination)) {
            previous = temp;
            temp = temp.getDirectSuperstate();
        }
        return previous;
    }

    /**
     * See {@link StateRegistry#getAllRelatedStates()}.
     *
     * @throws IllegalArgumentException if state is unknown.
     */
    // too simple for testing
    public Collection<S> getAllRelatedStates(S state) {
        StateRegistry<S, T> reg = getPossiblyNestedStateRegistry(state);
        if (reg == null) {
            throw new IllegalArgumentException("unknown state");
        }
        return reg.getAllRelatedStates();
    }

    /**
     * gets all associated states, being the direct states known to this state machine,
     * plus all states associated to child state machines. It doesn't get states from
     * super-statemachines (of course, as it has no knowledge of those).
     *
     */
    public Set<S> getAllStates() {
        Set<S> result = new HashSet<>(getStates());
        result.addAll(getSubStateMachineStates());
        return result;
    }

    /**
     * retrieves the Action which is to be taken when a trigger guard (condition) fails.
     */
    public Action<S, T> getFailedConditionTriggerAction() {
        return failedConditionTriggerAction;
    }

    /**
     * Analogous to the {@link MachineConfigurer#getParallelContainingStateEntry(Object)} but
     * contains a registry as value (and not a configurer).
     *
     */
    public Map.Entry<S, ParallelRegistry<S, T>> getParallelContainingStateEntry(S state) {
        for (Map.Entry<S, ParallelMachineConfigurer<S, T>> entry : nestedConfigsSupplier.get().entries()) {
            // value may be null if substates of parallel are not yet registered
            if ((entry.getValue() != null) &&
                    (entry.getValue().getRegistry().getAllStatesNotParent().contains(state))) {
                return Maps.immutableEntry(entry.getKey(), entry.getValue().getRegistry());
            }
        }
        return null;
    }

    /**
     * Gives StateRegistry for the specified state. May return null.
     * Does not look into child state machines.
     *
     * @param state The state
     * @return StateRegistry for the specified state, or null.
     */
    public StateRegistry<S, T> getStateRegistry(final S state) {
        return containedStates.get(state);
    }

    /**
     * Gives the StateRegistry for the specified state. May return null.
     *
     * @param state
     * @param searchNestedParallel - if true, also looks into child state machines. if false,
     * only looks in the actual level state machine.
     * @return
     */
    public StateRegistry<S, T> getStateRegistry(final S state, boolean searchNestedParallel) {
        requireNonNull(state, "state is null");
        StateRegistry<S, T> result = containedStates.get(state);
        if (searchNestedParallel) {
            while (result == null) {
                if (getAllStates().contains(state)) {
                    ParallelRegistry<S, T> parallelReg = getParallelContainingStateEntry(state).getValue();
                    result = parallelReg.getStateRegistry(state, true);
                } else {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * gets the LOCAL triggerConfiguration, so the triggerConfiguration valid for this
     * state machine, and not for any substatemachine in a parallel state.
     *
     * @param trigger
     * @return
     */
    public ParameterizedTrigger<S, T> getTriggerConfiguration(final T trigger) {
        return triggerConfiguration.get(trigger);
    }

    /**
     * retrieves the Action which is to be taken when an unhandled trigger is encountered.
     */
    public Action<S, T> getUnhandledTriggerAction() {
        return unhandledTriggerAction;
    }

    /**
     * Gets whether the entry action of the initial state of the state machine
     * must be executed when the state machine starts.
     * Default is false for backward compatibility sake.
     *
     * Added in 2.5.2
     *
     * @return true if the entry action of the initial state of the state machine
     * must be executed when the state machine starts.
     */
    public boolean isEntryActionOfInitialStateEnabled() {
        return entryActionOfInitialStateEnabled;
    }

    /**
     * Checks if the argument is a parallel state, by checking if it has
     * child state machines attached. This method will only return true if the
     * state is a parallel state in this config; parallel states which are in a
     * sub-statemachine cofig will not be found by this method, and thus return false.
     *
     */
    public boolean isParallelState(S state) {
        return !nestedConfigsSupplier.get().get(state).isEmpty();
    }

    /**
     * gets the initial state of a state machine. The initial state is the first state the state machine
     * is in, after it has been created.
     * <p>
     * Even though some state may have been marked as initial state, this method will only return a leaf
     * state (a state without child states) as initial state. If the state on the config marked as
     * initial state is NOT a leaf state, the method will seek the initial state of this marked state,
     * and repeat the procedure with that state. So it takes the initial state of an initial state (of an
     * initial state, etc) until it finds a leaf state.<br>
     * If the marked initial state of the state machine is NOT a leaf state, the initialState field of
     * this StateMachineConfig will be overwritten by the newly found leaf initial state.
     * <br>
     * If a parallel state is met on the way, it will be considered as a leaf state.
     */
    public S retrieveInitialState() {
        StateRegistry<S, T> resultStateRegistry = containedStates.get(initialState);
        while (resultStateRegistry != null && !resultStateRegistry.getDirectSubstates().isEmpty()) {
            resultStateRegistry = resultStateRegistry.getInitialState();
        }
        initialState = resultStateRegistry.getUnderlyingState();
        return initialState;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("SMReg: ");
        sb.append(initialState.toString());
        return sb.toString();
    }

    /**
     * returns all states known to any sub-statemachine. States belonging to the actual
     * state machine are NOT reported.
     *
     */
    protected Set<S> getSubStateMachineStates() {
        Set<S> result = new HashSet<>();
        for (ParallelMachineConfigurer<S, T> subStateMachineReg : nestedConfigsSupplier.get().values()) {
            if (subStateMachineReg != null) {
                result.addAll(subStateMachineReg.getRegistry().getAllStatesNotParent());
            }
        }
        return result;
    }

    /**
     * gets all states in the configuration which do not have a parent state.
     */
    protected Set<S> getTopLevelStates() {
        Set<S> result = new HashSet<>(containedStates.keySet().size());
        for (Entry<S, StateRegistry<S, T>> entry : containedStates.entrySet()) {
            if (entry.getValue().getDirectSuperstate() == null) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    /**
     * gets the StateRegistry for the initial state of the state machine. The initial state
     * is the first state the state machine is in after it has been created.
     *
     */
    StateRegistry<S, T> getInitialStateRegistry() {
        if (initialState == null) {
            return null;
        }
        return getStateRegistry(initialState);
    }

    /**
     * gets all the known states in the state machine, no matter how deeply
     * nested.
     * However, it doesn't go into sub-stateMachines. So children
     * of parallel states are not reported.
     *
     * @return
     */
    Set<S> getStates() {
        return containedStates.keySet();
    }

    /**
     * Checks if the argument is a parallel state. In contrast to {@link #isParallelState(Object)},
     * this method also finds deeply nested parallel states. Returns false if not found.
     */
    boolean isParallelStatePossiblyNested(S state) {
        if (!getAllStates().contains(state)) {
            return false;
        }
        boolean directlyFound = isParallelState(state);
        if (directlyFound) {
            return true;
        }
        StateRegistry<S, T> stateRegistry = getPossiblyNestedStateRegistry(state);
        return stateRegistry.isParallel();
    }

    void setFailedConditionTriggerAction(Action<S, T> action) {
        failedConditionTriggerAction = action;
    }

    void setUnhandledTriggerAction(Action<S, T> action) {
        unhandledTriggerAction = action;
    }

    /**
     * as {@link #getParallelContainingStateEntry(Object)} but returns just the ParallelRegistry.
     * @param state
     * @return
     */
    private ParallelRegistry<S, T> getParallelContainingState(S state) {
        for (Map.Entry<S, ParallelMachineConfigurer<S, T>> entry : nestedConfigsSupplier.get().entries()) {
            // value may be null if substates of parallel are not yet registered
            if ((entry.getValue() != null) &&
                    (entry.getValue().getRegistry().getAllStatesNotParent().contains(state))) {
                return entry.getValue().getRegistry();
            }
        }
        return null;
    }

    /**
     * finds the StateRegistry of a state who is possibly known in the present state
     * machine, or in some sub-statemachine. Returns
     * null if not found.
     * @param state
     * @return
     */
    private StateRegistry<S, T> getPossiblyNestedStateRegistry(final S state) {
        requireNonNull(state, "state is null");
        StateRegistry<S, T> result = containedStates.get(state);

        boolean foundSomewhere = false;
        while (result == null) {
            if (getAllStates().contains(state)) {
                foundSomewhere = true;
                ParallelRegistry<S, T> parallelReg = getParallelContainingState(state);
                result = parallelReg.getStateRegistry(state);
            } else {
                break;
            }
        }

        if (result == null && foundSomewhere) {
            throw new IllegalStateException("Could not locate existing state: " + state.toString());
        }
        return result;
    }

}
