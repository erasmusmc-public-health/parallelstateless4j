/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Function;

import nl.erasmusmc.mgz.parallelstateless4j.StateMachine;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;

/**
 * Can be used to configure parallel state machines. This is the state machine in a child state
 * of a parallel state.
 * <p>
 * The architectural approach for parallel state machines is as follows.
 *
 * <h2>General description</h2>
 * A parallel state is a state which encapsulates substates. All substates of a parallel state
 * will be simultaneously active if the parallel state is active. This is acchieved by using
 * nested state machines: The parallel state is configured as a normal state, but it holds references
 * to a child state machine instance for each of the declared substates of the parallel state.
 * In the parent state machine (where the parallel state is part of), the parallel state has no substates.
 * <p>
 * When the parallel state is entered (becomes active), the associated child state machines will be instantiated
 * and become active. When the parallel state is left, the associated child state machines will be closed
 * and deleted.
 *
 * <h2>Transitions to a parallel state</h2>
 * When a transition of the parent state machine points to the parallel state, the child state machines will
 * be instantiated, and will go into their initial states. No explicit trigger is followed for the movement
 * inside the child state machines.
 *
 * <h2>Transitions from parent state machine to child state machine</h2>
 * When a transition is invoked from the parent state machine, but points to a target inside the child
 * state machine (so, to a target which is some substate of a parallel state), a special approach is followed.
 * In the parent state machine, the trigger cannot be followed to the nominal target, because that target doesn't
 * exist in the parent state machine (as parallel states in the parent have no substates). In the parent,
 * when the target of a trigger cannot be found, the state machine looks for the target in the associated
 * child state machines. Then the hosting parallel state will serve as the target of the transition for
 * the parent state machine.
 * <p>
 * As the hosting parallel state is entered, all associated child state machines are instantiated. The specific
 * child state machine which holds the original target is then subject to a new trigger, which leads that
 * child state machine to the original target state.
 * Other child state machines are instantiated as well, but as they don't hold the original target, they will
 * use their declared initial states to go to the appropriate leaf state.
 * <p>
 *
 * So a transition into a child state machine is cut up in two different parts: one part in the parent, pointing
 * to the hosting parallel state, and one part in the child, pointing to the original declared target.
 * <p>
 *
 * As transitions may have guards (conditions) and actions associated with them, special care is to be given to
 * those when cutting up a transition in two parts:
 * <ul>
 * <li>Guards (conditions) are dropped when going into the child state machine. The reason for this is, that if
 * the first part of the transition took place (inside the parent state machine, to the parallel state), the
 * second part MUST also take place, and thus must not be subject to a guard which could prevent the second part
 * not happening, while the first part did happen. The idea is that when the guard didn't stop the first part,
 * it shouldn't be able to stop the second part.
 * <li>Actions are not dropped when going into the child state machine. This is because we don't know in advance
 * if the action is to be executed inside the parent state machine, or in the child state machine. Mostly (but
 * not always) actions take place on something inside the StateMachineContext, which is the same for the parent
 * as for the child state machine. In these cases, we considered the risk of an action not taking place in the
 * right state machine more dangerous than the risk of an action being executed twice - which is what usually
 * happens in case of cross-statemachine transitions when no special care is given. For this reason, the Action
 * functional interface was extended with an extra parameter, being the parent state. If the user doesn't want the
 * Action to be executed twice in case of cross-state machine transitions, he should put the executional block
 * inside an if-statement which checks on the parent state. This allows the choice to have the Action executed
 * in the parent, or in the child state machine.
 * </ul>
 *
 * <h2>Transitions from child state machine to parent state machine</h2>
 * When a transition is invoked from the child state machine, but points to a target inside the parent
 * state machine, a likewise approach is followed.
 * In the child state machine, the trigger cannot be followed to the nominal target, because that target doesn't
 * exist in the child state machine. In the child,
 * when the target of a trigger cannot be found, the state machine looks for the target in the referenced parent
 * state machine.
 * <p>
 * Again, the orinigal transition is split up into two parts:
 * <ul>
 * <li>child state machine: transition to an added final state called <code>out</code>, so that all onExit's
 * are executed as expected. After that the child state machine is killed.
 * <li>parent state machine: transition from hosting parallel state to the original transition target.
 * </ul>
 *
 * Again, any guard is only applied to the first part, and dropped for the second part.
 * Actions are active for both parts.
 *
 * <h2>Elements for communication between child and parent state machines</h2>
 * Several elements exist for communication/references between parent and child state machines:
 * <ul>
 * <li>Each {@link MachineConfigurer} has a Multimap storing the child state machines. These are all
 * possible state machine configs.
 * <li>Each <code>ParallelStateMachineConfig</code> has a <code>parentConfiguration</code> field,
 * pointing to the configuration of the parent state machine.
 * <li>Also, the <code>ParallelStateMachineConfig</code> stores the state in the parent state machine
 * which hosts the child state machines. This is the field <code>hostingStateInParent</code>.
 * <li>At runtime, a {@link StateRegistry} stores the state in the parent state machine which
 * hosts the child state machines in the field <code>parentStateMachineSuperState</code>. This is the
 * same field as the previous item in this list, but now as a StateRegistry. Note that in such a
 * case, the <code>superstate</code> field is null, as superstate only looks in the present state machine,
 * not in a parent.
 * <li>The StateRegistry also stores a List with child <code>ParallelStateMachineConfig</code>s, if
 * the state of the StateRegistry is a parallel state.
 * <li>At runtime the {@link StateMachine} instance holds a List with actual child state machines in the
 * field <code>parallelStateMachines</code>. Note that in contrast to the first item of this summation, this
 * list only holds actual active child state machine instances.
 * </ul>
 *
 * @author rinke
 */
class ParallelMachineConfigurer<S, T> extends MachineConfigurer<S, T> {

    private MachineConfigurer<S, T> parentConfiguration;

    ParallelMachineConfigurer(MachineConfigurer<S, T> parentConfiguration) {
        this.parentConfiguration = parentConfiguration;
        registry = new ParallelRegistry<S, T>(this::getNestedConfigs, parentConfiguration.registry);
    }

    public MachineConfigurer<S, T> getParentConfiguration() {
        return parentConfiguration;
    }

    @Override
    public ParallelRegistry<S, T> getRegistry() {
        return (ParallelRegistry<S, T>) super.getRegistry();
    }

    @Override
    public void onFailedConditionTrigger(final Action<S, T> failedConditionTriggerAction) {
        throw new UnsupportedOperationException("This can only be set at the toplevel configuration");
    }

    @Override
    public void onUnhandledTrigger(final Action<S, T> unhandledTriggerAction) {
        throw new UnsupportedOperationException("This can only be set at the toplevel configuration");
    }

    @Override
    public StateConfigurer<S, T> parallel(final S parallel) {
        throw new UnsupportedOperationException("This can only be called at a toplevel configuration");
    }

    /**
     * moves a StateRegistry from the parent configuration to this configuration.
     * This usually happens when the Registry's parent state is set via StateConfigurationHelper.substateOf()...
     * It removes the Registry from the parent config, puts it in this config, and sets the initial state
     * of this config if necessary.
     *
     * @return The lookup function, to be set on the StateConfigurer for this stateRegistry.
     *
     */
    protected Function<S, StateRegistry<S, T>> moveFromParent(StateRegistry<S, T> stateInParallel) {
        // I don't think this check is necessary, but alas...
        if (stateInParallel.hasNestedTriggersConfigured()) {
            // disallow moving around states over parallels if there are already triggerConfigurations in any involved states.
            throw new StateTypeInUseException(stateInParallel.toString() + "has already triggers in use, so cannot "
                    + "be configured as parallel state. First configure as parallel, then define triggers...");
        }

        S underlying = stateInParallel.getUnderlyingState();
        parentConfiguration.getRegistry().containedStates.remove(underlying);
        if (getRegistry().containedStates.isEmpty()) {
            setInitialState(stateInParallel);
        }
        getRegistry().containedStates.put(underlying, stateInParallel);
        if (parentConfiguration.getRegistry().getInitialStateRegistry().getUnderlyingState()
                .equals(stateInParallel.getUnderlyingState())) {
            // the state to be moved is the initial state of the parent config, so undo that.
            // this might create problems as the parent config doesn't have an initial state anymore,
            // but then the user will have to set it explicitly.
            parentConfiguration.getRegistry().initialState = null;
        }
        // if the state to be moved is itself a parallel state...
        if (stateInParallel.isParallel()) {
            // put all attached parallelconfigs of moving state to this parallelconfig
            nestedConfigs.putAll(underlying, parentConfiguration.nestedConfigs.get(underlying));
            // ..and remove them from the parent config.
            Collection<ParallelMachineConfigurer<S, T>> movedParallelConfigs = parentConfiguration.nestedConfigs
                    .removeAll(underlying);
            // the attached parallel configs of moving state have some points wrongly set...
            Iterator<ParallelMachineConfigurer<S, T>> it = movedParallelConfigs.iterator();
            int size = movedParallelConfigs.size();
            for (int i = 0; i < size; i++) {
                // moved is temporary set to null if the parallels aren't configured yet. So these actions should
                // only be done on non null elements.
                ParallelMachineConfigurer<S, T> moved = it.next();
                if (moved != null) {
                    moved.parentConfiguration = this;
                    moved.getRegistry().setHostingStateInParent(stateInParallel.getSuperstateIncludingParentStateMachine()
                            .getUnderlyingState());
                }
            }
        }
        return super::getOrCreateStateRegistry;
    }

    /**
     * {@inheritDoc}
     * In a nested state machine, every new state's parent is set to the hosting state in the parent state machine.
     * When the new state is assigned a substate, this field must be reset to null.
     *
     */
    @Override
    protected StateRegistry<S, T> registerNewStateRegistry(final S state, boolean asParallel) {
        StateRegistry<S, T> result = super.registerNewStateRegistry(state, asParallel);
        // we don't use the normal substateOf because that would register the parent state to the child state machine
        // so consult the parent registry to get the stateRegistry of the hosting state in parent which is stored in this registry.
        StateRegistry<S, T> hostingStateInParentRegistry = parentConfiguration.getRegistry()
                .getStateRegistry(getRegistry().getHostingStateInParent());
        result.setParentStateMachineSuperstate(hostingStateInParentRegistry);
        return result;
    }

}
