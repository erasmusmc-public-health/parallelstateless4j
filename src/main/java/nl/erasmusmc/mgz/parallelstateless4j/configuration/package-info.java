/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
/**
 * This package contains classes which are needed for configuring the state machine at configuration time.
 * It contains only classes needed for the actual configuration.
 * Once a state machine is used and running, these packages are no longer needed.
 * <p>
 * The general division of tasks over different classes is:
 * <ul>
 * <li><b>MachineConfigurer</b>: entry point for configuration; handles the configuration of the state machine
 * at config-time. Will not be needed anymore after configuration time. Was originally named StateMachineConfigurer,
 * but renamed for better systematic naming.
 * <li><b>MachineRegistry</b>: The stored result of the configuration. Stores all relevant configuration
 * data needed at runtime, for the complete state machine, which are not specific to a particular instance.
 * Was originally named StateMachineRegistry, but renamed for better systematic naming.
 * <li><b>StateConfigurer</b>: As the MachineConfigurer, but for configuration on a particular state.
 * Was originally named StateConfigurationHelper, but renamed for better systematic naming.
 * <li><b>StateRegistry</b>: Stores result of the configuration for a particular state. So it is like
 * the registry, but for only one state. Was originally named StateRepresentation, but renamed for better
 * systematic naming.
 * <li><b>StateMachine</b>: the runtime object which is an instance of a state machine. Only contains data
 * which keeps the actual state of a state machine, relevant to this particular instance.
 * <li><b>Parallel*</b>: Every class with Parallel as prefix does the same as its normally named equivalent,
 * but for nested state machines which are child of a parallel state.
 * </ul>
 *
 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;