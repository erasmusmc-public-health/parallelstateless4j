/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import com.google.common.collect.Multimap;

import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineContext;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TriggerBehaviour;

/**
 * The {@link MachineRegistry} for a nested sub-StateMachine which is a child of a parallel state.
 *
 * @author rinke
 *
 */
public class ParallelRegistry<S, T> extends MachineRegistry<S, T> {

    /**
     * the registry of the StateMachine hosting this registry's statemachine.
     */
    private MachineRegistry<S, T>                 parentRegistry;
    /**
     * the parallel in the parent registry which holds this state machine.
     */
    private S                                     hostingStateInParent;
    private final TriggerBehaviourContainer<S, T> incomingTriggerBehaviours = new TriggerBehaviourContainer<>();
    private Supplier<Boolean>                     entryActionOfInitialStateEnabledSupplier;
    private Supplier<Action<S, T>>                failedConditionTriggerActionSupplier;
    private Supplier<Action<S, T>>                unhandledTriggerActionSupplier;

    ParallelRegistry(Supplier<Multimap<S, ParallelMachineConfigurer<S, T>>> supplier,
            MachineRegistry<S, T> parentRegistry) {
        super(supplier);
        this.parentRegistry = parentRegistry;
        // the default trigger actions must always be read from the toplevel registry, so...
        entryActionOfInitialStateEnabledSupplier = parentRegistry::isEntryActionOfInitialStateEnabled;
        failedConditionTriggerActionSupplier = parentRegistry::getFailedConditionTriggerAction;
        unhandledTriggerActionSupplier = parentRegistry::getUnhandledTriggerAction;
    }

    /**
     * adds a trigger behaviour to the registry.
     * These are incoming triggers: triggers invoked from a parent state
     * machine targeting a state in this state machine.
     */
    public void addTriggerBehaviour(TriggerBehaviour<S, T> triggerBehaviour) {
        incomingTriggerBehaviours.add(triggerBehaviour);
    }

    /**
     * gets all associated states, being the direct states known to this state machine,
     * plus all states associated to child state machines, plus all states associated
     * to the parent state machine.
     * @return
     * @author rinke
     */
    @Override
    public Set<S> getAllStates() {
        Set<S> result = super.getAllStates();
        result.addAll(getSuperStateMachineStates());
        return result;
    }

    @Override
    public Action<S, T> getFailedConditionTriggerAction() {
        return failedConditionTriggerActionSupplier.get();
    }

    @Override
    public Action<S, T> getUnhandledTriggerAction() {
        return unhandledTriggerActionSupplier.get();
    }

    @Override
    public boolean isEntryActionOfInitialStateEnabled() {
        return entryActionOfInitialStateEnabledSupplier.get().booleanValue();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ParallelReg: ");
        sb.append(hostingStateInParent.toString());
        sb.append(" > ");
        sb.append(initialState.toString());
        return sb.toString();
    }

    public TriggerBehaviour<S, T> tryFindHandler(IStateMachineContext<S, T> stateMachineContext, T trigger, Object[] args) {
        return incomingTriggerBehaviours.tryFindHandler(stateMachineContext, trigger, args);
    }

    /**
     * returns all states know to the super (parent) state machine.
     */
    protected Set<S> getSuperStateMachineStates() {
        Set<S> result = new HashSet<>();
        result.addAll(parentRegistry.getStates());
        if (parentRegistry instanceof ParallelRegistry<S, T> castedParentRegistry) {
            result.addAll(castedParentRegistry.getSuperStateMachineStates());
        }
        return result;
    }

    /**
     * gets all associated states, being the direct states known to this state machine,
     * plus all states associated to child state machines, but NOT states associated with
     * the parent state machine.
     * @author rinke
     */
    Set<S> getAllStatesNotParent() {
        Set<S> result = new HashSet<>(getStates());
        result.addAll(getSubStateMachineStates());
        return result;
    }

    S getHostingStateInParent() {
        return hostingStateInParent;
    }

    void setHostingStateInParent(S state) {
        hostingStateInParent = state;
    }

}
