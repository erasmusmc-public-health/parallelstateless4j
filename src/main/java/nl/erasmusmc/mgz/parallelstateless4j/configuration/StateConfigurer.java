/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import static java.util.Objects.requireNonNull;

import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import nl.erasmusmc.mgz.parallelstateless4j.DestinationNotFoundException;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Func;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.DynamicTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ExitingTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.IgnoredTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ParameterizedTrigger;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ReentryTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ResurfacingTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TransitioningTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TriggerBehaviour;

/**
 * Responsible for the configuration of a particular state in a state machine.
 * <h1>Containing...</h1>
 * <ul>
 * <li>StateRegistry: the actual result of the state configuration; all configuration info
 * on this state needed at runtime.
 * <li>hostingConfig: the MachineConfigurer for the state machine where this state is part of.
 * <li>lookup: a getter function that gives access to the MachineRegistry kept in the MachineConfig,
 * so that the StateRegistry of other states than the underlying state can be retrieved.
 * </ul>
 *
 *
 * <h1>Trigger rules</h1>
 * <p>
 * Triggers registered to a state follow this approach:
 * <ul>
 * <li>There is a special approach for cross-statemachine transitions (transitions in or out
 * of a child state machine). See {@link ParallelMachineConfigurer}.
 * <li>if a trigger points to a non-leaf state, the trigger is redirected to a leaf state, by
 * searching for the initial state of the original target, until a leaf state is encountered.
 * See {@link #initialStateOf(Object)} for details.
 * <li>configuration (and resolving the ultimate target) is done as much as possible
 * at config time.
 * <li>Sometimes this configuration is not possible at config time (for example
 * because the target state is not yet registered at the moment the transition is configured).
 * In such a case target resolving all occurs at runtime, in the StateMachine class.
 * </ul>
 * Triggers generally follow the approach of state machine standards; see {@link TransitioningTriggerBehaviour}.
 *
 * <h1>Guard rules</h1>
 * Triggers can be accompanied by a Guard, a condition which is checked at runtime.
 * The following rules apply:
 * <ol>
 * <li>upon a fire, state machine looks for triggerBehaviour in present state and in parents.
 * If no working triggerbehaviour was found, but a triggerbehaviour whose condition was not met is found,
 * then the <code>failedConditionTriggerAction</code> is executed. Note however that first all parents
 * are searched to try and find a working triggerbehaviour, and the failedConditionTriggerAction is only
 * executed if on no level any working triggerBehaviour was found.
 * <li>the failedConditionTriggerAction can be set ({@link MachineConfigurer#onFailedConditionTrigger(Action)})
 * on the config for all state machines, and on an individual instance of a state machine for individual cases.
 * The default is throwing an exception.
 * <li>
 * </ol>
 *
 *
 * @param <S> - the type to represent the state
 * @param <T> - the type to represent the trigger
 */
public class StateConfigurer<S, T> {

    protected final Func<Boolean, S, T>      NO_GUARD  = (context, args) -> Boolean.TRUE;
    private final Action<S, T>               NO_ACTION = (stateMachineContext, t, p, args) -> {
                                                       };
    private final StateRegistry<S, T>        registry;
    /**
     * the lookup function for the states. This class uses external storage for the
     * state registries. This function gives a reference to lookup the states.
     * Usually, it gives access to the MachineConfigurer.getRegistry() method.
     */
    private Function<S, StateRegistry<S, T>> lookup;

    private MachineConfigurer<S, T>          hostingConfig;

    StateConfigurer(final StateRegistry<S, T> stateRegistry,
            final MachineConfigurer<S, T> hostingConfig,
            final Function<S, StateRegistry<S, T>> lookup) {
        registry = requireNonNull(stateRegistry, "state registry is null");
        this.lookup = requireNonNull(lookup, "lookup is null");
        this.hostingConfig = requireNonNull(hostingConfig, "config is null");
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (!(other instanceof StateConfigurer)) {
            return false;
        }
        StateConfigurer<S, T> castedOther = (StateConfigurer<S, T>) other;
        return ((registry).equals(castedOther.registry)
                && (hostingConfig).equals(castedOther.hostingConfig));
    }

    /**
     * ignore the specified trigger when in the configured state
     *
     * @param trigger The trigger to ignore
     * @return The receiver
     */
    public StateConfigurer<S, T> ignore(final T trigger) {
        return ignoreIf(trigger, NO_GUARD);
    }

    /**
     * ignore the specified trigger when in the configured state.
     *
     * @param trigger The trigger to ignore
     * @param action the action which is taken. The action is executed when the trigger is received.
     * The trigger itself doesn't lead to any state transition (because it is ignored).
     *
     * @return The receiver
     */
    public StateConfigurer<S, T> ignore(final T trigger, Action<S, T> action) {
        return ignoreIf(trigger, NO_GUARD, action);
    }

    /**
     * ignore the specified trigger when in the configured state, if the guard returns true. If the guard
     * returns false, then the trigger is interpreted as an unhandled trigger. The
     * unhandledTriggerAction is executed.
     *
     * @param trigger The trigger to ignore
     * @param guard Function that must return true in order for the trigger to be ignored
     * @return The receiver
     */
    public StateConfigurer<S, T> ignoreIf(final T trigger, final Func<Boolean, S, T> guard) {
        return ignoreIf(trigger, guard, NO_ACTION);
    }

    /**
     * ignore the specified trigger when in the configured state, if the guard returns true. If the guard
     * returns false, then the trigger is interpreted as an unhandled trigger. The
     * unhandledTriggerAction is executed.
     *
     * @param trigger The trigger to ignore
     * @param guard Function that must return true in order for the trigger to be ignored
     * @param action the action which is taken. The action is executed if (and only if) the
     * guard is passing. The trigger itself doesn't lead to any state transition (because it is ignored).
     * @return The receiver
     */
    public StateConfigurer<S, T> ignoreIf(final T trigger,
            final Func<Boolean, S, T> guard, Action<S, T> action) {
        requireNonNull(guard, "guard is null");
        if (action == null) {
            action = NO_ACTION;
        }
        S declaredIn = registry.getUnderlyingState();
        TriggerBehaviour<S, T> tb = new IgnoredTriggerBehaviour<>(trigger, guard, action, declaredIn);
        registry.addTriggerBehaviour(tb);
        return this;
    }

    /**
     * Marks the present state as initial state of the argument. The initial state of a
     * state is the substate in which the state machine will automatically go if not explicit
     * target substate is mentioned.<br>
     * If the present state is not yet marked as substate, this method will not only mark it
     * as initial state, but also as substate. <br>
     * This method sets this state explicitly as initial state. If you don't set an initial
     * state explicitly (via this method), then the first configured substate of a state is
     * marked as initial state.
     * <p>
     * BEWARE: ORDER IS IMPORTANT:
     * configure first the initialState before defining transitions/
     * triggers which point to the parent state. Otherwise triggers pointing just to a parent
     * state might not automatically enter into the correct sub/initial state.
     *
     * @param superstate
     * @return
     *
     * @author rinke
     *
     */
    public StateConfigurer<S, T> initialStateOf(final S superstate) {
        if (registry.getDirectSuperstate() == null || registry.getDirectSuperstate().getUnderlyingState() != superstate) {
            substateOf(superstate);
        }
        StateRegistry<S, T> superRegistry = lookup.apply(superstate);
        if (superRegistry.isParallel()) {
            // it makes no sense to set initials in a parallel state, so just return.
            return this;
        }
        superRegistry.setInitialState(registry);
        return this;
    }

    /**
     * Specify an action that will execute when transitioning into the configured state
     *
     * @param entryAction Action to execute, providing details of the transition
     * @return The receiver
     */
    public StateConfigurer<S, T> onEntry(final Action<S, T> entryAction) {
        requireNonNull(entryAction, "entryAction is null");
        registry.addEntryAction(entryAction);
        return this;
    }

    /**
     * Specify an action that will execute when transitioning into the configured state
     *
     * @param trigger The trigger by which the state must be entered in order for the action to execute
     * @param entryAction Action to execute, providing details of the transition
     * @return The receiver
     */
    public StateConfigurer<S, T> onEntryFrom(final ParameterizedTrigger<S, T> trigger, final Action<S, T> entryAction) {
        requireNonNull(entryAction, "entryAction is null");
        registry.addEntryAction(entryAction, trigger);
        return this;
    }

    /**
     * Specify an action that will execute when transitioning into the configured state
     *
     * @param trigger The trigger by which the state must be entered in order for the action to execute
     * @param entryAction Action to execute, providing details of the transition
     * @return The receiver
     */
    public StateConfigurer<S, T> onEntryFrom(final T trigger, final Action<S, T> entryAction) {
        requireNonNull(entryAction, "entryAction is null");
        registry.addEntryAction(entryAction, new ParameterizedTrigger<>(trigger));
        return this;
    }

    /**
     * Specify an action that will execute when transitioning from the configured state
     *
     * @param exitAction Action to execute
     * @return The receiver
     */
    public StateConfigurer<S, T> onExit(final Action<S, T> exitAction) {
        requireNonNull(exitAction, "exitAction is null");
        registry.addExitAction(exitAction);
        return this;
    }

    /**
     * Accept the specified trigger and transition to the destination state
     *
     * @param trigger The accepted trigger
     * @param destinationState The state that the trigger will cause a transition to
     * @return The receiver
     */
    public StateConfigurer<S, T> permit(final T trigger, final S destinationState) {
        enforceNotIdentityTransition(destinationState);
        return publicPermit(trigger, destinationState);
    }

    /**
     * Accept the specified trigger and transition to the destination state.
     * <p>
     * Additionally a given action is performed when transitioning. This action will be called after
     * the onExit action of the current state and before the onEntry action of
     * the destination state.
     *
     * @param trigger The accepted trigger
     * @param destinationState The state that the trigger will cause a transition to
     * @param action The action to be performed "during" transition
     * @return The reciever
     */
    public StateConfigurer<S, T> permit(final T trigger, final S destinationState, final Action<S, T> action) {
        enforceNotIdentityTransition(destinationState);
        return publicPermit(trigger, destinationState, action);
    }

    /**
     * Accept the specified trigger and transition to the destination state, calculated dynamically by the supplied
     * function.
     * <p>
     * Note that dynamic TriggerBehaviours are NOT tested in combination with parallel states. They will probably fail.
     *
     * @param trigger The accepted trigger
     * @param destinationStateSelector Function to calculate the state that the trigger will cause a transition to
     * @return The receiver
     */
    public StateConfigurer<S, T> permitDynamic(
            final ParameterizedTrigger<S, T> trigger,
            final Func<S, S, T> destinationStateSelector) {
        return permitDynamicIf(trigger, destinationStateSelector, NO_GUARD);
    }

    /**
     * Accept the specified trigger and transition to the destination state, calculated dynamically by the supplied
     * function
     *
     * @param trigger The accepted trigger
     * @param destinationStateSelector Function to calculate the state that the trigger will cause a transition to
     * @return The reciever
     */
    public StateConfigurer<S, T> permitDynamic(
            final T trigger,
            final Func<S, S, T> destinationStateSelector) {
        return permitDynamicIf(trigger, destinationStateSelector, NO_GUARD);
    }

    /**
     * Accept the specified trigger and transition to the destination state, calculated dynamically by the supplied
     * function
     * <p>
     * Additionally a given action is performed when transitioning. This action will be called after
     * the onExit action and before the onEntry action (of the re-entered state).
     *
     * @param trigger The accepted trigger
     * @param destinationStateSelector Function to calculate the state that the trigger will cause a transition to
     * @param action The action to be performed "during" transition
     * @return The receiver
     */
    public StateConfigurer<S, T> permitDynamic(
            final T trigger,
            final Func<S, S, T> destinationStateSelector,
            final Action<S, T> action) {
        return permitDynamicIf(trigger, destinationStateSelector, NO_GUARD, action);
    }

    /**
     * Accept the specified trigger and transition to the destination state, calculated dynamically by the supplied
     * function
     *
     * @param trigger The accepted trigger
     * @param destinationStateSelector Function to calculate the state that the trigger will cause a transition to
     * @param guard Function that must return true in order for the trigger to be accepted
     * @return The reciever
     */
    public StateConfigurer<S, T> permitDynamicIf(
            final ParameterizedTrigger<S, T> trigger,
            final Func<S, S, T> destinationStateSelector,
            final Func<Boolean, S, T> guard) {
        requireNonNull(trigger, "trigger is null");
        requireNonNull(destinationStateSelector, "destinationStateSelector is null");
        return publicPermitDynamicIf(
                trigger.getTrigger(),
                destinationStateSelector,
                guard,
                NO_ACTION);
    }

    /**
     * Accept the specified trigger and transition to the destination state, calculated dynamically by the supplied
     * function
     * <p>
     * Additionally a given action is performed when transitioning. This action will be called after
     * the onExit action of the current state and before the onEntry action of the destination state.
     * The parameter of the trigger will be given to this action.
     *
     * @param trigger The accepted trigger
     * @param destinationStateSelector Function to calculate the state that the trigger will cause a transition to
     * @param guard Function that must return true in order for the trigger to be accepted
     * @param action The action to be performed "during" transition
     * @return The receiver
     */
    public StateConfigurer<S, T> permitDynamicIf(
            final ParameterizedTrigger<S, T> trigger,
            final Func<S, S, T> destinationStateSelector,
            final Func<Boolean, S, T> guard,
            final Action<S, T> action) {
        requireNonNull(trigger, "trigger is null");
        requireNonNull(destinationStateSelector, "destinationStateSelector is null");
        return publicPermitDynamicIf(
                trigger.getTrigger(),
                destinationStateSelector,
                guard,
                action);
    }

    /**
     * Accept the specified trigger and transition to the destination state, calculated dynamically by the supplied
     * function
     *
     * @param trigger The accepted trigger
     * @param destinationStateSelector Function to calculate the state that the trigger will cause a transition to
     * @param guard Function that must return true in order for the trigger to be accepted
     * @return The reciever
     */
    public StateConfigurer<S, T> permitDynamicIf(
            final T trigger,
            final Func<S, S, T> destinationStateSelector,
            final Func<Boolean, S, T> guard) {
        requireNonNull(destinationStateSelector, "destinationStateSelector is null");
        return publicPermitDynamicIf(
                trigger,
                destinationStateSelector,
                guard,
                NO_ACTION);
    }

    /**
     * Accept the specified trigger and transition to the destination state, calculated dynamically by the supplied
     * function
     * <p>
     * Additionally a given action is performed when transitioning. This action will be called after
     * the onExit action of the current state and before the onEntry action of the destination state.
     *
     * @param trigger The accepted trigger
     * @param destinationStateSelector Function to calculate the state that the trigger will cause a transition to
     * @param guard Function that must return true in order for the trigger to be accepted
     * @param action The action to be performed "during" transition
     * @return The receiver
     */
    public StateConfigurer<S, T> permitDynamicIf(
            final T trigger,
            final Func<S, S, T> destinationStateSelector,
            final Func<Boolean, S, T> guard,
            final Action<S, T> action) {
        requireNonNull(destinationStateSelector, "destinationStateSelector is null");
        return publicPermitDynamicIf(
                trigger,
                destinationStateSelector,
                guard,
                action);
    }

    /**
     * Accept the specified trigger and transition to the destination state.
     * <p>
     * For guard rules see the description of the class.
     *
     * @param trigger The accepted trigger
     * @param destinationState The state that the trigger will cause a transition to
     * @param guard Function that must return true in order for the trigger to be accepted
     * @return The reciever
     */
    public StateConfigurer<S, T> permitIf(final T trigger, final S destinationState, final Func<Boolean, S, T> guard) {
        enforceNotIdentityTransition(destinationState);
        return publicPermitIf(trigger, destinationState, guard);
    }

    /**
     * Accept the specified trigger and transition to the destination state
     * <p>
     * Additionally a given action is performed when transitioning. This action will be called after
     * the onExit action of the current state and before the onEntry action of
     * the destination state.
     * <p>
     * For guard rules see the description of the class.
     *
     * @param trigger The accepted trigger
     * @param destinationState The state that the trigger will cause a transition to
     * @param guard Function that must return true in order for the trigger to be accepted
     * @param action The action to be performed "during" transition
     * @return The reciever
     */
    public StateConfigurer<S, T> permitIf(
            final T trigger,
            final S destinationState,
            final Func<Boolean, S, T> guard,
            final Action<S, T> action) {
        enforceNotIdentityTransition(destinationState);
        return publicPermitIf(trigger, destinationState, guard, action);
    }

    /**
     * Accept the specified trigger, execute exit actions and re-execute entry actions. See {@link ReentryTriggerBehaviour}
     * for an exact description of business rules for reentry.
     *
     * @param trigger The accepted trigger
     * @return The reciever
     */
    public StateConfigurer<S, T> permitReentry(final T trigger) {
        return publicPermit(trigger, null);
    }

    /**
     * Accept the specified trigger, execute exit actions and re-execute entry actions. See {@link ReentryTriggerBehaviour}
     * for an exact description of business rules for reentry.
     *
     * @param trigger The accepted trigger
     * @param action The action to be performed "during" transition
     * @return The reciever
     */
    public StateConfigurer<S, T> permitReentry(final T trigger, final Action<S, T> action) {
        return publicPermit(trigger, null, action);
    }

    /**
     * Accept the specified trigger, execute exit actions and re-execute entry actions. See {@link ReentryTriggerBehaviour}
     * for an exact description of business rules for reentry.
     *
     * @param trigger The accepted trigger
     * @param guard Function that must return true in order for the trigger to be accepted
     * @return The reciever
     */
    public StateConfigurer<S, T> permitReentryIf(final T trigger, final Func<Boolean, S, T> guard) {
        return publicPermitIf(trigger, null, guard);
    }

    /**
     * Accept the specified trigger, execute exit actions and re-execute entry actions. See {@link ReentryTriggerBehaviour}
     * for an exact description of business rules for reentry.
     * <p>
     * Additionally a given action is performed when transitioning. This action will be called after
     * the onExit action and before the onEntry action (of the re-entered state).
     *
     * @param trigger The accepted trigger
     * @param guard Function that must return true in order for the trigger to be accepted
     * @param action The transition action to execute in the case of the re-entry.
     * @return The reciever
     */
    public StateConfigurer<S, T> permitReentryIf(final T trigger, final Func<Boolean, S, T> guard,
            final Action<S, T> action) {
        return publicPermitIf(trigger, null, guard, action);
    }

    /**
     * Sets the superstate of which the configured state is a substate.
     * <p>
     * Substates inherit the allowed transitions of their superstate.
     * When entering directly into a substate from outside of the superstate,
     * entry actions for the superstate are executed.
     * Likewise when leaving from the substate to outside the supserstate,
     * exit actions for the superstate will execute.
     * <p>
     *
     *
     * @param superstate The superstate
     * @return this
     * @throws IllegalStateException - if the underlying state already has a superstate.
     * @throws IllegalStateException - if the given superstate doesn't exist.
     *
     * @author rinke
     */
    public StateConfigurer<S, T> substateOf(final S superstate) {
        if (registry.isFrozen()) {
            throw new IllegalStateException("State " + registry.getUnderlyingState().toString() +
                    " is in frozen state, meaning you cannot change the state tree anymore.\r\n" +
                    "First configure all states in the tree, and only after that configure the transitions.");
        }

        // is the superstate already set and if yes, => exception
        // not having to check for configuring this twice in a row makes live a lot easier.
        if (registry.getDirectSuperstate() != null) {
            throw new IllegalStateException("State " + registry.getUnderlyingState().toString() +
                    " has its superstate already set. You cannot set it twice.");
        }

        Map.Entry<S, ParallelMachineConfigurer<S, T>> parallelConfigEntry = hostingConfig
                .getParallelContainingStateEntry(superstate);
        while (parallelConfigEntry != null) {
            // the superstate exists in a substatemachine which is already known and registered.
            // so, the current registry is in the wrong config. Correct that.
            ParallelMachineConfigurer<S, T> parallelConfig = parallelConfigEntry.getValue();
            lookup = parallelConfig.moveFromParent(registry);
            hostingConfig = parallelConfig;
            // and rerun this block, so all is repeated untill we reach the SM config where parent
            // is not anymore in a child state machine.
            parallelConfigEntry = hostingConfig.getParallelContainingStateEntry(superstate);
        }

        // set the correct parent and sub...
        StateRegistry<S, T> superRegistry = lookup.apply(superstate);
        if (superRegistry == null) {
            // not found; superstate doesn't exist.
            throw new IllegalStateException("superstate " + superstate + "doesn't exist. Create it first via configure.");
        }

        // if superstate is a parallel state, a new parallel config has to be initialized.
        if (hostingConfig.getRegistry().isParallelState(superstate)) {
            // handle the configs correctly
            ParallelMachineConfigurer<S, T> parallelConfig = new ParallelMachineConfigurer<S, T>(hostingConfig);
            lookup = hostingConfig.addParallelStateMachineConfig(superstate, parallelConfig, registry);
            hostingConfig = parallelConfig;
            // and set the stuff right in the registry
            superRegistry.addParallelRegistry(parallelConfig.getRegistry());
            registry.setSuperstate(null);
            registry.setParentStateMachineSuperstate(superRegistry);
        } else {
            // superstate settings don't go over the state machine config boundaries, so only set this when NOT parallel
            registry.setSuperstate(superRegistry);
            superRegistry.addSubstate(registry);
        }
        return this;
    }

    StateConfigurer<S, T> publicPermit(final T trigger, final S destinationState) {
        return publicPermitIf(trigger, destinationState, NO_GUARD, NO_ACTION);
    }

    StateConfigurer<S, T> publicPermit(T trigger, S destinationState, Action<S, T> action) {
        return publicPermitIf(trigger, destinationState, NO_GUARD, action);
    }

    StateConfigurer<S, T> publicPermitDynamic(final T trigger, final Func<S, S, T> destinationStateSelector) {
        return publicPermitDynamicIf(trigger, destinationStateSelector, NO_GUARD, NO_ACTION);
    }

    StateConfigurer<S, T> publicPermitDynamicIf(
            final T trigger,
            final Func<S, S, T> destinationStateSelector,
            final Func<Boolean, S, T> guard,
            final Action<S, T> action) {
        requireNonNull(destinationStateSelector, "destinationStateSelector is null");
        requireNonNull(guard, "guard is null");
        S declaredIn = registry.getUnderlyingState();
        registry.addTriggerBehaviour(new DynamicTriggerBehaviour<>(trigger,
                destinationStateSelector, guard, action, declaredIn));
        return this;
    }

    StateConfigurer<S, T> publicPermitIf(T trigger, S destinationState, Func<Boolean, S, T> guard) {
        return publicPermitIf(trigger, destinationState, guard, NO_ACTION);
    }

    /**
     * Permits the transition to the destinationState as a reaction on the trigger.
     * <br>
     * This is the only real method that doesn't point to an overloaded version. The method
     * follows this approach in finding a destination for the trigger:
     * <ul>
     * <li>First look in the present configuration if the destination state can be found. If yes,
     * check if the declared destination is a leaf state. If it is not a leaf state, go into
     * the present destination's initial state, and repeat going into initial states, until a leaf
     * state is found. The leaf state is the new destination.
     * <li>if the present state machine doesn't know the destination, look in child state machines.
     * If found there, search its deepest nested leaf state of that child state machine (via initial states).
     * <li>if still not found, look in the parent state machine. If found there search its deepest nested
     * leaf state (via the initial states).
     * <li>if not found at all, raise an exception
     * </ul>
     *
     * @param trigger - the trigger which induces the transition
     * @param destinationState - the destination state - if this is not a leaf state, get the initial state of
     * the destination, and repeat that process until we reach a leaf state. If this is null, then a
     * ReentryTriggerBehaviour will be used.
     * @param guard - the condition; must evaluate to true or else it won't happen.
     * @param action - the action to be executed should the transition take place.
     * @throws DestinationNotFoundException in case the destination state cannot be found.
     *
     * @return
     *
     * @author rinke (completely rewritten)
     *
     */
    StateConfigurer<S, T> publicPermitIf(T trigger, S destinationState, Func<Boolean, S, T> guard, Action<S, T> action)
            throws DestinationNotFoundException {
        requireNonNull(guard, "guard is null");
        if (action == null) {
            action = NO_ACTION;
        }
        // in all cases freeze the current registry
        registry.freeze();

        TriggerBehaviour<S, T> triggerBehaviour;
        S correctedDestination = destinationState;
        // in this block we will try to find the deepest possible destination state.
        if (destinationState != null) {
            // we cannot use lookup directly, because that looks in childs too, and here we wanna know if it is in the current state machine
            if (hostingConfig.getRegistry().getStates().contains(destinationState)) {
                // THIS BLOCK HANDLES THE CURRENT STATE MACHINE
                // destination is known, we can go for the leaf state. and use lookup.
                StateRegistry<S, T> destinationRegistry = lookup.apply(destinationState);
                // go as deep as we can via initial states until we reach a leaf state.
                while (!destinationRegistry.isLeafState() && !destinationRegistry.hasParallelState()) {
                    destinationRegistry = destinationRegistry.getInitialState();
                }
                // destination found, freeze the whole line.
                destinationRegistry.freeze();
                correctedDestination = destinationRegistry.getUnderlyingState();

            } else if (hostingConfig.getRegistry().getAllStates().contains(destinationState)) {
                // THIS BLOCK HANDLES PARENT OR CHILD STATE MACHINE
                Map.Entry<S, ParallelMachineConfigurer<S, T>> entry = hostingConfig
                        .getParallelContainingStateEntry(destinationState);

                // look in child state machines
                if (entry != null) {
                    // THIS BLOCK HANDLES THE TRIGGER FOUND IN THE CHILD STATE MACHINE
                    // create a new incoming triggerbehavior for the parallel child config.
                    S declaredIn = registry.getUnderlyingState();
                    TriggerBehaviour<S, T> tg = new TransitioningTriggerBehaviour<>(trigger,
                            correctedDestination, guard, action, declaredIn);
                    entry.getValue().getRegistry().addTriggerBehaviour(tg);
                    // and return for this SM the state containing the parallel state machine.
                    correctedDestination = entry.getKey();
                    StateRegistry<S, T> destinationRegistry = lookup.apply(destinationState);
                    destinationRegistry.freeze();

                } else if (hostingConfig instanceof ParallelMachineConfigurer<S, T> castedHostingConfig) {
                    // THIS BLOCK CHECKS THE PARENT STATE MACHINE FOR A DESTINATION
                    MachineConfigurer<S, T> parentConfig = castedHostingConfig.getParentConfiguration();
                    // this will also apply if the destination is in the parent of the parent of the parent....
                    // because in this level it is only important that it should go to a parent. The resurface
                    // call then handles the stuff in the parent.
                    if (parentConfig.getRegistry().getAllStates().contains(destinationState)) {
                        // create a new outgoing triggerbehaviour for the present config
                        S declaredIn = registry.getUnderlyingState();
                        TriggerBehaviour<S, T> tg = new ExitingTriggerBehaviour<>(trigger, guard, declaredIn);
                        registry.addTriggerBehaviour(tg);
                        Integer resurfacingId = tg.getResurfacingId();
                        // and the parent config should save a triggerBehaviour for the original destination
                        S hostingStateInParent = castedHostingConfig.getRegistry().getHostingStateInParent();
                        // and repeat this whole procedure for the parent SM, without guard.
                        parentConfig.configure(hostingStateInParent).resurface(trigger, destinationState, resurfacingId, action);
                        // finally return before the normal procedure can take place.
                        return this;
                    }
                }
            } else {
                // if not found at all, completely unknown...
                throw new DestinationNotFoundException(correctedDestination);
            }
            S declaredIn = registry.getUnderlyingState();
            triggerBehaviour = new TransitioningTriggerBehaviour<>(trigger,
                    correctedDestination, guard, action, declaredIn);
        } else {
            // target is null, so reentry:
            // first check if any parallellism is involved. If so (for now): throw.
            if (registry.hasNestedParallels()) {
                throw new IllegalStateException("ReentryTriggerBehaviours not possible when parallel substates are involved.");
            }
            triggerBehaviour = new ReentryTriggerBehaviour<>(trigger, registry.getUnderlyingState(), guard, action);
        }

        registry.addTriggerBehaviour(triggerBehaviour);
        return this;
    }

    /**
     * checks that a transition doesn't target the leaf state where it is actually already in.
     * If it does, raises an exception.
     * <p>
     * The following types of identity transitions are possible:
     * <ol>
     * <li><b>self to self</b>: state to itself, source state == destination
     * <li><b>parent to sub</b>: destination state is a substate of source, and there is only one
     * possible substate on each level between source and destination. So, in other words: when in source,
     * the state machine MUST also be in destination because there is no other option of substates to be in.
     * <li><b>sub to parent</b>: destination state is a parent of the underlying state (or a parent of a
     * parent, so anywhere on the parental path), and the underlying source state is in the inital path of
     * the destination state.<br>
     * Note that the latter means that transitions to a state's own parent are allowed, as long
     * as they are permitted from a child state of the parent, and as long as that child state is not
     * in the initial path of the parent's state. Such a transition would lead into going in the parent,
     * and then going into the parent's initial state path - which is different from the source state.
     * <li><b>parallellism: parent to sub SM</b>: when a (parent of a) parallel state as source points
     * to some state that is in one of the state machines contained by the parallel state.
     * <li><b>parallellism: sub to parent SM</b>: when a state in a child state machine points to a
     * destination which is its present parent in a hosting state machine.
     * </ol>
     * <p>
     * Also note that this method is called at design time: there is no state machine yet, so there is not
     * either a notion of "present state being in". At run time, it might still be able to have a trigger
     * pointing to a substate of source in which the state machine already is, while this situation couldn't
     * have been caught/prevented by this method (so: second option in list, but with more than 1 possible
     * substates). This kind of situation should be caught at runtime.
     *
     */
    private void enforceNotIdentityTransition(final S destination) {
        if (destination == null) {
            // null destination means reentry trigger
            return;
        }
        boolean throwIt = false;
        if (destination.equals(registry.getUnderlyingState())) {
            // first option; easy
            throwIt = true;
        } else {
            StateRegistry<S, T> destinationReg = lookup.apply(destination);
            if (destinationReg != null) {
                // second option; fail if destination is a substate of source (only in present SM)
                if (registry.includes(destination)) {
                    // dive into substates untill we get more than one -> that makes it fail
                    StateRegistry<S, T> tempReg = registry;
                    boolean stop = false;
                    while (!stop) {
                        int size = tempReg.getDirectSubstates().size();
                        if (size > 1) {
                            stop = true;
                        } else if (size == 0) {
                            // end reached, break
                            throwIt = true;
                            break;
                        } else {
                            // size == 1, continue next level
                            tempReg = tempReg.getDirectSubstates().get(0);
                            if (tempReg.getUnderlyingState().equals(destination)) {
                                throwIt = true;
                                break;
                            } // if tempReg...
                        } // else size > 1 or size == 0
                    } // while
                } else {
                    // registry does not include destination, so try option 3 in javadoc list:
                    // fail if destination is a parent of underlying state
                    if (destinationReg.includes(registry.getUnderlyingState())) {
                        // source state must be in initial path of destination state
                        StateRegistry<S, T> tempReg = destinationReg;
                        while (!throwIt && tempReg != null) {
                            if (tempReg.getUnderlyingState().equals(registry.getUnderlyingState())) {
                                throwIt = true;
                            }
                            tempReg = tempReg.getInitialState();
                        } // while
                    } // if destinationReg includes
                } // else registry includes
            } // if destinationReg != null
        } // else destination equals underlying
        if (throwIt) {
            throw new IllegalStateException(
                    "Permit() (and PermitIf()) require that the destination state is not equal to the source state. "
                            + "To accept a trigger without changing state, use either Ignore() or PermitReentry();"
                            + " destination = " + destination.toString());
        }

        // fourth option: destination is in child SM of present state.
        Entry<S, ParallelMachineConfigurer<S, T>> entry = hostingConfig.getParallelContainingStateEntry(destination);
        if (entry != null) {
            // state is contained in some child SM
            // now fail if this registry contains parallelReg
            if (registry.includes(entry.getKey())) {
                throw new IllegalStateException(
                        "Cross-parallel triggers are not allowed. You're creating a trigger that points to a destination"
                                + " which is inside some parallel state inside the present state. "
                                + " destination = " + destination.toString());
            }
        }
        // fifth option: destination is some parent in some parent SM
        StateRegistry<S, T> tempReg = registry;
        boolean parallelBoundaryPassed = false;
        while (true) {
            if (parallelBoundaryPassed && tempReg.getUnderlyingState().equals(destination)) {
                throw new IllegalStateException(
                        "Cross-parallel triggers are not allowed. You're creating a trigger that points to a destination"
                                + " which is the parent state of an inclosing parent state machine. "
                                + " destination = " + destination.toString());
            }
            if (tempReg.getDirectSuperstate() == null) {
                parallelBoundaryPassed = true;
            }
            tempReg = tempReg.getSuperstateIncludingParentStateMachine();
            if (tempReg == null) {
                break;
            }
        }
    }

    /**
     * A stripped version of {@link #publicPermitIf(Object, Object, Func, Action)}, specifically for the situation
     * where the transition is from a nested child state machine to a state in the parent state machine, thus
     * exiting the child state machine. This method is like <code>publicPermitIf</code>, but creates a special
     * TriggerBehaviour for the rest of the transition (in the parent), and also skips the situations in
     * <code>publicPermitIf</code> which make no sense for resurfacing transitions.
     *
     * @param trigger
     * @param destinationState
     * @param action
     * @return
     */
    private StateConfigurer<S, T> resurface(T trigger, S destinationState, Integer resurfacingId, Action<S, T> action) {
        S correctedDestination = destinationState;
        registry.freeze();
        if (hostingConfig.getRegistry().getStates().contains(destinationState)) {
            // THIS BLOCK HANDLES THE CURRENT STATE MACHINE
            StateRegistry<S, T> destinationRegistry = lookup.apply(destinationState);
            // resurfacing is always in a parallel state, so no need to search for leaf states.
            destinationRegistry.freeze();
            correctedDestination = destinationRegistry.getUnderlyingState();
            TriggerBehaviour<S, T> tg = new ResurfacingTriggerBehaviour<>(trigger, correctedDestination, resurfacingId, action);
            registry.addTriggerBehaviour(tg);
        } else {
            // if not found in current, it MUST be in the parent.
            ParallelMachineConfigurer<S, T> castedHostingConfig = (ParallelMachineConfigurer<S, T>) hostingConfig;
            MachineConfigurer<S, T> parentConfig = castedHostingConfig.getParentConfiguration();
            // create a new outgoing triggerbehaviour for the present config
            // This must be resurfacing and exiting at the same time. There is a special constructor for that...
            TriggerBehaviour<S, T> tg = new ExitingTriggerBehaviour<>(trigger, resurfacingId);
            registry.addTriggerBehaviour(tg);
            // and the parent config should save a triggerBehaviour for the original destination
            S hostingStateInParent = castedHostingConfig.getRegistry().getHostingStateInParent();
            // and repeat this whole procedure for the parent SM, without guard.
            // the call to permit takes correctly care of necessary freezing.
            parentConfig.configure(hostingStateInParent).resurface(trigger, destinationState, resurfacingId, action);
        }

        return this;
    }

}
