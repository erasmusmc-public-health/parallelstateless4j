/*
This file is part of ParallelStateless4j, a <a href="https://en.wikipedia.org/wiki/Finite-state_machine">
Finite State Machine</a> implementation for java.
This project forks <a href="https://github.com/stateless4j/stateless4j">stateless4j</a>
and extends it with parallel states - states in which each direct substate will always be
entered simultaneous with other direct substates of the parallel state.

This work is under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


2019, 2021; written by Rinke Hoekstra
department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
http://www.erasmusmc.nl/public-health/?lang=en
E-mail: wormsim@erasmusmc.nl

More information can be found at
https://gitlab.com/erasmusmc-public-health/parallelstateless4j/-/wikis/home

 */
package nl.erasmusmc.mgz.parallelstateless4j.configuration;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import nl.erasmusmc.mgz.parallelstateless4j.IContextListenerRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.IContextPropertyListener;
import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineExitListener;
import nl.erasmusmc.mgz.parallelstateless4j.IStateMachineListenerRegistry;
import nl.erasmusmc.mgz.parallelstateless4j.OutVar;
import nl.erasmusmc.mgz.parallelstateless4j.delegates.Action;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.ParameterizedTrigger;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TransitioningTriggerBehaviour;
import nl.erasmusmc.mgz.parallelstateless4j.triggers.TriggerBehaviour;

/**
 * The state machine configuration, and main entry point for configuration actions.
 * <p>
 * The class basically performs the actions needed for configuration, but doesn't store
 * the configuration data. Therefore, it is only used at config-time. During runtime of a
 * state machine, the class isn't used anymore. The configuration is stored at the
 * {@link MachineRegistry} field.
 * <br>
 * The only thing which is stored by the configurer is a Multimap with all nested
 * configurer objects needed for parallelism.
 *
 * @param <S> The type used to represent the states
 * @param <T> The type used to represent the triggers that cause state transitions
 *
 */
public class MachineConfigurer<S, T> implements IContextListenerRegistry, IStateMachineListenerRegistry<S, T> {

    public final Action<S, T>                                    EMPTY_ACTION  = (stateMachineContext, transition, parent,
            args) -> {
                                                                               };

    protected MachineRegistry<S, T>                              registry;

    /**
     * a Multimap which holds all available direct sub-statemachinesConfigurers. The key is the
     * parallel state holding child state machines; the values are the {@link ParallelMachineConfigurer}s.
     */
    protected final Multimap<S, ParallelMachineConfigurer<S, T>> nestedConfigs = HashMultimap
            .create();

    /**
     * a Map with <code>IContextPropertyListener</code>s which will be valid for any state machine that
     * is instantiated via this config.
     */
    private Multimap<String, IContextPropertyListener>           contextListeners;

    /**
     * a List with <code>IStateMachineExitListener</code>s which will be valid for any state machine that
     * is instantiated via this config.
     */
    private List<IStateMachineExitListener<S, T>>                exitListeners = new ArrayList<>(3);

    public MachineConfigurer() {
        registry = new MachineRegistry<S, T>(this::getNestedConfigs);
        registry.setUnhandledTriggerAction((stateMachineContext, transition, parent, args) -> {
            Object id = stateMachineContext.getAttribute("id");
            String idStr = (id == null) ? "" : "Agent #" + ((Long) id).toString();
            String msg = String.format(idStr + "No valid leaving transitions are known from states %s "
                    + "for trigger '%s'. Consider ignoring the trigger.",
                    stateMachineContext.getStateMachine().getStateMachineStatus().toString(),
                    transition.getTrigger());
            throw new IllegalStateException(msg);
        });
        registry.setFailedConditionTriggerAction((stateMachineContext, transition, parent, args) -> {
            throw new IllegalStateException(
                    String.format("Trigger '%s' does have a valid transition defined from state '%s' but its "
                            + "condition fails, hence the transition cannot take place.",
                            transition.getTrigger(),
                            transition.getSource()));
        });
    }

    /**
     * {@inheritDoc}
     * ExitListeners are NOT passed to parallel sub-statemachines. So onExit is usually only
     * notified for the top level state machine exiting.
     * You can try to explicitly set exitListeners for sub-statemachines, and it might work, but
     * it has not been tested.
     */
    @Override
    public void addListener(IStateMachineExitListener<S, T> listener) {
        exitListeners.add(listener);
    }

    @Override
    public void addListener(String key, IContextPropertyListener listener) {
        if (contextListeners == null) {
            contextListeners = HashMultimap.create(2, 2);
        }
        contextListeners.put(key, listener);
    }

    /**
     * Begin configuration of a state in the state machine via this method. You may call this repeatedly without problems.
     * It can also be called on a parallel state which has already been configured via a call to
     * <code>parallel(S)</code>.
     *
     * @param state The state to configure
     * @return A configuration object through which the state can be configured
     */
    public StateConfigurer<S, T> configure(final S state) {
        return configure(state, false);
    }

    /**
     * writes info to the specified OutputStream. The information is a textual presentation of the nested states in the
     * configuration.
     *
     */
    public void generateDotFileInfo(final OutputStream dotFile) throws IOException {
        try (OutputStreamWriter w = new OutputStreamWriter(dotFile, "UTF-8")) {
            PrintWriter writer = new PrintWriter(w);
            writer.write("digraph G {\n");
            OutVar<S> destination = new OutVar<>();
            for (Map.Entry<S, StateRegistry<S, T>> entry : registry.containedStates.entrySet()) {
                Multimap<T, TriggerBehaviour<S, T>> behaviours = entry.getValue().getTriggerBehaviours();
                for (TriggerBehaviour<S, T> triggerBehaviour : behaviours.values()) {
                    if (triggerBehaviour instanceof TransitioningTriggerBehaviour) {
                        destination.set(null);
                        triggerBehaviour.resultsInTransitionFrom(null, null, null, destination);
                        writer.write(String.format("\t%s -> %s;\n", entry.getKey(), destination));
                    }
                }
            }
            writer.write("}");
        }
    }

    public Multimap<String, IContextPropertyListener> getContextListeners() {
        return contextListeners;
    }

    public List<IStateMachineExitListener<S, T>> getExitListeners() {
        return exitListeners;
    }

    /**
     * gets the registry, the object in which all configuration settings are stored.
     */
    public MachineRegistry<S, T> getRegistry() {
        return registry;
    }

    /**
     * Sets the behaviour of a state machine in case a trigger is fired for which no working triggers exists
     * except but a triggerBehaviour whose condition is failing.
     * This methods sets this behaviour for all State Machines instantiated via this config. It overrides
     * the default behaviour (= throwing exception) set in the constructor of this class.
     * <p>
     * However, you can overwrite this behaviour for each individual state machine, by calling
     * the equally named method on that particular state machine instance.
     *
     * @param failedConditionTriggerAction the action to be exectued when no trigger is found
     * except one whose condition is failing. May be null, in which case no action is used.
     *
     * @author rinke
     */
    public void onFailedConditionTrigger(final Action<S, T> failedConditionTriggerAction) {
        if (failedConditionTriggerAction == null) {
            registry.setFailedConditionTriggerAction(EMPTY_ACTION);
        } else {
            registry.setFailedConditionTriggerAction(failedConditionTriggerAction);
        }
    }

    /**
     * Sets the behaviour of a state machine in case of firing an unhandled trigger.
     * This method sets this behaviour for all State Machines instantiated via this config.
     * It overrides
     * the default behaviour (= throwing exception) set in the constructor of this class.
     * <p>
     * However, you can overwrite this behaviour for each individual state machine, by calling
     * the equally named method on that particular state machine instance.
     *
     * @param unhandledTriggerAction An action to call when an unhandled trigger is fired.
     * May be null, in which case an empty action is used.
     *
     * @author rinke
     */
    public void onUnhandledTrigger(final Action<S, T> unhandledTriggerAction) {
        if (unhandledTriggerAction == null) {
            registry.setUnhandledTriggerAction(EMPTY_ACTION);
        } else {
            registry.setUnhandledTriggerAction(unhandledTriggerAction);
        }
    }

    /**
     * Begin the configuration of a parallel state in the state machine.
     * A parallel state is a state who's children are entered in parallel,
     * meaning all substates of the parallel state are entered simultaneously.
     * <p>
     * This method can be called repeatedly on a parallel state. You can also call {@link #configure(Object)}
     * after the first call to <code>parallel</code>; it has the same effect. So the following is allowed:
     *
     * <pre>
     * config.parallel(parallelState);
     * config.parallel(parallelState)...;
     * config.configure(parallelState)...;
     * </pre>
     *
     * However: This is NOT allowed:
     *
     * <pre>
     * config.configure(state);
     * config.parallel(state);
     * </pre>
     *
     * @param parallel The parallel state to configure
     * @return A configuration object through which the parallel state can be configured. Note that this
     * is the same class as with {@link #configure(Object)}; returning a special class for parallel configuration
     * would not help, because the only method which makes a difference for parallel is
     * {@link StateConfigurer#substateOf(Object)} which is called via the child and not via the parent.
     */
    public StateConfigurer<S, T> parallel(final S parallel) {
        // The parallel state may already have been registered with an earlier call to parallel(), which is
        // OK. But if it is registered with an earlier call to configure(), it is a configuration error.
        // So, if it is NOT registered as parallel, but if it is known, it must have been registered via configure.
        if (!registry.isParallelStatePossiblyNested(parallel) && registry.getAllStates().contains(parallel)) {
            throw new StateTypeInUseException("You cannot configure state " + parallel.toString() +
                    " as parallel state, while it is already registered as normal state");
        }
        if (!nestedConfigs.containsKey(parallel)) {
            // as we don't know yet the child state machines we store the key with a value of null, which is allowed in the multimap
            nestedConfigs.put(parallel, null);
        }
        return configure(parallel, true);
    }

    @Override
    public void removeAllListeners() {
        exitListeners.clear();
    }

    @Override
    public void removeListener(IStateMachineExitListener<S, T> listener) {
        exitListeners.remove(listener);
    }

    @Override
    public void removeListener(String key, IContextPropertyListener listener) {
        if (contextListeners != null) {
            contextListeners.remove(key, listener);
        }
    }

    @Override
    public void removeListeners(String key) {
        if (contextListeners != null) {
            contextListeners.removeAll(key);
        }
    }

    /**
     * sets the speficied state as initial state of the state machine.
     * If there is already an initial state set, it is overwritten, and
     * the specified state is now marked as new initial state. This method is to be called when
     * the end user wants to explicitly configure a state as initial.
     * <br>
     * The initial state is the first state a state machine starts in, after it
     * has been created. Any state can be marked as initial state; if the state marked as initial state is
     * not a leaf state, the configuration will automatically search for the first initial substate of the
     * state that is marked as initial state (and further, until a leaf state is found).
     * If the state marked as initial state is not a direct substate of the state machine, then
     * then the whole tree which is needed to reach the marked initial state will be assigned as initial
     * state, so including all parents.
     * <p>
     * If the specified state was not yet known by the stateMachine, it creates a new
     * configuration and StateRegistration for this state. If the state was already known/configured,
     * then it uses the already existing StateRegistration.
     * <p>
     * If no initial state is explicitly set on a state machine, then the first state which was ever
     * configured for this state machine is regarded as the initial state. However, in case of using
     * parallelism, we recommend to set the initial state explicitly via this method, just after all
     * parallel states have been configured correctly.
     *
     * @param state
     * @throws IllegalStateException if the involved states are frozen.
     * @return a StateConfiguration object through which the initial state can be further configured.
     *
     * @author rinke
     */
    public StateConfigurer<S, T> setInitialState(final S state) {
        // first check on frozen...
        if (registry.initialState != null && registry.getInitialStateRegistry().isFrozen()) {
            throw new IllegalStateException("cannot reset the initial state of a state machine because the present " +
                    "initial state is already in use for a transition. Configure first the complete state tree, and " +
                    "only then the transitions.");
        }
        StateRegistry<S, T> initialRegistration = this.getStateRegistry(state);
        if (initialRegistration != null && initialRegistration.isFrozen()) {
            throw new IllegalStateException("cannot reset the initial state of a state machine because that new " +
                    "initial state is already in use for a transition. Configure first the complete state tree, and " +
                    "only then the transitions.");
        }
        registry.initialState = state;
        return configure(state);
    }

    /**
     * Specify the arguments that must be supplied when a specific trigger is fired
     *
     * @param trigger The underlying trigger value
     * @return An object that can be passed to the fire() method in order to fire the parameterised trigger
     */
    @SuppressWarnings("rawtypes")
    public ParameterizedTrigger<S, T> setTriggerParameters(
            final T trigger,
            final Class... types) {

        ParameterizedTrigger<S, T> configuration = new ParameterizedTrigger<>(trigger, types);
        saveTriggerConfiguration(configuration);
        return configuration;
    }

    /**
     * adds the given ParallelStateMachineConfig to the childStateMachines multimap.
     * This method takes care of the following:
     * <ul>
     * <li>sets the config correctly to the childStateMachines multimap
     * <li>sets the states correctly to the parallel config
     * <li>moves the presentation from this config to the parallel child config
     * </ul>
     *
     * @return The lookup function, to be set on the StateConfigurer for this stateRegistration.
     * @author rinke
     */
    protected Function<S, StateRegistry<S, T>> addParallelStateMachineConfig(S parent,
            ParallelMachineConfigurer<S, T> parallelConfig,
            StateRegistry<S, T> topStateInParallel) {
        nestedConfigs.remove(parent, null);
        nestedConfigs.put(parent, parallelConfig);
        parallelConfig.getRegistry().setHostingStateInParent(parent);
        return parallelConfig.moveFromParent(topStateInParallel);
    }

    protected Multimap<S, ParallelMachineConfigurer<S, T>> getNestedConfigs() {
        return nestedConfigs;
    }

    /**
     * Returns StateRegistry for the specified state. Looks in the present configuration, and in
     * possible parallel child configurations. Creates StateRegistry if it does not exist.
     * If no stateConfiguration exists at all (so: if this is the first state to be configured or used on
     * this state machine), then the state is marked as initial state.
     *
     * @param state The state
     * @return StateRegistry for the specified state.
     */
    protected StateRegistry<S, T> getOrCreateStateRegistry(final S state) {
        return getOrCreateStateRegistry(state, false);
    }

    /**
     * gets the parallel state and parallel configurer which contain the specified state, even
     * if it is deeply nested in yet another substatemachine.
     * <p>
     * In case of deeply nested parallelism, this
     * method will return only the topmost entry, not the deeper nested. <br>
     * For example:
     *
     * <pre>
     * <PARALLEL parallel
     * ---<PARALLEL nestedParallel
     * ------<state c1
     * ------<state c2
     * ---<state c
     * </pre>
     *
     * In this configuration, <code>getParallelContainingStateEntry(c1)</code> will return entry <code>
     * parallel</code> and the ParallelConfigurer which has <code>nestedParallel</code> as init state.
     * If you want it to return entry <code>nestedParallel</code> and the ParallelConfigurer having
     * <code>c1</code> as initial state, then call {@link #getDeepestParallelContainingStateEntry(Object)}.
     * <p>
     * returns null if the state could not be found, or if the state could be found, but is not part
     * of a parallel setup.
     *
     */
    protected Map.Entry<S, ParallelMachineConfigurer<S, T>> getParallelContainingStateEntry(S state) {
        for (Map.Entry<S, ParallelMachineConfigurer<S, T>> entry : nestedConfigs.entries()) {
            // value may be null if substates of parallel are not yet registered
            if ((entry.getValue() != null) &&
                    (entry.getValue().getRegistry().getAllStatesNotParent().contains(state))) {
                return entry;
            }
        }
        return null;
    }

    /**
     * as {@link #getOrCreateStateRegistry(Object)}, but doesn't create a new StateRegistry in the
     * registry if it's not found. If not found, does nothing and just returns null.
     *
     * @param state
     * @return
     */
    protected StateRegistry<S, T> getStateRegistry(final S state) {
        return registry.getStateRegistry(state, true);
    }

    protected StateRegistry<S, T> registerNewStateRegistry(final S state, boolean asParallel) {
        StateRegistry<S, T> result = new StateRegistry<>(state, asParallel);
        if (registry.containedStates.isEmpty()) {
            registry.initialState = state;
        }
        registry.containedStates.put(state, result);
        return result;
    }

    /**
     * as {@link #setInitialState(Object)}, but use this method for internal setting of the initial state (vs setting it
     * as end user). Call this when the stateRegistry is already available and can be passed
     * as argument. In such a case this method is more efficient than the overloaded version, because some checks can
     * be skipped, the search for the correct StateRegistry object can be skipped, and the final configuration creation
     * may be skipped too.
     * @param initialStateRegistry - the stateRegistry for the state which is the initial state of this state machine.
     * @throws IllegalStateException: if the state machine registry has already an initial state set which is frozen, or if
     * the initial state to be is already frozen.
     */
    void setInitialState(StateRegistry<S, T> initialStateRegistry) {
        // first check on frozen...
        if (registry.initialState != null && registry.getInitialStateRegistry().isFrozen()) {
            throw new IllegalStateException("cannot reset the initial state of a state machine because the present " +
                    "initial state is already in use for a transition. Configure first the complete state tree, and " +
                    "only then the transitions.");
        }
        if (initialStateRegistry.isFrozen()) {
            throw new IllegalStateException("cannot reset the initial state of a state machine because that new " +
                    "initial state is already in use for a transition. Configure first the complete state tree, and " +
                    "only then the transitions.");
        }
        registry.initialState = initialStateRegistry.getUnderlyingState();
    }

    /**
     * {@link #configure(Object)}
     *
     * @param state
     * @param isParallel if true the state is to be marked as parallel state.
     * @return
     */
    private StateConfigurer<S, T> configure(final S state, boolean isParallel) {
        MachineConfigurer<S, T> hostingConfig = getDeepestParallelContainingState(state);
        StateRegistry<S, T> representation = getOrCreateStateRegistry(state, isParallel);
        return new StateConfigurer<>(representation,
                hostingConfig == null ? this : hostingConfig,
                this::getStateRegistry);
    }

    /**
     * as {@link #getDeepestParallelContainingStateEntry(Object)} but returns just the ParallelStateMachineConfig.
     * @param state
     * @return
     * @author rinke
     */
    private ParallelMachineConfigurer<S, T> getDeepestParallelContainingState(S state) {
        Map.Entry<S, ParallelMachineConfigurer<S, T>> entry = getDeepestParallelContainingStateEntry(state);
        if (entry == null) {
            return null;
        }
        return entry.getValue();
    }

    /**
     * As {@link #getParallelContainingStateEntry(Object)}, but gives the deepest nested entry possible.
     * So in this case:
     *
     * <pre>
     * <PARALLEL parallel
     * ---<PARALLEL nestedParallel
     * ------<state c1
     * ------<state c2
     * ---<state c
     * </pre>
     *
     * It will return entry <code>nestedParallel</code> and the config of <code>c1</code>.
     *
     */
    private Map.Entry<S, ParallelMachineConfigurer<S, T>> getDeepestParallelContainingStateEntry(S state) {
        Map.Entry<S, ParallelMachineConfigurer<S, T>> result = null;
        Map.Entry<S, ParallelMachineConfigurer<S, T>> previous = null;
        MachineConfigurer<S, T> config = this;
        do {
            previous = result;
            result = config.getParallelContainingStateEntry(state);
            if (result != null) {
                config = result.getValue();
            } else {
                break;
            }
        } while (true);
        return previous;
    }

    private StateRegistry<S, T> getOrCreateStateRegistry(final S state, boolean asParallel) {
        requireNonNull(state, "state is null");
        StateRegistry<S, T> result = getStateRegistry(state);
        if (result == null) {
            result = registerNewStateRegistry(state, asParallel);
        }
        return result;
    }

    private void saveTriggerConfiguration(final ParameterizedTrigger<S, T> trigger) {
        if (registry.triggerConfiguration.containsKey(trigger.getTrigger())) {
            throw new IllegalStateException("Parameters for the trigger '" + trigger + "' have already been configured.");
        }

        registry.triggerConfiguration.put(trigger.getTrigger(), trigger);
    }

}
