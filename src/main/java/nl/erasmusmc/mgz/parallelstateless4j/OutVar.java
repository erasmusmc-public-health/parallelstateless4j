package nl.erasmusmc.mgz.parallelstateless4j;

/**
 * a wrapper class to store a destination
 *
 * @param <T>
 */
public final class OutVar<T> {

    private T obj;

    public T get() {
        return obj;
    }

    public void set(T v) {
        this.obj = v;
    }

    @Override
    public String toString() {
        return String.valueOf(obj);
    }
}
